﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Security;
using System.Security.Permissions;


namespace EWC_NSAA_WebService_XML
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class EWC_NSAA_WebService_XML: System.Web.Services.WebService
    {

        [WebMethod]
        public DataSet GetCountriesList()
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetCountriesList", "Trace Begin");
            retDS = enrollmentDAO.GetCountriesList();
            HttpContext.Current.Trace.Write("GetCountriesList", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet GetLanguagesList()
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetLanguagesList", "Trace Begin");
            retDS = enrollmentDAO.GetLanguagesList();
            HttpContext.Current.Trace.Write("GetLanguagesList", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetNavigator(string strDomainName)
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            if (strDomainName.Length <= 20)
            {
                //Start Trace for Adding Code to the database
                HttpContext.Current.Trace.Write("GetNavigatorID", "Trace Begin");
                retDS = enrollmentDAO.GetNavigator(strDomainName);
                HttpContext.Current.Trace.Write("GetNavigatorID", "Trace End");
            }
            else
            {
                throw new SecurityException("Parameter length exceeded maximum limit");

            }

            return retDS;
        }

        [WebMethod]
        public DataSet GetRaceList()
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetRaceList", "Trace Begin");
            retDS = enrollmentDAO.GetRaceList();
            HttpContext.Current.Trace.Write("GetRaceList", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet GetRaceAsianList()
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetRaceAsianList", "Trace Begin");
            retDS = enrollmentDAO.GetRaceAsianList();
            HttpContext.Current.Trace.Write("GetRaceAsianList", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet GetRacePacIslanderList()
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetRacePacIslanderList", "Trace Begin");
            retDS = enrollmentDAO.GetRacePacIslanderList();
            HttpContext.Current.Trace.Write("GetRacePacIslanderList", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetRecipientsList(int NavigatorID)
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetRecipientsList", "Trace Begin");
            retDS = enrollmentDAO.GetRecipientsList(NavigatorID);
            HttpContext.Current.Trace.Write("GetRecipientsList", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int InsertServiceRecipient(int FK_NavigatorID, string FK_RecipID, string LastName, string FirstName, string MiddleInitial,
                            DateTime? DOB, bool? AddressNotAvailable, string Address1, string Address2, string City, string State, string Zip,
                            string Phone1, int P1PhoneType, bool P1PersonalMessage, string Phone2, int? P2PhoneType, bool? P2PersonalMessage, string MotherMaidenName,
                            string Email, string SSN, bool? ImmigrantStatus, int? CountryOfOrigin, string Gender, bool? Ethnicity, int PrimaryLanguage, bool AgreeToSpeakEnglish,
                            string CaregiverName, int? Relationship, string RelationshipOther, string CaregiverPhone, int? CaregiverPhoneType,
                            bool? CaregiverPhonePersonalMessage, string CaregiverEmail, int? CaregiverContactPreference, string Comments)
        {
            int retValue = 0;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertServiceRecipient", "Trace Begin");
            retValue = enrollmentDAO.InsertServiceRecipient(FK_NavigatorID, FK_RecipID, LastName, FirstName, MiddleInitial,
                        DOB, AddressNotAvailable, Address1, Address2, City, State, Zip, Phone1, P1PhoneType, P1PersonalMessage,
                        Phone2, P2PhoneType, P2PersonalMessage, MotherMaidenName, Email, SSN, ImmigrantStatus, CountryOfOrigin, Gender, Ethnicity,
                        PrimaryLanguage, AgreeToSpeakEnglish, CaregiverName, Relationship, RelationshipOther, CaregiverPhone,
                        CaregiverPhoneType, CaregiverPhonePersonalMessage, CaregiverEmail, CaregiverContactPreference,
                        Comments);
            HttpContext.Current.Trace.Write("InsertServiceRecipient", "Trace End");

            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int UpdateServiceRecipient(int NSRecipientID, int FK_NavigatorID, string FK_RecipID, string LastName, string FirstName, string MiddleInitial,
                            DateTime? DOB, bool? AddressNotAvailable, string Address1, string Address2, string City, string State, string Zip,
                            string Phone1, int P1PhoneType, bool P1PersonalMessage, string Phone2, int? P2PhoneType, bool? P2PersonalMessage, string MotherMaidenName,
                            string Email, string SSN, bool? ImmigrantStatus, int? CountryOfOrigin, string Gender, bool? Ethnicity, int PrimaryLanguage, bool AgreeToSpeakEnglish,
                            string CaregiverName, int? Relationship, string RelationshipOther, string CaregiverPhone, int? CaregiverPhoneType,
                            bool? CaregiverPhonePersonalMessage, string CaregiverEmail, int? CaregiverContactPreference, string Comments)
        {
            int retValue = 0;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();


            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateServiceRecipient", "Trace Begin");
            retValue = enrollmentDAO.UpdateServiceRecipient(NSRecipientID, FK_NavigatorID, FK_RecipID, LastName, FirstName, MiddleInitial,
                        DOB, AddressNotAvailable, Address1, Address2, City, State, Zip, Phone1, P1PhoneType, P1PersonalMessage,
                        Phone2, P2PhoneType, P2PersonalMessage, MotherMaidenName, Email, SSN, ImmigrantStatus, CountryOfOrigin, Gender, Ethnicity,
                        PrimaryLanguage, AgreeToSpeakEnglish, CaregiverName, Relationship, RelationshipOther, CaregiverPhone,
                        CaregiverPhoneType, CaregiverPhonePersonalMessage, CaregiverEmail, CaregiverContactPreference, Comments);
            HttpContext.Current.Trace.Write("UpdateServiceRecipient", "Trace End");

            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int SaveRace(int NSID, string RaceList, string RaceOther)
        {
            int retValue = 0;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            if (RaceList.Length <= 10 && RaceOther.Length <= 20)
            {
                //Start Trace for Adding Code to the database
                HttpContext.Current.Trace.Write("SaveRace", "Trace Begin");
                retValue = enrollmentDAO.SaveRace(NSID, RaceList, RaceOther);
                HttpContext.Current.Trace.Write("SaveRace", "Trace End");
            }
            else
            {
                throw new SecurityException("Parameter length exceeded maximum limit");

            }

            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetRace(int NSID)
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetRace", "Trace Begin");
            retDS = enrollmentDAO.GetRace(NSID);
            HttpContext.Current.Trace.Write("GetRace", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetServiceRecipient(int NSID)
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetServiceRecipient", "Trace Begin");
            retDS = enrollmentDAO.GetServiceRecipient(NSID);
            HttpContext.Current.Trace.Write("GetServiceRecipient", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet SelectServiceRecipient(int NavigatorID)
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("SelectServiceRecipient", "Trace Begin");
            retDS = enrollmentDAO.SelectServiceRecipient(NavigatorID);
            HttpContext.Current.Trace.Write("SelectServiceRecipient", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int InsertNavigator(string DomainName, int Region, int Type, string LastName, string FirstName,
                            string Address1, string Address2, string City, string State, string Zip,
                            string BusinessTelephone, string MobileTelephone, string Email)
        {
            int retValue = 0;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertNavigator", "Trace Begin");
            retValue = navigatorDAO.InsertNavigator(DomainName, Region, Type, LastName, FirstName,
                        Address1, Address2, City, State, Zip, BusinessTelephone, MobileTelephone, Email);
            HttpContext.Current.Trace.Write("InsertNavigator", "Trace End");

            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int UpdateNavigator(int NavigatorID, string DomainName, int Region, int Type, string LastName, string FirstName,
                            string Address1, string Address2, string City, string State, string Zip,
                            string BusinessTelephone, string MobileTelephone, string Email)
        {
            int retValue = 0;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateNavigator", "Trace Begin");
            retValue = navigatorDAO.UpdateNavigator(NavigatorID, DomainName, Region, Type, LastName, FirstName,
                        Address1, Address2, City, State, Zip, BusinessTelephone, MobileTelephone, Email);
            HttpContext.Current.Trace.Write("UpdateNavigator", "Trace End");


            return retValue;
        }

        [WebMethod]
        public DataSet GetCancerSitesList()
        {
            DataSet retDS;
            CycleDAO cycleDAO = new CycleDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetCancerSitesList", "Trace Begin");
            retDS = cycleDAO.GetCancerSitesList();
            HttpContext.Current.Trace.Write("GetCancerSitesList", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet GetNSProblemsList()
        {
            DataSet retDS;
            CycleDAO cycleDAO = new CycleDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetNSProblemsList", "Trace Begin");
            retDS = cycleDAO.GetNSProblemsList();
            HttpContext.Current.Trace.Write("GetNSProblemsList", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet GetHealthProgramsList()
        {
            DataSet retDS;
            CycleDAO cycleDAO = new CycleDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetHealthProgramsList", "Trace Begin");
            retDS = cycleDAO.GetHealthProgramsList();
            HttpContext.Current.Trace.Write("GetHealthProgramsList", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet GetHealthInsuranceList()
        {
            DataSet retDS;
            CycleDAO cycleDAO = new CycleDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetHealthInsuranceList", "Trace Begin");
            retDS = cycleDAO.GetHealthInsuranceList();
            HttpContext.Current.Trace.Write("GetHealthInsuranceList", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet SelectNavigationCycle(int NSID)
        {
            DataSet retDS;
            CycleDAO cycleDAO = new CycleDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("SelectNavigationCycle", "Trace Begin");
            retDS = cycleDAO.SelectNavigationCycle(NSID);
            HttpContext.Current.Trace.Write("SelectNavigationCycle", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int InsertNSProvider(string PCPName, bool? EWCPCP, string PCP_NPI, string PCP_Owner, string PCP_Location, string PCPAddress1,
                string PCPAddress2, string PCPCity, string PCPState, string PCPZip, string PCPContactFName, string PCPContactLName,
                string PCPContactTitle, string PCPContactTelephone, string PCPContactEmail, bool? Medical, int? MedicalSpecialty,
                string MedicalSpecialtyOther, int? ManagedCarePlan, string ManagedCarePlanOther)
        {
            int retValue = 0;
            CycleDAO cycleDAO = new CycleDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertNSProvider", "Trace Begin");
            retValue = cycleDAO.InsertNSProvider(PCPName, EWCPCP, PCP_NPI, PCP_Owner, PCP_Location, PCPAddress1,
            PCPAddress2, PCPCity, PCPState, PCPZip, PCPContactFName, PCPContactLName, PCPContactTitle, PCPContactTelephone,
            PCPContactEmail, Medical, MedicalSpecialty, MedicalSpecialtyOther, ManagedCarePlan, ManagedCarePlanOther);
            HttpContext.Current.Trace.Write("InsertNSProvider", "Trace End");

            return retValue;
        }


        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int UpdateNSProvider(int NSProviderID, string PCPName, bool? EWCPCP, string PCP_NPI, string PCP_Owner, string PCP_Location, string PCPAddress1,
                string PCPAddress2, string PCPCity, string PCPState, string PCPZip, string PCPContactFName, string PCPContactLName,
                string PCPContactTitle, string PCPContactTelephone, string PCPContactEmail, bool? Medical, int? MedicalSpecialty,
                string MedicalSpecialtyOther, int? ManagedCarePlan, string ManagedCarePlanOther)
        {
            int retValue = 0;
            CycleDAO cycleDAO = new CycleDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateNSProvider", "Trace Begin");
            retValue = cycleDAO.UpdateNSProvider(NSProviderID, PCPName, EWCPCP, PCP_NPI, PCP_Owner, PCP_Location, PCPAddress1,
            PCPAddress2, PCPCity, PCPState, PCPZip, PCPContactFName, PCPContactLName, PCPContactTitle, PCPContactTelephone,
            PCPContactEmail, Medical, MedicalSpecialty, MedicalSpecialtyOther, ManagedCarePlan, ManagedCarePlanOther);
            HttpContext.Current.Trace.Write("UpdateNSProvider", "Trace End");

            return retValue;
        }


        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int InsertNavigationCycle(int FK_RecipientID, int? FK_ProviderID, int? FK_ReferralID, int CancerSite, int NSProblem,
               int HealthProgram, int HealthInsuranceStatus, int? HealthInsurancePlan, string HealthInsurancePlanNumber,
               string ContactPrefDays, string ContactPrefHours, string ContactPrefHoursOther, int? ContactPrefType,
               bool? SRIndentifierCodeGenerated, int FK_NavigatorID)
        {
            int retValue = 0;
            CycleDAO cycleDAO = new CycleDAO();


            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertNavigationCycle", "Trace Begin");
            retValue = cycleDAO.InsertNavigationCycle(FK_RecipientID, FK_ProviderID, FK_ReferralID, CancerSite, NSProblem,
            HealthProgram, HealthInsuranceStatus, HealthInsurancePlan, HealthInsurancePlanNumber, ContactPrefDays,
            ContactPrefHours, ContactPrefHoursOther, ContactPrefType, SRIndentifierCodeGenerated, FK_NavigatorID);
            HttpContext.Current.Trace.Write("InsertNavigationCycle", "Trace End");

            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int UpdateNavigationCycle(int NavigationCycleID, int FK_RecipientID, int? FK_ProviderID, int? FK_ReferralID, int CancerSite, int NSProblem,
               int HealthProgram, int HealthInsuranceStatus, int? HealthInsurancePlan, string HealthInsurancePlanNumber,
               string ContactPrefDays, string ContactPrefHours, string ContactPrefHoursOther, int? ContactPrefType,
               bool? SRIndentifierCodeGenerated, int FK_NavigatorID)
        {
            int retValue = 0;
            CycleDAO cycleDAO = new CycleDAO();


            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateNavigationCycle", "Trace Begin");
            retValue = cycleDAO.UpdateNavigationCycle(NavigationCycleID, FK_RecipientID, FK_ProviderID, FK_ReferralID, CancerSite,
                NSProblem, HealthProgram, HealthInsuranceStatus, HealthInsurancePlan, HealthInsurancePlanNumber, ContactPrefDays,
            ContactPrefHours, ContactPrefHoursOther, ContactPrefType, SRIndentifierCodeGenerated, FK_NavigatorID);
            HttpContext.Current.Trace.Write("UpdateNavigationCycle", "Trace End");


            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int InsertEncounter(int FK_NavigatorID, int FK_RecipientID, int EncounterNumber,
            DateTime? EncounterDt, string EncounterStartTime, string EncounterEndTime, int EncounterType, int EncounterReason,
            bool? MissedAppointment, int? SvcOutcomeStatus, string SvcOutcomeStatusReason, DateTime? NextAppointmentDt,
            string NextAppointmentTime, int? NextAppointmentType, bool? ServicesTerminated, DateTime? ServicesTerminatedDt,
            string ServicesTerminatedReason, string Notes)
        {
            int retValue = 0;
            EncounterDAO encounterDAO = new EncounterDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertEncounter", "Trace Begin");
            retValue = encounterDAO.InsertEncounter(FK_NavigatorID, FK_RecipientID, EncounterNumber,
                EncounterDt, EncounterStartTime, EncounterEndTime, EncounterType, EncounterReason, MissedAppointment, SvcOutcomeStatus,
                SvcOutcomeStatusReason, NextAppointmentDt, NextAppointmentTime, NextAppointmentType, ServicesTerminated,
                ServicesTerminatedDt, ServicesTerminatedReason, Notes);
            HttpContext.Current.Trace.Write("InsertEncounter", "Trace End");


            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int UpdateEncounter(int EncounterID, int FK_NavigatorID, int FK_RecipientID, int EncounterNumber,
            DateTime? EncounterDt, string EncounterStartTime, string EncounterEndTime, int EncounterType, int EncounterReason,
            bool? MissedAppointment, int? SvcOutcomeStatus, string SvcOutcomeStatusReason, DateTime? NextAppointmentDt,
            string NextAppointmentTime, int? NextAppointmentType, bool? ServicesTerminated, DateTime? ServicesTerminatedDt,
            string ServicesTerminatedReason, string Notes)
        {
            int retValue = 0;
            EncounterDAO encounterDAO = new EncounterDAO();


            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateEncounter", "Trace Begin");
            retValue = encounterDAO.UpdateEncounter(EncounterID, FK_NavigatorID, FK_RecipientID, EncounterNumber,
                EncounterDt, EncounterStartTime, EncounterEndTime, EncounterType, EncounterReason, MissedAppointment, SvcOutcomeStatus,
                SvcOutcomeStatusReason, NextAppointmentDt, NextAppointmentTime, NextAppointmentType, ServicesTerminated,
                ServicesTerminatedDt, ServicesTerminatedReason, Notes);
            HttpContext.Current.Trace.Write("UpdateEncounter", "Trace End");

            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet SelectEncounter(int RecipientID)
        {
            DataSet retDS;
            EncounterDAO encounterDAO = new EncounterDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("SelectEncounter", "Trace Begin");
            retDS = encounterDAO.SelectEncounter(RecipientID);
            HttpContext.Current.Trace.Write("SelectEncounter", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int InsertBarrier(int FK_EncounterID, int? BarrierType, int? BarrierAssessed, DateTime FollowUpDt)
        {
            int retValue = 0;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertBarrier", "Trace Begin");
            retValue = barrierDAO.InsertBarrier(FK_EncounterID, BarrierType, BarrierAssessed, FollowUpDt);
            HttpContext.Current.Trace.Write("InsertBarrier", "Trace End");


            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int UpdateBarrier(int BarrierID, int FK_EncounterID, int? BarrierType, int? BarrierAssessed, DateTime FollowUpDt)
        {
            int retValue = 0;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateBarrier", "Trace Begin");
            retValue = barrierDAO.UpdateBarrier(BarrierID, FK_EncounterID, BarrierType, BarrierAssessed, FollowUpDt);
            HttpContext.Current.Trace.Write("UpdateBarrier", "Trace End");


            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int InsertSolution(int FK_BarrierID, int SolutionOffered, int SolutionImplementationStatus, DateTime? FollowUpDt)
        {
            int retValue = 0;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertSolution", "Trace Begin");
            retValue = barrierDAO.InsertSolution(FK_BarrierID, SolutionOffered, SolutionImplementationStatus, FollowUpDt);
            HttpContext.Current.Trace.Write("InsertSolution", "Trace End");


            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int UpdateSolution(int SolutionID, int FK_BarrierID, int SolutionOffered, int SolutionImplementationStatus, DateTime FollowUpDt)
        {
            int retValue = 0;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateSolution", "Trace Begin");
            retValue = barrierDAO.UpdateSolution(SolutionID, FK_BarrierID, SolutionOffered, SolutionImplementationStatus, FollowUpDt);
            HttpContext.Current.Trace.Write("UpdateSolution", "Trace End");


            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetSolutionsList(int BarrierID)
        {
            DataSet retDS;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetSolutionsList", "Trace Begin");
            retDS = barrierDAO.GetSolutionsList(BarrierID);
            HttpContext.Current.Trace.Write("GetSolutionsList", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int DeleteSolutions(int BarrierID)
        {
            int retVal;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetSolutionsList", "Trace Begin");
            retVal = barrierDAO.DeleteSolutions(BarrierID);
            HttpContext.Current.Trace.Write("GetSolutionsList", "Trace End");

            return retVal;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetBarriersList(int EncounterID)
        {
            DataSet retDS;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetBarriersList", "Trace Begin");
            retDS = barrierDAO.GetBarriersList(EncounterID);
            HttpContext.Current.Trace.Write("GetBarriersList", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetNavigationCycle(int mode, int NavigationCycleID, int NSRecipientID)
        {
            DataSet retDS;
            CycleDAO cycleDAO = new CycleDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetNavigationCycle", "Trace Begin");
            retDS = cycleDAO.GetNavigationCycle(mode, NavigationCycleID, NSRecipientID);
            HttpContext.Current.Trace.Write("GetNavigationCycle", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet SelectNSProvider(int NSProviderID)
        {
            DataSet retDS;
            CycleDAO cycleDAO = new CycleDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("SelectNSProvider", "Trace Begin");
            retDS = cycleDAO.SelectNSProvider(NSProviderID);
            HttpContext.Current.Trace.Write("SelectNSProvider", "Trace End");

            return retDS;
        }


        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetEncounter(int EncounterID)
        {
            DataSet retDS;
            EncounterDAO encounterDAO = new EncounterDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetEncounter", "Trace Begin");
            retDS = encounterDAO.GetEncounter(EncounterID);
            HttpContext.Current.Trace.Write("GetEncounter", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet SelectBarrierType()
        {
            DataSet retDS;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetBarrierType", "Trace Begin");
            retDS = barrierDAO.SelectBarrierType();
            HttpContext.Current.Trace.Write("GetBarrierType", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet SelectBarrierSolutions()
        {
            DataSet retDS;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetBarrierType", "Trace Begin");
            retDS = barrierDAO.SelectBarrierSolutions();
            HttpContext.Current.Trace.Write("GetBarrierType", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet SelectBarriers()
        {
            DataSet retDS;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetBarrierType", "Trace Begin");
            retDS = barrierDAO.SelectBarriers();
            HttpContext.Current.Trace.Write("GetBarrierType", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetBarrier(int BarrierID)
        {
            DataSet retDS;
            BarrierDAO barrierDAO = new BarrierDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetBarrier", "Trace Begin");
            retDS = barrierDAO.GetBarrier(BarrierID);
            HttpContext.Current.Trace.Write("GetBarrier", "Trace End");

            return retDS;
        }


        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int InsertUserAccess(string DomainName)
        {
            int retValue = 0;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertUserAccess", "Trace Begin");
            retValue = navigatorDAO.InsertUserAccess(DomainName);
            HttpContext.Current.Trace.Write("InsertUserAccess", "Trace End");


            return retValue;
        }

        [WebMethod]
        public DataSet GetCaregiverApprovalList()
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetCaregiverApprovalList", "Trace Begin");
            retDS = enrollmentDAO.GetCaregiverApprovalList();
            HttpContext.Current.Trace.Write("GetCaregiverApprovalList", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int SaveCaregiverApproval(int NSID, string ApprovalList, string ApprovalOther)
        {
            int retValue = 0;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            if (ApprovalList.Length <= 20 && ApprovalOther.Length <= 20)
            {
                //Start Trace for Adding Code to the database
                HttpContext.Current.Trace.Write("SaveCaregiverApproval", "Trace Begin");
                retValue = enrollmentDAO.SaveCaregiverApproval(NSID, ApprovalList, ApprovalOther);
                HttpContext.Current.Trace.Write("SaveCaregiverApproval", "Trace End");
            }
            else
            {
                throw new SecurityException("Parameter length exceeded maximum limit");

            }

            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetCaregiverApproval(int NSID)
        {
            DataSet retDS;
            EnrollmentDAO enrollmentDAO = new EnrollmentDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetCaregiverApproval", "Trace Begin");
            retDS = enrollmentDAO.GetCaregiverApproval(NSID);
            HttpContext.Current.Trace.Write("GetCaregiverApproval", "Trace End");

            return retDS;
        }


        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetNavigatorInfo(int mode, int NSRecipientID, int OldNavigatorID, int NewNavigatorID,
                        DateTime NavigatorTransferDt, int NavigatorTransferReason)
        {
            DataSet retDS;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetNavigatorInfo", "Trace Begin");
            retDS = navigatorDAO.GetNavigatorInfo(mode, NSRecipientID, OldNavigatorID, NewNavigatorID, NavigatorTransferDt, NavigatorTransferReason);
            HttpContext.Current.Trace.Write("GetNavigatorInfo", "Trace End");

            return retDS;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public int TransferNavigator(int mode, int NSRecipientID, int OldNavigatorID, int NewNavigatorID,
                        DateTime NavigatorTransferDt, string NavigatorTransferReason)
        {
            int retValue;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("TransferNavigator", "Trace Begin");
            retValue = navigatorDAO.TransferNavigator(mode, NSRecipientID, OldNavigatorID, NewNavigatorID, NavigatorTransferDt, NavigatorTransferReason);
            HttpContext.Current.Trace.Write("TransferNavigator", "Trace End");

            return retValue;
        }

        [WebMethod]
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "EWC_NSAA_Contributors, EWC_NSAA_A")]
        public DataSet GetNavigatorsList(int mode)
        {
            DataSet retDS;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetNavigatorsList", "Trace Begin");
            retDS = navigatorDAO.GetNavigatorsList(mode);
            HttpContext.Current.Trace.Write("GetNavigatorsList", "Trace End");

            return retDS;
        }

        [WebMethod]
        public int UpdateTransferStatus(int TransferID, int TransferStatus, string TransferRefusedReason,
                        DateTime TransferRefusedDt)
        {
            int retValue = 0;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateTransferStatus", "Trace Begin");
            retValue = navigatorDAO.UpdateTransferStatus(TransferID, TransferStatus, TransferRefusedReason, TransferRefusedDt);
            HttpContext.Current.Trace.Write("UpdateTransferStatus", "Trace End");


            return retValue;
        }

        [WebMethod]
        public DataSet GetRecipientReport(int mode, int NSRecipientID, int NSProviderID)
        {
            DataSet retDS;
            ReportDAO reportDAO = new ReportDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetRecipientReport", "Trace Begin");
            retDS = reportDAO.GetRecipientReport(mode, NSRecipientID, NSProviderID);
            HttpContext.Current.Trace.Write("GetRecipientReport", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet GetSearchRecipients(int? NSID, string LastName, DateTime? DOB)
        {
            DataSet retDS;
            SearchDAO searchDAO = new SearchDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetSearchRecipients", "Trace Begin");
            retDS = searchDAO.GetSearchRecipients(NSID, LastName, DOB);
            HttpContext.Current.Trace.Write("GetSearchRecipients", "Trace End");

            return retDS;
        }

        [WebMethod]
        public DataSet GetTrainingsList(int NavigatorID)
        {
            DataSet retDS;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetTrainingsList", "Trace Begin");
            retDS = navigatorDAO.GetTrainingsList(NavigatorID);
            HttpContext.Current.Trace.Write("GetTrainingsList", "Trace End");

            return retDS;
        }

        [WebMethod]
        public int InsertNavigatorTraining(int FK_NavigatorID, string CourseName, string Organization,
                        DateTime CompletionDt, bool Certificate)
        {
            int retValue = 0;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertNavigatorTraining", "Trace Begin");
            retValue = navigatorDAO.InsertNavigatorTraining(FK_NavigatorID, CourseName, Organization, CompletionDt, Certificate);
            HttpContext.Current.Trace.Write("InsertNavigatorTraining", "Trace End");


            return retValue;
        }

        [WebMethod]
        public int UpdateNavigatorTraining(int TrainingID, int FK_NavigatorID, string CourseName, string Organization,
                        DateTime CompletionDt, bool Certificate)
        {
            int retValue = 0;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateNavigatorTraining", "Trace Begin");
            retValue = navigatorDAO.UpdateNavigatorTraining(TrainingID, FK_NavigatorID, CourseName, Organization, CompletionDt, Certificate);
            HttpContext.Current.Trace.Write("UpdateNavigatorTraining", "Trace End");


            return retValue;
        }

        [WebMethod]
        public DataSet GetNavigatorLanguagesList(int NavigatorID)
        {
            DataSet retDS;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetNavigatorLanguagesList", "Trace Begin");
            retDS = navigatorDAO.GetNavigatorLanguagesList(NavigatorID);
            HttpContext.Current.Trace.Write("GetNavigatorLanguagesList", "Trace End");

            return retDS;
        }

        [WebMethod]
        public int InsertNavigatorLanguage(int FK_NavigatorID, int LanguageCode, int SpeakingScore,
                       int ListeningScore, int ReadingScore, int WritingScore)
        {
            int retValue = 0;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("InsertNavigatorLanguage", "Trace Begin");
            retValue = navigatorDAO.InsertNavigatorLanguage(FK_NavigatorID, LanguageCode, SpeakingScore, ListeningScore, ReadingScore, WritingScore);
            HttpContext.Current.Trace.Write("InsertNavigatorLanguage", "Trace End");


            return retValue;
        }

        [WebMethod]
        public int UpdateNavigatorLanguage(int NavigatorLanguageID, int FK_NavigatorID, int LanguageCode, int SpeakingScore,
                        int ListeningScore, int ReadingScore, int WritingScore)
        {
            int retValue = 0;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace for Adding Code to the database
            HttpContext.Current.Trace.Write("UpdateNavigatorLanguage", "Trace Begin");
            retValue = navigatorDAO.UpdateNavigatorLanguage(NavigatorLanguageID, FK_NavigatorID, LanguageCode, SpeakingScore, ListeningScore, ReadingScore, WritingScore);
            HttpContext.Current.Trace.Write("UpdateNavigatorLanguage", "Trace End");


            return retValue;
        }

        [WebMethod]
        public DataSet GetTransferReasonsList(int NavigatorID1, int NavigatorID2)
        {
            DataSet retDS;
            NavigatorDAO navigatorDAO = new NavigatorDAO();

            //Start Trace
            HttpContext.Current.Trace.Write("GetTransferReasonsList", "Trace Begin");
            retDS = navigatorDAO.GetTransferReasonsList(NavigatorID1, NavigatorID2);
            HttpContext.Current.Trace.Write("GetTransferReasonsList", "Trace End");

            return retDS;
        }

    }
}