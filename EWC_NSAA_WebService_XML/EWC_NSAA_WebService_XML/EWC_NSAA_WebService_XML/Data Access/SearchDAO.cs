﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Security;

    public class SearchDAO : BaseClass
    {

        #region Get Search Recipients

        /* -------------------------------------------------------------------------
    '   Function:       GetSearchRecipients
    '
    '   Description:    To get list of Recipients based on the Search criteria
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			10/27/2016		created
    '
    ' ------------------------------------------------------------------------- */

        public DataSet GetSearchRecipients(int? NSID, string LastName, DateTime? DOB)
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SearchRecipients");
                //Add input parameters for the stored procedure
                db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, NSID);
                db.AddInParameter(sqlCommand, "@LastName", SqlDbType.Char, LastName);
                if (DOB == DateTime.MinValue)
                {
                    db.AddInParameter(sqlCommand, "@DOB", SqlDbType.DateTime, DBNull.Value);
                }
                else 
                {
                    db.AddInParameter(sqlCommand, "@DOB", SqlDbType.DateTime, @DOB);
                }
                
                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("SearchDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("SearchDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

    }