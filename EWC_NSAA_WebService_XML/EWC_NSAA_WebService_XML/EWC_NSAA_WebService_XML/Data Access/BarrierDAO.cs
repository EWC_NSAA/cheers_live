﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Security;

/// <summary>
/// Summary description for BarrierDAO
/// </summary>
public class BarrierDAO : BaseClass
{

    #region Insert Barrier 

    /* -------------------------------------------------------------------------
    '   Function:       InsertBarrier
    '
    '   Description:    To insert a new record in tBarrier table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertBarrier(int FK_EncounterID, int? BarrierType, int? BarrierAssessed, DateTime FollowUpDt)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;
        int BarrierID = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertBarrier");
            //Add input parameters for the stored procedure
            db.AddOutParameter(sqlCommand, "BarrierID", SqlDbType.Int, -1);
            db.AddInParameter(sqlCommand, "@FK_EncounterID", SqlDbType.Int, FK_EncounterID);
            db.AddInParameter(sqlCommand, "@BarrierType", SqlDbType.Int, BarrierType);
            db.AddInParameter(sqlCommand, "@BarrierAssessed", SqlDbType.Int, BarrierAssessed);
            if (FollowUpDt == DateTime.MinValue)
            {
                db.AddInParameter(sqlCommand, "@FollowUpDt", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@FollowUpDt", SqlDbType.DateTime, FollowUpDt);
            }
            
            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            BarrierID = Convert.ToInt32(sqlCommand.Parameters["@BarrierID"].Value);



        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return BarrierID;
    }

    #endregion

    #region Update Barrier 

    /* -------------------------------------------------------------------------
    '   Function:       UpdateBarrier
    '
    '   Description:    To update a record in tBarrier table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateBarrier(int BarrierID, int FK_EncounterID, int? BarrierType, int? BarrierAssessed, DateTime FollowUpDt)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateBarrier");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@BarrierID", SqlDbType.Int, BarrierID);
            db.AddInParameter(sqlCommand, "@FK_EncounterID", SqlDbType.Int, FK_EncounterID);
            db.AddInParameter(sqlCommand, "@BarrierType", SqlDbType.Int, BarrierType);
            db.AddInParameter(sqlCommand, "@BarrierAssessed", SqlDbType.Int, BarrierAssessed);
            if (FollowUpDt == DateTime.MinValue)
            {
                db.AddInParameter(sqlCommand, "@FollowUpDt", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@FollowUpDt", SqlDbType.DateTime, FollowUpDt);
            }

            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);


        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Insert Solution 

    /* -------------------------------------------------------------------------
    '   Function:       InsertSolution
    '
    '   Description:    To insert a new record in tSolution table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertSolution(int FK_BarrierID, int SolutionOffered, int SolutionImplementationStatus, DateTime? FollowUpDt)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;
        
        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertSolution");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_BarrierID", SqlDbType.Int, FK_BarrierID);
            db.AddInParameter(sqlCommand, "@SolutionOffered", SqlDbType.Int, SolutionOffered);
            
            if (FollowUpDt == DateTime.MinValue)
            {
                db.AddInParameter(sqlCommand, "@FollowUpDt", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@FollowUpDt", SqlDbType.DateTime, FollowUpDt);
            }

            if (SolutionImplementationStatus == 0)
            {
                db.AddInParameter(sqlCommand, "@SolutionImplementationStatus", SqlDbType.Int, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@SolutionImplementationStatus", SqlDbType.Int, SolutionImplementationStatus);
            }


            
            
            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Update Solution 

    /* -------------------------------------------------------------------------
    '   Function:       UpdateSolution
    '
    '   Description:    To update a record in tSolution table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateSolution(int SolutionID, int FK_BarrierID, int SolutionOffered, int SolutionImplementationStatus, DateTime FollowUpDt)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateSolution");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@SolutionID", SqlDbType.Int, SolutionID);
            db.AddInParameter(sqlCommand, "@FK_BarrierID", SqlDbType.Int, FK_BarrierID);
            db.AddInParameter(sqlCommand, "@SolutionOffered", SqlDbType.Int, SolutionOffered);
            if (FollowUpDt == DateTime.MinValue)
            {
                db.AddInParameter(sqlCommand, "@FollowUpDt", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@FollowUpDt", SqlDbType.DateTime, FollowUpDt);
            }

            if (SolutionImplementationStatus == 0)
            {
                db.AddInParameter(sqlCommand, "@SolutionImplementationStatus", SqlDbType.Int, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@SolutionImplementationStatus", SqlDbType.Int, SolutionImplementationStatus);
            }


            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);


        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Get Barriers List

    /* -------------------------------------------------------------------------
    '   Function:       GetBarriersList
    '
    '   Description:    To get Barriers list from tBarriers table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetBarriersList(int EncounterID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectBarrierList");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_EncounterID", SqlDbType.Int, EncounterID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Solutions List

    /* -------------------------------------------------------------------------
    '   Function:       GetSolutionsList
    '
    '   Description:    To get Solutions list from tSolutions table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetSolutionsList(int BarrierID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectSolutionsList");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_BarrierID", SqlDbType.Int, BarrierID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Delete Solutions 

    /* -------------------------------------------------------------------------
    '   Function:       DeleteSolutions
    '
    '   Description:    To delete Solutions from tSolutions table for a Barrier
    '        					
    '   Parameters:     Barrier ID
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			8/17/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int DeleteSolutions(int BarrierID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retVal;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_DeleteSolutions");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_BarrierID", SqlDbType.Int, BarrierID);
            //Execute stored procedure
            retVal = db.ExecuteNonQuery(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retVal;
    }

    #endregion

    #region Select Barriers

    /* -------------------------------------------------------------------------
    '   Function:       SelectBarrier
    '
    '   Description:    To get list of Barrier records from trBarrier table 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet SelectBarriers()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectBarrier");
            //db.AddInParameter(sqlCommand, "@BarrierTypeCode", SqlDbType.Int, BarrierTypeCode);

            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Select Barrier Solutions

    /* -------------------------------------------------------------------------
    '   Function:       SelectBarrierSolutions
    '
    '   Description:    To get list of Solution records from trBarrierSolution table 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet SelectBarrierSolutions()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectBarrierSolution");
            //db.AddInParameter(sqlCommand, "@BarrierTypeCode", SqlDbType.Int, BarrierTypeCode);

            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Select Barrier Type

    /* -------------------------------------------------------------------------
    '   Function:       SelectBarrierType
    '
    '   Description:    To get list of records from trBarrierType table 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet SelectBarrierType()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectBarrierType");

            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Barrier

    /* -------------------------------------------------------------------------
    '   Function:       GetBarrier
    '
    '   Description:    To get single Barrier record 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetBarrier(int BarrierID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_GetBarrier");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@BarrierID", SqlDbType.Int, BarrierID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("BarrierDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

}