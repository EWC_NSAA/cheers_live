﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


    public class AssistanceDAO : BaseClass
    {

        #region Insert Assistance

        /* -------------------------------------------------------------------------
    '   Function:       InsertAssistance
    '
    '   Description:    To insert a new record in tAssistance table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			11/27/2017		created
    '
    ' ------------------------------------------------------------------------- */
        public int InsertAssistance(int FK_NavigatorID, int IncidentNumber, DateTime IncidentDt, int? FK_CommunicationCode, 
            string FirstName, string LastName, string Telephone, string Email, string RecipID, int FK_RequestorCode,
            string NPI, string NPIOwner, string NPISvcLoc, string BusinessName)
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            int retValue = 0;
            int AssistanceID = 0;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertAssistance");
                //Add input parameters for the stored procedure
                db.AddOutParameter(sqlCommand, "@AssistanceID", SqlDbType.Int, -1);
                db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
                db.AddInParameter(sqlCommand, "@IncidentNumber", SqlDbType.Int, IncidentNumber);
                db.AddInParameter(sqlCommand, "@IncidentDt", SqlDbType.DateTime, IncidentDt);
                db.AddInParameter(sqlCommand, "@FK_CommunicationCode", SqlDbType.Int, FK_CommunicationCode);
                db.AddInParameter(sqlCommand, "@FirstName", SqlDbType.Char, FirstName);
                db.AddInParameter(sqlCommand, "@LastName", SqlDbType.Char, LastName);
                db.AddInParameter(sqlCommand, "@Telephone", SqlDbType.Char, Telephone);
                db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);
                db.AddInParameter(sqlCommand, "@RecipID", SqlDbType.Char, RecipID);
                db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);
                db.AddInParameter(sqlCommand, "@FK_RequestorCode", SqlDbType.Int, FK_RequestorCode);
                db.AddInParameter(sqlCommand, "@NPI", SqlDbType.Char, NPI);
                db.AddInParameter(sqlCommand, "@NPIOwner", SqlDbType.Char, NPIOwner);
                db.AddInParameter(sqlCommand, "@NPISvcLoc", SqlDbType.Char, NPISvcLoc);
                db.AddInParameter(sqlCommand, "@BusinessName", SqlDbType.Char, BusinessName);



                //Execute stored procedure
                retValue = db.ExecuteNonQuery(sqlCommand);

                AssistanceID = Convert.ToInt32(sqlCommand.Parameters["@AssistanceID"].Value);



            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return AssistanceID;
        }

        #endregion

        #region Update Assistance

        /* -------------------------------------------------------------------------
    '   Function:       UpdateAssistance
    '
    '   Description:    To update a new record in tAssistance table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			11/27/2017		created
    '
    ' ------------------------------------------------------------------------- */
        public int UpdateAssistance(int AssistanceID, int FK_NavigatorID, int IncidentNumber, DateTime IncidentDt, int? FK_CommunicationCode,
            string FirstName, string LastName, string Telephone, string Email, string RecipID, int FK_RequestorCode,
            string NPI, string NPIOwner, string NPISvcLoc, string BusinessName)
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            int retValue = 0;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateAssistance");
                //Add input parameters for the stored procedure
                db.AddInParameter(sqlCommand, "@AssistanceID", SqlDbType.Int, AssistanceID);
                db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
                db.AddInParameter(sqlCommand, "@IncidentNumber", SqlDbType.Int, IncidentNumber);
                db.AddInParameter(sqlCommand, "@IncidentDt", SqlDbType.DateTime, IncidentDt);
                db.AddInParameter(sqlCommand, "@FK_CommunicationCode", SqlDbType.Int, FK_CommunicationCode);
                db.AddInParameter(sqlCommand, "@FirstName", SqlDbType.Char, FirstName);
                db.AddInParameter(sqlCommand, "@LastName", SqlDbType.Char, LastName);
                db.AddInParameter(sqlCommand, "@Telephone", SqlDbType.Char, Telephone);
                db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);
                db.AddInParameter(sqlCommand, "@RecipID", SqlDbType.Char, RecipID);
                db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);
                db.AddInParameter(sqlCommand, "@FK_RequestorCode", SqlDbType.Int, FK_RequestorCode);
                db.AddInParameter(sqlCommand, "@NPI", SqlDbType.Char, NPI);
                db.AddInParameter(sqlCommand, "@NPIOwner", SqlDbType.Char, NPIOwner);
                db.AddInParameter(sqlCommand, "@NPISvcLoc", SqlDbType.Char, NPISvcLoc);
                db.AddInParameter(sqlCommand, "@BusinessName", SqlDbType.Char, BusinessName);



                //Execute stored procedure
                retValue = db.ExecuteNonQuery(sqlCommand);

                
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return AssistanceID;
        }

        #endregion

        #region Get Assistance

        /* -------------------------------------------------------------------------
        '   Function:       GetAssistance
        '
        '   Description:    To get single Assistance record 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/27/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet GetAssistance(int AssistanceID)
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_GetAssistance");
                //Add input parameters for the stored procedure
                db.AddInParameter(sqlCommand, "@AssistanceID", SqlDbType.Int, AssistanceID);
                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistance
        '
        '   Description:    To get list of Assistance records from tAssistance table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistance()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceList");

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance Communication

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistanceCommunication
        '
        '   Description:    To get list of Assistance Communication records from trAssistanceCommunication table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistanceCommunication()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceCommunication");
                //db.AddInParameter(sqlCommand, "@BarrierTypeCode", SqlDbType.Int, BarrierTypeCode);

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance EWC Enrollee

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistanceEWCEnrollee
        '
        '   Description:    To get list of Assistance EWC Enrollee records from trAssistanceEWCEnrollee table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistanceEWCEnrollee()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceEWCEnrollee");

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance EWC PCP

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistanceEWCPCP
        '
        '   Description:    To get list of Assistance EWC PCP records from trAssistanceEWCPCP table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistanceEWCPCP()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceEWCPCP");

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance EWC Recipient

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistanceEWCRecipient
        '
        '   Description:    To get list of Assistance EWC Recipient records from trAssistanceEWCRecipient table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistanceEWCRecipient()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceEWCRecipient");

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance Referral Provider

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistanceReferralProvider
        '
        '   Description:    To get list of Assistance Referral Provider records from trAssistanceReferralProvider table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistanceReferralProvider()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceReferralProvider");

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance Requestor

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistanceRequestor
        '
        '   Description:    To get list of Assistance Requestor records from trAssistanceRequestor table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistanceRequestor()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceRequestor");

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance Resolution

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistanceResolution
        '
        '   Description:    To get list of Assistance Resolution records from trAssistanceResolution table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistanceResolution()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceResolution");

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance Issues List

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistanceIssuesList
        '
        '   Description:    To get list of Assistance Issues records from tAssistanceIssues table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistanceIssuesList()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceIssuesList");

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Select Assistance Resolution List

        /* -------------------------------------------------------------------------
        '   Function:       SelectAssistanceResolutionList
        '
        '   Description:    To get list of Assistance Resolutions records from tAssistanceResolution table 
        '        					
        '   Parameters:     None
        '
        '   Return Value:   DataSet
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/28/2017		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet SelectAssistanceResolutionList()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectAssistanceResolutionList");

                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("AssistanceDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

    }
