﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Security;

/// <summary>
/// Summary description for NavigatorDAO
/// </summary>
public class NavigatorDAO : BaseClass
{

    #region Insert User Access

    /* -------------------------------------------------------------------------
    '   Function:       InsertUserAccess
    '
    '   Description:    To insert a new record in tUserAccess table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertUserAccess(string DomainName)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertUserAccess");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@DomainName", SqlDbType.Char, DomainName);
            
            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Insert Navigator

    /* -------------------------------------------------------------------------
    '   Function:       InsertNavigator
    '
    '   Description:    To insert a new record in tNavigator table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertNavigator(string DomainName, int Region, int Type, string LastName, string FirstName, 
                        string Address1, string Address2, string City, string State, string Zip,
                        string BusinessTelephone, string MobileTelephone, string Email)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertNavigator");
            //Add input parameters for the stored procedure
            db.AddOutParameter(sqlCommand, "@NavigatorID", SqlDbType.Int, -1);
            db.AddInParameter(sqlCommand, "@DomainName", SqlDbType.Char, DomainName);
            db.AddInParameter(sqlCommand, "@Region", SqlDbType.Int, Region);
            db.AddInParameter(sqlCommand, "@Type", SqlDbType.Int, Type);
            db.AddInParameter(sqlCommand, "@LastName", SqlDbType.Char, LastName);
            db.AddInParameter(sqlCommand, "@FirstName", SqlDbType.Char, FirstName);
            db.AddInParameter(sqlCommand, "@Address1", SqlDbType.Char, Address1);
            db.AddInParameter(sqlCommand, "@Address2", SqlDbType.Char, Address2);
            db.AddInParameter(sqlCommand, "@City", SqlDbType.Char, City);
            db.AddInParameter(sqlCommand, "@State", SqlDbType.Char, State);
            db.AddInParameter(sqlCommand, "@Zip", SqlDbType.Char, Zip);
            db.AddInParameter(sqlCommand, "@BusinessTelephone", SqlDbType.Char, BusinessTelephone);
            db.AddInParameter(sqlCommand, "@MobileTelephone", SqlDbType.Char, MobileTelephone);
            db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);

            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            int NavigatorID = Convert.ToInt32(sqlCommand.Parameters["@NavigatorID"].Value);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Update Navigator

    /* -------------------------------------------------------------------------
    '   Function:       UpdateNavigator
    '
    '   Description:    To update a Navigator record in tNavigator table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateNavigator(int NavigatorID, string DomainName, int Region, int Type, string LastName, string FirstName,
                        string Address1, string Address2, string City, string State, string Zip,
                        string BusinessTelephone, string MobileTelephone, string Email)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateNavigator");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@NavigatorID", SqlDbType.Int, NavigatorID);
            db.AddInParameter(sqlCommand, "@DomainName", SqlDbType.Char, DomainName);
            db.AddInParameter(sqlCommand, "@Region", SqlDbType.Int, Region);
            db.AddInParameter(sqlCommand, "@Type", SqlDbType.Int, Type);
            db.AddInParameter(sqlCommand, "@LastName", SqlDbType.Char, LastName);
            db.AddInParameter(sqlCommand, "@FirstName", SqlDbType.Char, FirstName);
            db.AddInParameter(sqlCommand, "@Address1", SqlDbType.Char, Address1);
            db.AddInParameter(sqlCommand, "@Address2", SqlDbType.Char, Address2);
            db.AddInParameter(sqlCommand, "@City", SqlDbType.Char, City);
            db.AddInParameter(sqlCommand, "@State", SqlDbType.Char, State);
            db.AddInParameter(sqlCommand, "@Zip", SqlDbType.Char, Zip);
            db.AddInParameter(sqlCommand, "@BusinessTelephone", SqlDbType.Char, BusinessTelephone);
            db.AddInParameter(sqlCommand, "@MobileTelephone", SqlDbType.Char, MobileTelephone);
            db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);

            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Get Navigator Info

    /* -------------------------------------------------------------------------
    '   Function:       GetNavigatorInfo
    '
    '   Description:    To select Navigator details from tNavigator
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			8/09/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetNavigatorInfo(int mode, int NSRecipientID, int OldNavigatorID, int NewNavigatorID,
                        DateTime NavigatorTransferDt, int NavigatorTransferReason)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_NavigatorTransfer");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@mode", SqlDbType.Int, mode);
            db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, NSRecipientID);
            db.AddInParameter(sqlCommand, "@OldNavigatorID", SqlDbType.Int, OldNavigatorID);
            db.AddInParameter(sqlCommand, "@NewNavigatorID", SqlDbType.Int, DBNull.Value);
            db.AddInParameter(sqlCommand, "@NavigatorTransferDt", SqlDbType.DateTime, DBNull.Value);
            db.AddInParameter(sqlCommand, "@NavigatorTransferReason", SqlDbType.Int, DBNull.Value);
            

            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Transfer Navigator

    /* -------------------------------------------------------------------------
    '   Function:       TransferNavigator
    '
    '   Description:    To transfer Navigator details from one Navigator to another
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			8/09/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int TransferNavigator(int mode, int NSRecipientID, int OldNavigatorID, int NewNavigatorID,
                        DateTime NavigatorTransferDt, string NavigatorTransferReason)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_NavigatorTransfer");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@mode", SqlDbType.Int, mode);
            db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, NSRecipientID);
            db.AddInParameter(sqlCommand, "@OldNavigatorID", SqlDbType.Int, OldNavigatorID);
            db.AddInParameter(sqlCommand, "@NewNavigatorID", SqlDbType.Int, NewNavigatorID);
            db.AddInParameter(sqlCommand, "@NavigatorTransferDt", SqlDbType.DateTime, NavigatorTransferDt);
            db.AddInParameter(sqlCommand, "@NavigatorTransferReason", SqlDbType.VarChar, NavigatorTransferReason);


            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Get Navigators List

    /* -------------------------------------------------------------------------
        '   Function:       GetNavigatorsList
        '
        '   Description:    To get navigators list from Lookup tables
        '        					
        '   Parameters:     None
        '
        '   Return Value:   Dataset
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			8/09/2016		created
        '
        ' ------------------------------------------------------------------------- */

    public DataSet GetNavigatorsList(int mode)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_NavigatorTransfer");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@mode", SqlDbType.Int, mode);
            db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, DBNull.Value);
            db.AddInParameter(sqlCommand, "@OldNavigatorID", SqlDbType.Int, DBNull.Value);
            db.AddInParameter(sqlCommand, "@NewNavigatorID", SqlDbType.Int, DBNull.Value);
            db.AddInParameter(sqlCommand, "@NavigatorTransferDt", SqlDbType.DateTime, DBNull.Value);
            db.AddInParameter(sqlCommand, "@NavigatorTransferReason", SqlDbType.Int, DBNull.Value);

            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Update Transfer Status

    /* -------------------------------------------------------------------------
    '   Function:       UpdateTransferStatus
    '
    '   Description:    To update a Navigator transfer record in tNavigatorTransfer table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateTransferStatus(int TransferID, int TransferStatus, string TransferRefusedReason,
                        DateTime TransferRefusedDt)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateTransferStatus");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@TransferID", SqlDbType.Int, TransferID);
            db.AddInParameter(sqlCommand, "@TransferStatus", SqlDbType.Int, TransferStatus);
            db.AddInParameter(sqlCommand, "@TransferRefusedReason", SqlDbType.Char, TransferRefusedReason);
            db.AddInParameter(sqlCommand, "@TransferRefusedDt", SqlDbType.DateTime, TransferRefusedDt);

            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Get Trainings List

    /* -------------------------------------------------------------------------
        '   Function:       GetTrainingsList
        '
        '   Description:    To get trainings list from Navigator training table
        '        					
        '   Parameters:     None
        '
        '   Return Value:   Dataset
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			11/15/2016		created
        '
        ' ------------------------------------------------------------------------- */

    public DataSet GetTrainingsList(int NavigatorID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_GetTrainingsList");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, NavigatorID);
            
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Insert Navigator Training

    /* -------------------------------------------------------------------------
    '   Function:       InsertNavigatorTraining
    '
    '   Description:    To insert a new record in tNavigatorTraining table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake		    11/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertNavigatorTraining(int FK_NavigatorID, string CourseName, string Organization,
                        DateTime CompletionDt, bool Certificate)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertNavigatorTraining");
            //Add input parameters for the stored procedure
            //db.AddOutParameter(sqlCommand, "@TrainingID", SqlDbType.Int, -1);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            db.AddInParameter(sqlCommand, "@CourseName", SqlDbType.Char, CourseName);
            db.AddInParameter(sqlCommand, "@Organization", SqlDbType.Char, Organization);
            db.AddInParameter(sqlCommand, "@CompletionDt", SqlDbType.DateTime, CompletionDt);
            db.AddInParameter(sqlCommand, "@Certificate", SqlDbType.Bit, Certificate);
            
            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            //int TrainingID = Convert.ToInt32(sqlCommand.Parameters["@TrainingID"].Value);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Update Navigator Training

    /* -------------------------------------------------------------------------
    '   Function:       UpdateNavigatorTraining
    '
    '   Description:    To update a record in tNavigatorTraining table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			11/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateNavigatorTraining(int TrainingID, int FK_NavigatorID, string CourseName, string Organization,
                        DateTime CompletionDt, bool Certificate)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateNavigatorTraining");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@TrainingID", SqlDbType.Int, TrainingID);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            db.AddInParameter(sqlCommand, "@CourseName", SqlDbType.Char, CourseName);
            db.AddInParameter(sqlCommand, "@Organization", SqlDbType.Char, Organization);
            db.AddInParameter(sqlCommand, "@CompletionDt", SqlDbType.DateTime, CompletionDt);
            db.AddInParameter(sqlCommand, "@Certificate", SqlDbType.Bit, Certificate);

            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Get Languages List

    /* -------------------------------------------------------------------------
        '   Function:       GetLanguagesList
        '
        '   Description:    To get languages list from Navigator languages table
        '        					
        '   Parameters:     None
        '
        '   Return Value:   Dataset
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			12/01/2016		created
        '
        ' ------------------------------------------------------------------------- */

    public DataSet GetNavigatorLanguagesList(int NavigatorID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_GetLanguagesList");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, NavigatorID);

            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Insert Navigator Language

    /* -------------------------------------------------------------------------
    '   Function:       InsertNavigatorLanguage
    '
    '   Description:    To insert a new record in tnavigatorLanguages table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake		    12/12/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertNavigatorLanguage(int FK_NavigatorID, int LanguageCode, int SpeakingScore,
                        int ListeningScore, int ReadingScore, int WritingScore)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertNavigatorLanguage");
            //Add input parameters for the stored procedure
            //db.AddOutParameter(sqlCommand, "@TrainingID", SqlDbType.Int, -1);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            db.AddInParameter(sqlCommand, "@LanguageCode", SqlDbType.Int, LanguageCode);
            db.AddInParameter(sqlCommand, "@SpeakingScore", SqlDbType.Int, SpeakingScore);
            db.AddInParameter(sqlCommand, "@ListeningScore", SqlDbType.Int, ListeningScore);
            db.AddInParameter(sqlCommand, "@ReadingScore", SqlDbType.Int, ReadingScore);
            db.AddInParameter(sqlCommand, "@WritingScore", SqlDbType.Int, WritingScore);

            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            //int TrainingID = Convert.ToInt32(sqlCommand.Parameters["@TrainingID"].Value);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Update Navigator Language

    /* -------------------------------------------------------------------------
    '   Function:       UpdateNavigatorLanguage
    '
    '   Description:    To update a record in tNavigatorLanguages table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			12/12/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateNavigatorLanguage(int NavigatorLanguageID, int FK_NavigatorID, int LanguageCode, int SpeakingScore,
                        int ListeningScore, int ReadingScore, int WritingScore)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateNavigatorLanguage");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@NavigatorLanguageID", SqlDbType.Int, NavigatorLanguageID);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            db.AddInParameter(sqlCommand, "@LanguageCode", SqlDbType.Int, LanguageCode);
            db.AddInParameter(sqlCommand, "@SpeakingScore", SqlDbType.Int, SpeakingScore);
            db.AddInParameter(sqlCommand, "@ListeningScore", SqlDbType.Int, ListeningScore);
            db.AddInParameter(sqlCommand, "@ReadingScore", SqlDbType.Int, ReadingScore);
            db.AddInParameter(sqlCommand, "@WritingScore", SqlDbType.Int, WritingScore);

            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Get Transfer Reasons List

    /* -------------------------------------------------------------------------
    '   Function:       GetTransferReasonsList
    '
    '   Description:    To get transfer reasons from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			12/19/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetTransferReasonsList(int NavigatorID1, int NavigatorID2)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectTransferReasons");
            db.AddInParameter(sqlCommand, "@NavigatorID1", SqlDbType.Int, NavigatorID1);
            db.AddInParameter(sqlCommand, "@NavigatorID2", SqlDbType.Int, NavigatorID2);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("NavigatorDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

}