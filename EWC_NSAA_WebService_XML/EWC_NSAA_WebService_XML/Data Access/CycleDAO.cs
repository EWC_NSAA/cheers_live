﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Security;

/// <summary>
/// Summary description for CycleDAO
/// </summary>
public class CycleDAO : BaseClass
{

    #region Get Cancer Sites List

    /* -------------------------------------------------------------------------
    '   Function:       GetCancerSitesList
    '
    '   Description:    To get cancers sites list from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetCancerSitesList()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectCancerSite");
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CyclesDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CyclesDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get NS Problems List

    /* -------------------------------------------------------------------------
    '   Function:       GetNSProblemsList
    '
    '   Description:    To get NS Problems list from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetNSProblemsList()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectNSProblem");
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CyclesDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CyclesDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Health Programs List

    /* -------------------------------------------------------------------------
    '   Function:       GetHealthProgramsList
    '
    '   Description:    To get health programs list from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetHealthProgramsList()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectHealthProgram");
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CyclesDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CyclesDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Health Insurance List

    /* -------------------------------------------------------------------------
    '   Function:       GetHealthInsuranceList
    '
    '   Description:    To get health Insurance list from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetHealthInsuranceList()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectHealthInsurance");
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CyclesDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CyclesDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Insert Provider

    /* -------------------------------------------------------------------------
    '   Function:       InsertProvider
    '
    '   Description:    To insert a new record in tNSProvider table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertNSProvider(string PCPName, bool? EWCPCP, string PCP_NPI, string PCP_Owner, string PCP_Location, string PCPAddress1,
            string PCPAddress2, string PCPCity, string PCPState, string PCPZip, string PCPContactFName, string PCPContactLName,
            string PCPContactTitle, string PCPContactTelephone, string PCPContactEmail, bool? Medical, int? MedicalSpecialty, 
            string MedicalSpecialtyOther, int? ManagedCarePlan, string ManagedCarePlanOther)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;
        int NSProviderID = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertNSProvider");
            //Add input parameters for the stored procedure
            db.AddOutParameter(sqlCommand, "NSProviderID", SqlDbType.Int, -1);
            db.AddInParameter(sqlCommand, "@PCPName", SqlDbType.Char, PCPName);
            db.AddInParameter(sqlCommand, "@EWCPCP", SqlDbType.Bit, EWCPCP);
            db.AddInParameter(sqlCommand, "@PCP_NPI", SqlDbType.Char, PCP_NPI);
            db.AddInParameter(sqlCommand, "@PCP_Owner", SqlDbType.Char, PCP_Owner);
            db.AddInParameter(sqlCommand, "@PCP_Location", SqlDbType.Char, PCP_Location);
            db.AddInParameter(sqlCommand, "@PCPAddress1", SqlDbType.Char, PCPAddress1);
            db.AddInParameter(sqlCommand, "@PCPAddress2", SqlDbType.Char, PCPAddress2);
            db.AddInParameter(sqlCommand, "@PCPCity", SqlDbType.Char, PCPCity);
            db.AddInParameter(sqlCommand, "@PCPState", SqlDbType.Char, PCPState);
            db.AddInParameter(sqlCommand, "@PCPZip", SqlDbType.Char, PCPZip);
            db.AddInParameter(sqlCommand, "@PCPContactFName", SqlDbType.Char, PCPContactFName);
            db.AddInParameter(sqlCommand, "@PCPContactLName", SqlDbType.Char, PCPContactLName);
            db.AddInParameter(sqlCommand, "@PCPContactTitle", SqlDbType.Char, PCPContactTitle);
            db.AddInParameter(sqlCommand, "@PCPContactTelephone", SqlDbType.Char, PCPContactTelephone);
            db.AddInParameter(sqlCommand, "@PCPContactEmail", SqlDbType.Char, PCPContactEmail);
            db.AddInParameter(sqlCommand, "@Medical", SqlDbType.Bit, Medical);
            db.AddInParameter(sqlCommand, "@MedicalSpecialty", SqlDbType.Int, MedicalSpecialty);
            db.AddInParameter(sqlCommand, "@MedicalSpecialtyOther", SqlDbType.Char, MedicalSpecialtyOther);
            db.AddInParameter(sqlCommand, "@ManagedCarePlan", SqlDbType.Int, ManagedCarePlan);
            db.AddInParameter(sqlCommand, "@ManagedCarePlanOther", SqlDbType.Char, ManagedCarePlanOther);


            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            NSProviderID = Convert.ToInt32(sqlCommand.Parameters["@NSProviderID"].Value);



        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return NSProviderID;
    }

    #endregion

    #region Update Provider

    /* -------------------------------------------------------------------------
    '   Function:       UpdateProvider
    '
    '   Description:    To update a record in tNSProvider table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateNSProvider(int NSProviderID, string PCPName, bool? EWCPCP, string PCP_NPI, string PCP_Owner, string PCP_Location, string PCPAddress1,
            string PCPAddress2, string PCPCity, string PCPState, string PCPZip, string PCPContactFName, string PCPContactLName,
            string PCPContactTitle, string PCPContactTelephone, string PCPContactEmail, bool? Medical, int? MedicalSpecialty,
            string MedicalSpecialtyOther, int? ManagedCarePlan, string ManagedCarePlanOther)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;
        
        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateNSProvider");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@NSProviderID", SqlDbType.Int, NSProviderID);
            db.AddInParameter(sqlCommand, "@PCPName", SqlDbType.Char, PCPName);
            db.AddInParameter(sqlCommand, "@EWCPCP", SqlDbType.Bit, EWCPCP);
            db.AddInParameter(sqlCommand, "@PCP_NPI", SqlDbType.Char, PCP_NPI);
            db.AddInParameter(sqlCommand, "@PCP_Owner", SqlDbType.Char, PCP_Owner);
            db.AddInParameter(sqlCommand, "@PCP_Location", SqlDbType.Char, PCP_Location);
            db.AddInParameter(sqlCommand, "@PCPAddress1", SqlDbType.Char, PCPAddress1);
            db.AddInParameter(sqlCommand, "@PCPAddress2", SqlDbType.Char, PCPAddress2);
            db.AddInParameter(sqlCommand, "@PCPCity", SqlDbType.Char, PCPCity);
            db.AddInParameter(sqlCommand, "@PCPState", SqlDbType.Char, PCPState);
            db.AddInParameter(sqlCommand, "@PCPZip", SqlDbType.Char, PCPZip);
            db.AddInParameter(sqlCommand, "@PCPContactFName", SqlDbType.Char, PCPContactFName);
            db.AddInParameter(sqlCommand, "@PCPContactLName", SqlDbType.Char, PCPContactLName);
            db.AddInParameter(sqlCommand, "@PCPContactTitle", SqlDbType.Char, PCPContactTitle);
            db.AddInParameter(sqlCommand, "@PCPContactTelephone", SqlDbType.Char, PCPContactTelephone);
            db.AddInParameter(sqlCommand, "@PCPContactEmail", SqlDbType.Char, PCPContactEmail);
            db.AddInParameter(sqlCommand, "@Medical", SqlDbType.Bit, Medical);
            db.AddInParameter(sqlCommand, "@MedicalSpecialty", SqlDbType.Int, MedicalSpecialty);
            db.AddInParameter(sqlCommand, "@MedicalSpecialtyOther", SqlDbType.Char, MedicalSpecialtyOther);
            db.AddInParameter(sqlCommand, "@ManagedCarePlan", SqlDbType.Int, ManagedCarePlan);
            db.AddInParameter(sqlCommand, "@ManagedCarePlanOther", SqlDbType.Char, ManagedCarePlanOther);


            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            NSProviderID = Convert.ToInt32(sqlCommand.Parameters["@NSProviderID"].Value);



        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return NSProviderID;
    }

    #endregion

    #region Select Navigation Cycle

    /* -------------------------------------------------------------------------
    '   Function:       SelectNavigationCycle
    '
    '   Description:    To get list of Navigation Cycle records based on Recipient ID 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet SelectNavigationCycle(int FK_NSRecipientID)
    { 
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectNavigationCycle");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_RecipientID", SqlDbType.Int, FK_NSRecipientID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Insert Navigation Cycle

    /* -------------------------------------------------------------------------
    '   Function:       InsertNavigationCycle
    '
    '   Description:    To insert a new record in tNavigationCycle table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertNavigationCycle(int FK_RecipientID, int? FK_ProviderID, int? FK_ReferralID, int CancerSite, int NSProblem,
           int HealthProgram, int HealthInsuranceStatus, int? HealthInsurancePlan, string HealthInsurancePlanNumber,
           string ContactPrefDays, string ContactPrefHours, string ContactPrefHoursOther, int? ContactPrefType, 
           bool? SRIndentifierCodeGenerated, int FK_NavigatorID)
   {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;
        int NavigationCycleID = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertNavigationCycle");
            //Add input parameters for the stored procedure
            db.AddOutParameter(sqlCommand, "NavigationCycleID", SqlDbType.Int, -1);
            db.AddInParameter(sqlCommand, "@FK_RecipientID", SqlDbType.Int, FK_RecipientID);
            db.AddInParameter(sqlCommand, "@FK_ProviderID", SqlDbType.Int, FK_ProviderID);
            db.AddInParameter(sqlCommand, "@FK_ReferralID", SqlDbType.Int, FK_ReferralID);
            db.AddInParameter(sqlCommand, "@CancerSite", SqlDbType.Int, CancerSite);
            db.AddInParameter(sqlCommand, "@NSProblem", SqlDbType.Int, NSProblem);
            db.AddInParameter(sqlCommand, "@HealthProgram", SqlDbType.Int, HealthProgram);
            db.AddInParameter(sqlCommand, "@HealthInsuranceStatus", SqlDbType.Int, HealthInsuranceStatus);
            db.AddInParameter(sqlCommand, "@HealthInsurancePlan", SqlDbType.Int, HealthInsurancePlan);
            db.AddInParameter(sqlCommand, "@HealthInsurancePlanNumber", SqlDbType.Char, HealthInsurancePlanNumber);
            db.AddInParameter(sqlCommand, "@ContactPrefDays", SqlDbType.Char, ContactPrefDays);
            db.AddInParameter(sqlCommand, "@ContactPrefHours", SqlDbType.Char, ContactPrefHours);
            db.AddInParameter(sqlCommand, "@ContactPrefHoursOther", SqlDbType.Char, ContactPrefHoursOther);
            db.AddInParameter(sqlCommand, "@ContactPrefType", SqlDbType.Int, ContactPrefType);
            db.AddInParameter(sqlCommand, "@SRIndentifierCodeGenerated", SqlDbType.Int, SRIndentifierCodeGenerated);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            
            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            NavigationCycleID = Convert.ToInt32(sqlCommand.Parameters["@NavigationCycleID"].Value);



        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return NavigationCycleID;
    }

    #endregion

    #region Update Navigation Cycle

    /* -------------------------------------------------------------------------
    '   Function:       UpdateNavigationCycle
    '
    '   Description:    To update a record in tNSProvider table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateNavigationCycle(int NSProviderID, int FK_RecipientID, int? FK_ProviderID, int? FK_ReferralID, int CancerSite, int NSProblem,
           int HealthProgram, int HealthInsuranceStatus, int? HealthInsurancePlan, string HealthInsurancePlanNumber,
           string ContactPrefDays, string ContactPrefHours, string ContactPrefHoursOther, int? ContactPrefType,
           bool? SRIndentifierCodeGenerated, int FK_NavigatorID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;
        
        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateNavigationCycle");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@NavigationCycleID", SqlDbType.Int, NSProviderID);
            db.AddInParameter(sqlCommand, "@FK_RecipientID", SqlDbType.Int, FK_RecipientID);
            db.AddInParameter(sqlCommand, "@FK_ProviderID", SqlDbType.Int, FK_ProviderID);
            db.AddInParameter(sqlCommand, "@FK_ReferralID", SqlDbType.Int, FK_ReferralID);
            db.AddInParameter(sqlCommand, "@CancerSite", SqlDbType.Int, CancerSite);
            db.AddInParameter(sqlCommand, "@NSProblem", SqlDbType.Int, NSProblem);
            db.AddInParameter(sqlCommand, "@HealthProgram", SqlDbType.Int, HealthProgram);
            db.AddInParameter(sqlCommand, "@HealthInsuranceStatus", SqlDbType.Int, HealthInsuranceStatus);
            db.AddInParameter(sqlCommand, "@HealthInsurancePlan", SqlDbType.Int, HealthInsurancePlan);
            db.AddInParameter(sqlCommand, "@HealthInsurancePlanNumber", SqlDbType.Char, HealthInsurancePlanNumber);
            db.AddInParameter(sqlCommand, "@ContactPrefDays", SqlDbType.Char, ContactPrefDays);
            db.AddInParameter(sqlCommand, "@ContactPrefHours", SqlDbType.Char, ContactPrefHours);
            db.AddInParameter(sqlCommand, "@ContactPrefHoursOther", SqlDbType.Char, ContactPrefHoursOther);
            db.AddInParameter(sqlCommand, "@ContactPrefType", SqlDbType.Int, ContactPrefType);
            db.AddInParameter(sqlCommand, "@SRIndentifierCodeGenerated", SqlDbType.Int, SRIndentifierCodeGenerated);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);


            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Get Navigation Cycle

    /* -------------------------------------------------------------------------
    '   Function:       GetNavigationCycle
    '
    '   Description:    To get single Navigation Cycle record 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetNavigationCycle(int mode, int NavigationCycleID, int NSRecipientID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_GetNavigationCycle");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@mode", SqlDbType.Int, mode);
            db.AddInParameter(sqlCommand, "@NavigationCycleID", SqlDbType.Int, NavigationCycleID);
            db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, NSRecipientID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Select NS Provider

    /* -------------------------------------------------------------------------
    '   Function:       SelectNSProvider
    '
    '   Description:    To get list of NSProvider records based on NS ProviderID
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet SelectNSProvider(int NSProviderID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectNSProvider");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@NSProviderID", SqlDbType.Int, NSProviderID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion


}