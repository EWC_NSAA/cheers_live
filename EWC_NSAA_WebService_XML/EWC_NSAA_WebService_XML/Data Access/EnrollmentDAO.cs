﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Security;


/// <summary>
/// Summary description for EnrollmentDAO
/// </summary>
public class EnrollmentDAO : BaseClass
{

    #region Get Countries List

        /* -------------------------------------------------------------------------
        '   Function:       GetCountriesList
        '
        '   Description:    To get countries list from Lookup tables
        '        					
        '   Parameters:     None
        '
        '   Return Value:   Dataset
        '
        '   
        '   Side Effects:	
        '
        '	Modifications:
        '		By						Date			Purpose
        '		-----------------		--------		-------------------------------
        '		Bilwa Buchake			6/20/2016		created
        '
        ' ------------------------------------------------------------------------- */

        public DataSet GetCountriesList()
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectCountries");
                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

    #endregion

    #region Get Languages List

    /* -------------------------------------------------------------------------
    '   Function:       GetLanguagesList
    '
    '   Description:    To get languages from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetLanguagesList()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectLanguages");
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Navigator ID

    /* -------------------------------------------------------------------------
    '   Function:       GetNavigatorID
    '
    '   Description:    To get NavigatorID from domain name
    '        					
    '   Parameters:     None
    '
    '   Return Value:   int
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetNavigator(string strDomainName)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectNavigator");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@DomainName", SqlDbType.Char, strDomainName);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Race List

    /* -------------------------------------------------------------------------
    '   Function:       GetRaceList
    '
    '   Description:    To get Race from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetRaceList()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectRace");
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Recipients List

    /* -------------------------------------------------------------------------
    '   Function:       GetRecipientsList
    '
    '   Description:    To get Recipients list from tServiceRecipients table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetRecipientsList(int NavigatorID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectServiceRecipient");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, NavigatorID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Insert Service Recipient

    /* -------------------------------------------------------------------------
    '   Function:       InsertServiceRecipient
    '
    '   Description:    To insert a new record in tServiceRecipients table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertServiceRecipient(int FK_NavigatorID, string FK_RecipID, string LastName, string FirstName, string MiddleInitial,
                        DateTime? DOB, bool? AddressNotAvailable, string Address1, string Address2, string City, string State, string Zip,
                        string Phone1, int P1PhoneType, bool P1PersonalMessage, string Phone2, int? P2PhoneType, bool? P2PersonalMessage,
                        string MotherMaidenName, string Email, string SSN, bool? ImmigrantStatus, int? CountryOfOrigin, string Gender, bool? Ethnicity, 
                        int PrimaryLanguage, bool AgreeToSpeakEnglish, string CaregiverName, int? Relationship, string RelationshipOther, 
                        string CaregiverPhone, int? CaregiverPhoneType, bool? CaregiverPhonePersonalMessage, string CaregiverEmail, 
                        int? CaregiverContactPreference, string Comments)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;
        int NSRecipientID = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertServiceRecipient");
            //Add input parameters for the stored procedure
            db.AddOutParameter(sqlCommand, "NSRecipientID", SqlDbType.Int, -1);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            db.AddInParameter(sqlCommand, "@FK_RecipID", SqlDbType.Char, FK_RecipID);
            db.AddInParameter(sqlCommand, "@LastName", SqlDbType.Char, LastName);
            db.AddInParameter(sqlCommand, "@FirstName", SqlDbType.Char, FirstName);
            db.AddInParameter(sqlCommand, "@MiddleInitial", SqlDbType.Char, MiddleInitial);
            db.AddInParameter(sqlCommand, "@DOB", SqlDbType.DateTime, DOB);
            db.AddInParameter(sqlCommand, "@AddressNotAvailable", SqlDbType.Bit, AddressNotAvailable);
            db.AddInParameter(sqlCommand, "@Address1", SqlDbType.Char, Address1);
            db.AddInParameter(sqlCommand, "@Address2", SqlDbType.Char, Address2);
            db.AddInParameter(sqlCommand, "@City", SqlDbType.Char, City);
            db.AddInParameter(sqlCommand, "@State", SqlDbType.Char, State);
            db.AddInParameter(sqlCommand, "@Zip", SqlDbType.Char, Zip);
            db.AddInParameter(sqlCommand, "@Phone1", SqlDbType.Char, Phone1);
            db.AddInParameter(sqlCommand, "@P1PhoneType", SqlDbType.Int, P1PhoneType);
            db.AddInParameter(sqlCommand, "@P1PersonalMessage", SqlDbType.Bit, P1PersonalMessage);
            db.AddInParameter(sqlCommand, "@Phone2", SqlDbType.Char, Phone2);
            db.AddInParameter(sqlCommand, "@P2PhoneType", SqlDbType.Int, P2PhoneType);
            db.AddInParameter(sqlCommand, "@P2PersonalMessage", SqlDbType.Bit, P2PersonalMessage);
            db.AddInParameter(sqlCommand, "@MotherMaidenName", SqlDbType.Char, MotherMaidenName);
            db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);
            db.AddInParameter(sqlCommand, "@SSN", SqlDbType.Char, SSN);
            db.AddInParameter(sqlCommand, "@ImmigrantStatus", SqlDbType.Bit, ImmigrantStatus);
            db.AddInParameter(sqlCommand, "@CountryOfOrigin", SqlDbType.Char, CountryOfOrigin);
            db.AddInParameter(sqlCommand, "@Gender", SqlDbType.Char, Gender);
            db.AddInParameter(sqlCommand, "@Ethnicity", SqlDbType.Bit, Ethnicity);
            db.AddInParameter(sqlCommand, "@PrimaryLanguage", SqlDbType.Int, PrimaryLanguage);
            db.AddInParameter(sqlCommand, "@AgreeToSpeakEnglish", SqlDbType.Bit, AgreeToSpeakEnglish);
            db.AddInParameter(sqlCommand, "@CaregiverName", SqlDbType.Char, CaregiverName);
            db.AddInParameter(sqlCommand, "@Relationship", SqlDbType.Int, Relationship);
            db.AddInParameter(sqlCommand, "@RelationshipOther", SqlDbType.Char, RelationshipOther);
            db.AddInParameter(sqlCommand, "@CaregiverPhone", SqlDbType.Char, CaregiverPhone);
            db.AddInParameter(sqlCommand, "@CaregiverPhoneType", SqlDbType.Int, CaregiverPhoneType);
            db.AddInParameter(sqlCommand, "@CaregiverPhonePersonalMessage", SqlDbType.Bit, CaregiverPhonePersonalMessage);
            db.AddInParameter(sqlCommand, "@CaregiverEmail", SqlDbType.Char, CaregiverEmail);
            db.AddInParameter(sqlCommand, "@CaregiverContactPreference", SqlDbType.Int, CaregiverContactPreference);
            db.AddInParameter(sqlCommand, "@Comments", SqlDbType.Char, Comments);

            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            NSRecipientID = Convert.ToInt32(sqlCommand.Parameters["@NSRecipientID"].Value);
            
            

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return NSRecipientID;
    }

    #endregion

    #region Update Service Recipient

    /* -------------------------------------------------------------------------
    '   Function:       UpdateServiceRecipient
    '
    '   Description:    To update a record in tServiceRecipients table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateServiceRecipient(int NSRecipientID, int FK_NavigatorID, string FK_RecipID, string LastName, string FirstName, string MiddleInitial,
                        DateTime? DOB, bool? AddressNotAvailable, string Address1, string Address2, string City, string State, string Zip,
                        string Phone1, int P1PhoneType, bool P1PersonalMessage, string Phone2, int? P2PhoneType, bool? P2PersonalMessage,
                        string MotherMaidenName, string Email, string SSN, bool? ImmigrantStatus, int? CountryOfOrigin, string Gender, bool? Ethnicity,
                        int PrimaryLanguage, bool AgreeToSpeakEnglish, string CaregiverName, int? Relationship, string RelationshipOther,
                        string CaregiverPhone, int? CaregiverPhoneType, bool? CaregiverPhonePersonalMessage, string CaregiverEmail,
                        int? CaregiverContactPreference, string Comments)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateServiceRecipient");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, NSRecipientID);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            db.AddInParameter(sqlCommand, "@FK_RecipID", SqlDbType.Char, FK_RecipID);
            db.AddInParameter(sqlCommand, "@LastName", SqlDbType.Char, LastName);
            db.AddInParameter(sqlCommand, "@FirstName", SqlDbType.Char, FirstName);
            db.AddInParameter(sqlCommand, "@MiddleInitial", SqlDbType.Char, MiddleInitial);
            db.AddInParameter(sqlCommand, "@DOB", SqlDbType.DateTime, DOB);
            db.AddInParameter(sqlCommand, "@AddressNotAvailable", SqlDbType.Bit, AddressNotAvailable);
            db.AddInParameter(sqlCommand, "@Address1", SqlDbType.Char, Address1);
            db.AddInParameter(sqlCommand, "@Address2", SqlDbType.Char, Address2);
            db.AddInParameter(sqlCommand, "@City", SqlDbType.Char, City);
            db.AddInParameter(sqlCommand, "@State", SqlDbType.Char, State);
            db.AddInParameter(sqlCommand, "@Zip", SqlDbType.Char, Zip);
            db.AddInParameter(sqlCommand, "@Phone1", SqlDbType.Char, Phone1);
            db.AddInParameter(sqlCommand, "@P1PhoneType", SqlDbType.Int, P1PhoneType);
            db.AddInParameter(sqlCommand, "@P1PersonalMessage", SqlDbType.Bit, P1PersonalMessage);
            db.AddInParameter(sqlCommand, "@Phone2", SqlDbType.Char, Phone2);
            db.AddInParameter(sqlCommand, "@P2PhoneType", SqlDbType.Int, P2PhoneType);
            db.AddInParameter(sqlCommand, "@P2PersonalMessage", SqlDbType.Bit, P2PersonalMessage);
            db.AddInParameter(sqlCommand, "@MotherMaidenName", SqlDbType.Char, MotherMaidenName);
            db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);
            db.AddInParameter(sqlCommand, "@SSN", SqlDbType.Char, SSN);
            db.AddInParameter(sqlCommand, "@ImmigrantStatus", SqlDbType.Bit, ImmigrantStatus);
            db.AddInParameter(sqlCommand, "@CountryOfOrigin", SqlDbType.Char, CountryOfOrigin);
            db.AddInParameter(sqlCommand, "@Gender", SqlDbType.Char, Gender);
            db.AddInParameter(sqlCommand, "@Ethnicity", SqlDbType.Bit, Ethnicity);
            db.AddInParameter(sqlCommand, "@PrimaryLanguage", SqlDbType.Int, PrimaryLanguage);
            db.AddInParameter(sqlCommand, "@AgreeToSpeakEnglish", SqlDbType.Bit, AgreeToSpeakEnglish);
            db.AddInParameter(sqlCommand, "@CaregiverName", SqlDbType.Char, CaregiverName);
            db.AddInParameter(sqlCommand, "@Relationship", SqlDbType.Int, Relationship);
            db.AddInParameter(sqlCommand, "@RelationshipOther", SqlDbType.Char, RelationshipOther);
            db.AddInParameter(sqlCommand, "@CaregiverPhone", SqlDbType.Char, CaregiverPhone);
            db.AddInParameter(sqlCommand, "@CaregiverPhoneType", SqlDbType.Int, CaregiverPhoneType);
            db.AddInParameter(sqlCommand, "@CaregiverPhonePersonalMessage", SqlDbType.Bit, CaregiverPhonePersonalMessage);
            db.AddInParameter(sqlCommand, "@CaregiverEmail", SqlDbType.Char, CaregiverEmail);
            db.AddInParameter(sqlCommand, "@CaregiverContactPreference", SqlDbType.Int, CaregiverContactPreference);
            db.AddInParameter(sqlCommand, "@Comments", SqlDbType.Char, Comments);

            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Save Race

    /* -------------------------------------------------------------------------
    '   Function:       SaveRace
    '
    '   Description:    To save Race to Race table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   int
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int SaveRace(int NSID, string RaceList, string RaceOther)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SaveRace");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_NSRecipientID", SqlDbType.Int, NSID);
            db.AddInParameter(sqlCommand, "@RaceCodeList", SqlDbType.Char, RaceList);
            db.AddInParameter(sqlCommand, "@RaceOther", SqlDbType.Char, RaceOther);
            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Get Race

    /* -------------------------------------------------------------------------
    '   Function:       GetRace
    '
    '   Description:    To get Race values for a Recipient 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetRace(int NSID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_GetRace");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, NSID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Service Recipient

    /* -------------------------------------------------------------------------
    '   Function:       GetServiceRecipient
    '
    '   Description:    To get single Recipient record 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetServiceRecipient(int NSID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_GetRecipient");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, NSID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Select Service Recipient

    /* -------------------------------------------------------------------------
    '   Function:       SelectServiceRecipient
    '
    '   Description:    To get list of Recipient records based on Navigator ID 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/20/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet SelectServiceRecipient(int FK_NavigatorID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectServiceRecipient");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Asian Race List

    /* -------------------------------------------------------------------------
    '   Function:       GetAsianRaceList
    '
    '   Description:    To get Asian Race list from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			7/7/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetRaceAsianList()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectRaceAsian");
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Pac Islander Race List

    /* -------------------------------------------------------------------------
    '   Function:       GetPacIslanderRaceList
    '
    '   Description:    To get Pac Islander Race list from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			7/7/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetRacePacIslanderList()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectRacePacIslander");
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Caregiver Approval List

    /* -------------------------------------------------------------------------
    '   Function:       GetCaregiverApprovalList
    '
    '   Description:    To get Caregiver Approval from Lookup tables
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			7/12/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetCaregiverApprovalList()
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectCaregiverApproval");
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Save Caregiver Approval

    /* -------------------------------------------------------------------------
    '   Function:       SaveCaregiverApproval
    '
    '   Description:    To save Caregiver Approval to table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   int
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			7/12/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int SaveCaregiverApproval(int NSID, string ApprovalList, string ApprovalOther)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SaveCaregiverApproval");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_NSRecipientID", SqlDbType.Int, NSID);
            db.AddInParameter(sqlCommand, "@ApprovalCodeList", SqlDbType.Char, ApprovalList);
            db.AddInParameter(sqlCommand, "@ApprovalOther", SqlDbType.Char, ApprovalOther);
            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Get Caregiver Approval

    /* -------------------------------------------------------------------------
    '   Function:       GetCaregiverApproval
    '
    '   Description:    To get Caregiver Approval values for a Recipient 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			7/13/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetCaregiverApproval(int NSID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("GetCaregiverApproval");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, NSID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EnrollmentDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

}