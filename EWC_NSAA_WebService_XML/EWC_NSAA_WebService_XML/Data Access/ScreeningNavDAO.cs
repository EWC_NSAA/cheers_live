﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Security;


     public class ScreeningNavDAO : BaseClass
    {

        #region Insert ScreeningNav

        /* -------------------------------------------------------------------------
    '   Function:       InsertScreeningNav
    '
    '   Description:    To insert a new record in tScreeningNav table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

        public int InsertScreeningNav(int FK_NavigatorID, string SN_Code1, int SN_Code2, string LastName, string FirstName, DateTime? DOB, string Address1,
         string Address2, string City, string State, string Zip, string Email, string Cellphone, string HomeTelephone, bool Computer,
         string TextMessage, DateTime? DateOfContact1, string ApptScreen1, DateTime? SvcDate1, string SvcType1, string Response1, 
         DateTime? DateOfContact2, string ApptScreen2, DateTime? SvcDate2, string SvcType2, string Response2, string NSEnrollment,
         DateTime? EnrollmentDate )
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            int retValue = 0;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertScreeningNav");
                //Add input parameters for the stored procedure
                db.AddOutParameter(sqlCommand, "@SN_ID", SqlDbType.Int, -1);
                db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
                db.AddInParameter(sqlCommand, "@SN_CODE1", SqlDbType.Char, SN_Code1);
                db.AddInParameter(sqlCommand, "@SN_CODE2", SqlDbType.Int, SN_Code2);
                db.AddInParameter(sqlCommand, "@LastName", SqlDbType.Char, LastName);
                db.AddInParameter(sqlCommand, "@FirstName", SqlDbType.Char, FirstName);
                db.AddInParameter(sqlCommand, "@DOB", SqlDbType.DateTime, DOB);
                db.AddInParameter(sqlCommand, "@Address1", SqlDbType.Char, Address1);
                db.AddInParameter(sqlCommand, "@Address2", SqlDbType.Char, Address2);
                db.AddInParameter(sqlCommand, "@City", SqlDbType.Char, City);
                db.AddInParameter(sqlCommand, "@State", SqlDbType.Char, State);
                db.AddInParameter(sqlCommand, "@Zip", SqlDbType.Char, Zip);
                db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);
                db.AddInParameter(sqlCommand, "@HomeTelephone", SqlDbType.Char, HomeTelephone);
                db.AddInParameter(sqlCommand, "@Cellphone", SqlDbType.Char, Cellphone);
                db.AddInParameter(sqlCommand, "@Computer", SqlDbType.Bit, Computer);
                db.AddInParameter(sqlCommand, "@TextMessage", SqlDbType.Char, TextMessage);
                db.AddInParameter(sqlCommand, "@DateOfContact1", SqlDbType.DateTime, DateOfContact1);
                db.AddInParameter(sqlCommand, "@ApptScreen1", SqlDbType.Char, ApptScreen1);
                db.AddInParameter(sqlCommand, "@SvcDate1", SqlDbType.DateTime, SvcDate1);
                db.AddInParameter(sqlCommand, "@SvcType1", SqlDbType.Char, SvcType1);
                db.AddInParameter(sqlCommand, "@Response1", SqlDbType.Char, Response1);
                db.AddInParameter(sqlCommand, "@DateOfContact2", SqlDbType.DateTime, DateOfContact2);
                db.AddInParameter(sqlCommand, "@ApptScreen2", SqlDbType.Char, ApptScreen2);
                db.AddInParameter(sqlCommand, "@SvcDate2", SqlDbType.DateTime, SvcDate2);
                db.AddInParameter(sqlCommand, "@SvcType2", SqlDbType.Char, SvcType2);
                db.AddInParameter(sqlCommand, "@Response2", SqlDbType.Char, Response2);
                db.AddInParameter(sqlCommand, "@NSEnrollment", SqlDbType.Char, NSEnrollment);
                db.AddInParameter(sqlCommand, "@EnrollmentDate", SqlDbType.DateTime, EnrollmentDate);
                

                //Execute stored procedure
                retValue = db.ExecuteNonQuery(sqlCommand);

                int SN_ID = Convert.ToInt32(sqlCommand.Parameters["@SN_ID"].Value);

            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("ScreeningNavDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("ScreeningNavDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retValue;
        }

        #endregion

        #region Update Screening Nav

        /* -------------------------------------------------------------------------
    '   Function:       UpdateScreeningNav
    '
    '   Description:    To update a Screening Nav record in tScreeningNav table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			7/31/2017		created
    '
    ' ------------------------------------------------------------------------- */

        public int UpdateScreeningNav(int SN_ID, int FK_NavigatorID, string SN_Code1, int SN_Code2, string LastName, string FirstName, DateTime? DOB, string Address1,
         string Address2, string City, string State, string Zip, string Email, string Cellphone, string HomeTelephone, bool Computer,
         string TextMessage, DateTime? DateOfContact1, string ApptScreen1, DateTime? SvcDate1, string SvcType1, string Response1,
         DateTime? DateOfContact2, string ApptScreen2, DateTime? SvcDate2, string SvcType2, string Response2, string NSEnrollment,
         DateTime? EnrollmentDate)
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            int retValue = 0;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateScreeningNav");
                //Add input parameters for the stored procedure
                db.AddInParameter(sqlCommand, "@SN_ID", SqlDbType.Int, SN_ID);
                db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
                db.AddInParameter(sqlCommand, "@SN_CODE1", SqlDbType.Char, SN_Code1);
                db.AddInParameter(sqlCommand, "@SN_CODE2", SqlDbType.Int, SN_Code2);
                db.AddInParameter(sqlCommand, "@LastName", SqlDbType.Char, LastName);
                db.AddInParameter(sqlCommand, "@FirstName", SqlDbType.Char, FirstName);
                db.AddInParameter(sqlCommand, "@DOB", SqlDbType.DateTime, DOB);
                db.AddInParameter(sqlCommand, "@Address1", SqlDbType.Char, Address1);
                db.AddInParameter(sqlCommand, "@Address2", SqlDbType.Char, Address2);
                db.AddInParameter(sqlCommand, "@City", SqlDbType.Char, City);
                db.AddInParameter(sqlCommand, "@State", SqlDbType.Char, State);
                db.AddInParameter(sqlCommand, "@Zip", SqlDbType.Char, Zip);
                db.AddInParameter(sqlCommand, "@Email", SqlDbType.Char, Email);
                db.AddInParameter(sqlCommand, "@HomeTelephone", SqlDbType.Char, HomeTelephone);
                db.AddInParameter(sqlCommand, "@Cellphone", SqlDbType.Char, Cellphone);
                db.AddInParameter(sqlCommand, "@Computer", SqlDbType.Bit, Computer);
                db.AddInParameter(sqlCommand, "@TextMessage", SqlDbType.Char, TextMessage);
                db.AddInParameter(sqlCommand, "@DateOfContact1", SqlDbType.DateTime, DateOfContact1);
                db.AddInParameter(sqlCommand, "@ApptScreen1", SqlDbType.Char, ApptScreen1);
                db.AddInParameter(sqlCommand, "@SvcDate1", SqlDbType.DateTime, SvcDate1);
                db.AddInParameter(sqlCommand, "@SvcType1", SqlDbType.Char, SvcType1);
                db.AddInParameter(sqlCommand, "@Response1", SqlDbType.Char, Response1);
                db.AddInParameter(sqlCommand, "@DateOfContact2", SqlDbType.DateTime, DateOfContact2);
                db.AddInParameter(sqlCommand, "@ApptScreen2", SqlDbType.Char, ApptScreen2);
                db.AddInParameter(sqlCommand, "@SvcDate2", SqlDbType.DateTime, SvcDate2);
                db.AddInParameter(sqlCommand, "@SvcType2", SqlDbType.Char, SvcType2);
                db.AddInParameter(sqlCommand, "@Response2", SqlDbType.Char, Response2);
                db.AddInParameter(sqlCommand, "@NSEnrollment", SqlDbType.Char, NSEnrollment);
                db.AddInParameter(sqlCommand, "@EnrollmentDate", SqlDbType.DateTime, EnrollmentDate);

                //Execute stored procedure
                retValue = db.ExecuteNonQuery(sqlCommand);

            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("ScreeningNavDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("ScreeningNavDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retValue;
        }

        #endregion

        #region Select Screening Nav

        /* -------------------------------------------------------------------------
    '   Function:       SelectScreeningNav
    '
    '   Description:    To get list of Screening Nav records based on SN_ID 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			7/31/2017		created
    '
    ' ------------------------------------------------------------------------- */

        public DataSet SelectScreeningNav(int SN_ID)
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectScreeningNav");
                //Add input parameters for the stored procedure
                db.AddInParameter(sqlCommand, "@SN_ID", SqlDbType.Int, SN_ID);
                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("ScreeningNavDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("ScreeningNavDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

        #region Get Screening Nav

        /* -------------------------------------------------------------------------
    '   Function:       GetScreeningNav
    '
    '   Description:    To get list of Screening Nav records based on FK_NavigatorID
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			7/31/2017		created
    '
    ' ------------------------------------------------------------------------- */

        public DataSet GetScreeningNav(int FK_NavigatorID)
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_GetScreeningNav");
                //Add input parameters for the stored procedure
                db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("ScreeningNavDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("ScreeningNavDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion
    }
