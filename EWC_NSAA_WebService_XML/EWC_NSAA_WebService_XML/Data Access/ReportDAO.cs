﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Security;


    public class ReportDAO : BaseClass
    {

        #region Get Recipient Report

        /* -------------------------------------------------------------------------
    '   Function:       GetRecipientReport
    '
    '   Description:    To get required data from various tables for the Recipient Report
    '        					
    '   Parameters:     None
    '
    '   Return Value:   Dataset
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			10/06/2016		created
    '
    ' ------------------------------------------------------------------------- */

        public DataSet GetRecipientReport(int mode, int NSRecipientID, int NSProviderID)
        {
            DBConnection DbConnection = new DBConnection();
            //Connect to the database
            SqlDatabase db = DbConnection.GetDatabase();

            SqlCommand sqlCommand;
            DataSet retDS;

            try
            {
                //Get the stored procedure to execute
                sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_RecipientReport");
                //Add input parameters for the stored procedure
                db.AddInParameter(sqlCommand, "@mode", SqlDbType.Int, mode);
                db.AddInParameter(sqlCommand, "@NSRecipientID", SqlDbType.Int, NSRecipientID);
                db.AddInParameter(sqlCommand, "@NSProviderID", SqlDbType.Int, NSProviderID);
                //Execute stored procedure
                retDS = db.ExecuteDataSet(sqlCommand);
            }
            catch (SqlException SqlEx)
            {
                throw RaiseException("ReportDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
            }
            catch (Exception ex)
            {
                throw RaiseException("ReportDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
            }

            return retDS;
        }

        #endregion

    }
