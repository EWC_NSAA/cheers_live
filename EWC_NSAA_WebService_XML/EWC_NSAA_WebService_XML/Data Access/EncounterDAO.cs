﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Configuration;
using System.Security;

/// <summary>
/// Summary description for EncounterDAO
/// </summary>
public class EncounterDAO : BaseClass
{

    #region Insert Encounter

    /* -------------------------------------------------------------------------
    '   Function:       InsertEncounter
    '
    '   Description:    To insert a new record in tEncounter table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int InsertEncounter(int FK_NavigatorID, int FK_RecipientID, int EncounterNumber,
        DateTime? EncounterDt, string EncounterStartTime, string EncounterEndTime, int EncounterType, int EncounterReason,
        bool? MissedAppointment, int? SvcOutcomeStatus, string SvcOutcomeStatusReason, DateTime? NextAppointmentDt,
        string NextAppointmentTime, int? NextAppointmentType, bool? ServicesTerminated, DateTime? ServicesTerminatedDt,
        string ServicesTerminatedReason, string Notes)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;
        int EncounterID = 0;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_InsertEncounter");
            //Add input parameters for the stored procedure
            db.AddOutParameter(sqlCommand, "EncounterID", SqlDbType.Int, -1);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            db.AddInParameter(sqlCommand, "@FK_RecipientID", SqlDbType.Int, FK_RecipientID);
            db.AddInParameter(sqlCommand, "@EncounterNumber", SqlDbType.Int, EncounterNumber);
            db.AddInParameter(sqlCommand, "@EncounterDt", SqlDbType.DateTime, EncounterDt);
            db.AddInParameter(sqlCommand, "@EncounterStartTime", SqlDbType.Char, EncounterStartTime);
            db.AddInParameter(sqlCommand, "@EncounterEndTime", SqlDbType.Char, EncounterEndTime);
            db.AddInParameter(sqlCommand, "@EncounterType", SqlDbType.Int, EncounterType);
            db.AddInParameter(sqlCommand, "@EncounterReason", SqlDbType.Int, EncounterReason);
            db.AddInParameter(sqlCommand, "@MissedAppointment", SqlDbType.Bit, MissedAppointment);
            db.AddInParameter(sqlCommand, "@SvcOutcomeStatus", SqlDbType.Bit, SvcOutcomeStatus);
            db.AddInParameter(sqlCommand, "@SvcOutcomeStatusReason", SqlDbType.VarChar, SvcOutcomeStatusReason);
            if (NextAppointmentDt == DateTime.MinValue)
            {
                db.AddInParameter(sqlCommand, "@NextAppointmentDt", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@NextAppointmentDt", SqlDbType.DateTime, NextAppointmentDt);
            }
            
            db.AddInParameter(sqlCommand, "@NextAppointmentTime", SqlDbType.Char, NextAppointmentTime);
            db.AddInParameter(sqlCommand, "@NextAppointmentType", SqlDbType.Int, NextAppointmentType);
            db.AddInParameter(sqlCommand, "@ServicesTerminated", SqlDbType.Bit, ServicesTerminated);
            if (ServicesTerminatedDt == DateTime.MinValue)
            {
                db.AddInParameter(sqlCommand, "@ServicesTerminatedDt", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@ServicesTerminatedDt", SqlDbType.DateTime, ServicesTerminatedDt);
            }
            
            db.AddInParameter(sqlCommand, "@ServicesTerminatedReason", SqlDbType.VarChar, ServicesTerminatedReason);
            db.AddInParameter(sqlCommand, "@Notes", SqlDbType.Char, Notes);


            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);

            EncounterID = Convert.ToInt32(sqlCommand.Parameters["@EncounterID"].Value);



        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return EncounterID;
    }

    #endregion

    #region Update Encounter

    /* -------------------------------------------------------------------------
    '   Function:       UpdateEncounter
    '
    '   Description:    To update a record in tEncounter table
    '        					
    '   Parameters:     None
    '
    '   Return Value:   None
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public int UpdateEncounter(int EncounterID, int FK_NavigatorID, int FK_RecipientID, int EncounterNumber,
        DateTime? EncounterDt, string EncounterStartTime, string EncounterEndTime, int EncounterType, int EncounterReason,
        bool? MissedAppointment, int? SvcOutcomeStatus, string SvcOutcomeStatusReason, DateTime? NextAppointmentDt,
        string NextAppointmentTime, int? NextAppointmentType, bool? ServicesTerminated, DateTime? ServicesTerminatedDt,
        string ServicesTerminatedReason, string Notes)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        int retValue = 0;
        
        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_UpdateEncounter");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "EncounterID", SqlDbType.Int, EncounterID);
            db.AddInParameter(sqlCommand, "@FK_NavigatorID", SqlDbType.Int, FK_NavigatorID);
            db.AddInParameter(sqlCommand, "@FK_RecipientID", SqlDbType.Int, FK_RecipientID);
            db.AddInParameter(sqlCommand, "@EncounterNumber", SqlDbType.Int, EncounterNumber);
            db.AddInParameter(sqlCommand, "@EncounterDt", SqlDbType.DateTime, EncounterDt);
            db.AddInParameter(sqlCommand, "@EncounterStartTime", SqlDbType.Char, EncounterStartTime);
            db.AddInParameter(sqlCommand, "@EncounterEndTime", SqlDbType.Char, EncounterEndTime);
            db.AddInParameter(sqlCommand, "@EncounterType", SqlDbType.Int, EncounterType);
            db.AddInParameter(sqlCommand, "@EncounterReason", SqlDbType.Int, EncounterReason);
            db.AddInParameter(sqlCommand, "@MissedAppointment", SqlDbType.Bit, MissedAppointment);
            db.AddInParameter(sqlCommand, "@SvcOutcomeStatus", SqlDbType.Bit, SvcOutcomeStatus);
            db.AddInParameter(sqlCommand, "@SvcOutcomeStatusReason", SqlDbType.VarChar, SvcOutcomeStatusReason);
            if (NextAppointmentDt == DateTime.MinValue)
            {
                db.AddInParameter(sqlCommand, "@NextAppointmentDt", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@NextAppointmentDt", SqlDbType.DateTime, NextAppointmentDt);
            }
            db.AddInParameter(sqlCommand, "@NextAppointmentTime", SqlDbType.Char, NextAppointmentTime);
            db.AddInParameter(sqlCommand, "@NextAppointmentType", SqlDbType.Int, NextAppointmentType);
            db.AddInParameter(sqlCommand, "@ServicesTerminated", SqlDbType.Bit, ServicesTerminated);
            if (ServicesTerminatedDt == DateTime.MinValue)
            {
                db.AddInParameter(sqlCommand, "@ServicesTerminatedDt", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                db.AddInParameter(sqlCommand, "@ServicesTerminatedDt", SqlDbType.DateTime, ServicesTerminatedDt);
            }
            db.AddInParameter(sqlCommand, "@ServicesTerminatedReason", SqlDbType.VarChar, ServicesTerminatedReason);
            db.AddInParameter(sqlCommand, "@Notes", SqlDbType.Char, Notes);


            //Execute stored procedure
            retValue = db.ExecuteNonQuery(sqlCommand);


        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("CycleDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retValue;
    }

    #endregion

    #region Select Encounter

    /* -------------------------------------------------------------------------
    '   Function:       SelectEncounter
    '
    '   Description:    To get list of Encounter records based on Navigation Cycle ID 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet SelectEncounter(int FK_RecipientID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_SelectEncounter");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@FK_RecipientID", SqlDbType.Int, FK_RecipientID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EncounterDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EncounterDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

    #region Get Encounter

    /* -------------------------------------------------------------------------
    '   Function:       GetEncounter
    '
    '   Description:    To get single Encounter record 
    '        					
    '   Parameters:     None
    '
    '   Return Value:   DataSet
    '
    '   
    '   Side Effects:	
    '
    '	Modifications:
    '		By						Date			Purpose
    '		-----------------		--------		-------------------------------
    '		Bilwa Buchake			6/21/2016		created
    '
    ' ------------------------------------------------------------------------- */

    public DataSet GetEncounter(int EncounterID)
    {
        DBConnection DbConnection = new DBConnection();
        //Connect to the database
        SqlDatabase db = DbConnection.GetDatabase();

        SqlCommand sqlCommand;
        DataSet retDS;

        try
        {
            //Get the stored procedure to execute
            sqlCommand = (SqlCommand)db.GetStoredProcCommand("usp_GetEncounter");
            //Add input parameters for the stored procedure
            db.AddInParameter(sqlCommand, "@EncounterID", SqlDbType.Int, EncounterID);
            //Execute stored procedure
            retDS = db.ExecuteDataSet(sqlCommand);
        }
        catch (SqlException SqlEx)
        {
            throw RaiseException("EncounterDAO.vb", "EWC_NSAA_Service", SqlEx.Message, ErrorCode.StoredProcedureExecutionFailed.ToString(), SqlEx.Source, (int)FaultCode.Server);
        }
        catch (Exception ex)
        {
            throw RaiseException("EncounterDAO.vb", "EWC_NSAA_Service", ex.Message, ErrorCode.UnknownException.ToString(), ex.Source, (int)FaultCode.Server);
        }

        return retDS;
    }

    #endregion

}