﻿using EWC_NSAA.Models;
using EWC_NSAA.RaceCBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.CaregiverApprovalCBL.Models;
//Navigation Cycles ViewModel
using EWC_NSAA.ContactPreferenceCBL.Models;

namespace EWC_NSAA.ViewModels
{
   
    public class RecipientViewModel
    {
        //RecipientViewModel Properties
        public tServiceRecipient tServiceRecipient { get; set; }
        public tRace tRace { get; set; }
        public IEnumerable<Race> AvailableRace { get; set; }
        public IEnumerable<Race> SelectedRace { get; set; }
        public PostedRace postedRace { get; set; }
        public int NSRecipientID { get; set; }  
        public int RaceAsianCode { get; set; }
        public int RacePacIslanderCode { get; set; }
        public tCaregiverApprovals tCaregiverApprovals { get; set; }
        public IEnumerable<CaregiverApprovals> AvailableApprovals { get; set; }
        public IEnumerable<CaregiverApprovals> SelectedApprovals { get; set; }
        public PostedCaregiverApproval postedCaregiverApproval { get; set; }
        public string RaceOther { get; set; }
        
        //NavigationViewModel Properties
        public tNavigationCycle tNavigationCycle { get; set; }
        public IEnumerable<WeekdayContactPreference> AvailableWeekday { get; set; }
        public IEnumerable<WeekdayContactPreference> SelectedWeekday { get; set; }
        public PostedWeekday postedWeekday { get; set; }
        public IEnumerable<TimeContactPreference> AvailableTime { get; set; }
        public IEnumerable<TimeContactPreference> SelectedTime { get; set; }
        public PostedTime postedTime { get; set; }
        public tNSProvider tNSProvider { get; set; }

        public int selectedRaceCode { get; set; }
        public string selectedCaregiverApprovals { get; set; }

        public string NewComments { get; set; }
    }

}


