﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EWC_NSAA.Models;
using System.Web.Mvc;

namespace EWC_NSAA.ViewModels
{
    public class EncounterListViewModel
    {
        public IEnumerable<tEncounter> Encounters { get; set; }
        public IEnumerable<tServiceRecipient> Recipients { get; set; }
        public IEnumerable<SelectListItem> EncounterTypeSelectItems
        {
            get
            {
                foreach (var item in EWC_NSAA.Controllers.EnrollmentController.GetCaregiverContactPrefDropDown())
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = item.Text;
                    selectListItem.Value = item.Value;
                    selectListItem.Selected = item.Selected;
                    yield return selectListItem;
                }
            }
        }
    }
}