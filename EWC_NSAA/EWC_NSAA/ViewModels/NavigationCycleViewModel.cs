﻿using EWC_NSAA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EWC_NSAA.ContactPreferenceCBL.Models;

namespace EWC_NSAA.ViewModels
{
    public class NavigationCycleViewModel
    {
        public tNavigationCycle tNavigationCycle { get; set; }
        public IEnumerable<WeekdayContactPreference> AvailableWeekday { get; set; }
        public IEnumerable<WeekdayContactPreference> SelectedWeekday { get; set; }
        public PostedWeekday postedWeekday { get; set; }
        public IEnumerable<TimeContactPreference> AvailableTime { get; set; }
        public IEnumerable<TimeContactPreference> SelectedTime { get; set; }
        public PostedTime postedTime { get; set; }
        public tNSProvider tNSProvider { get; set; }
    }
}