﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.ViewModels
{
    public class NavigatorTransferInterimViewModel
    {
        public int TransferID { get; set; }
        public int FK_RecipientID { get; set; }
        public int CurrentNavigatorID { get; set; }
        public int NewNavigatorID { get; set; }
        public string NavigatorTransferReason { get; set; }
        public DateTime TransferDt { get; set; }
        public int TransferStatus { get; set; }
        public string TransferRefusedReason { get; set; }
        public DateTime TransferRefusedDt { get; set; }
        public string xtag { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdated { get; set; }
        public string UpdatedBy { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Type { get; set; }
        public int Region { get; set; }
        public string RecipLastName { get; set; }
        public string RecipFirstName { get; set; }
        public string Language { get; set; }
        public string AgreeToSpeakEnglish { get; set; }


    }
}