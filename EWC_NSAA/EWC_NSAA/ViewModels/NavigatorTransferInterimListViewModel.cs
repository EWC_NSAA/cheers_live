﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.ViewModels
{
    public class NavigatorTransferInterimListViewModel
    {
        public IEnumerable<NavigatorTransferInterimViewModel> NavigatorTransfers { get; set; }
    }
}