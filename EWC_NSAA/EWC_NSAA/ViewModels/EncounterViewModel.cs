﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.Models;

namespace EWC_NSAA.ViewModels
{
    public class EncounterViewModel
    {
        public tEncounter tEncounter { get; set; }
        public List<tBarrier> BarrierList { get; set; }
        public tBarrier tBarrier { get; set; }
        //public tSolution tSolution { get; set; }
        public int? SelectedBarrierType { get; set; }
        public int? SelectedBarrier { get; set; }
        public string SelectedSolution { get; set; }
        public string SelectedFollowupDt { get; set; }
        public string SelectedImplementationStatus { get; set; }
        public int ControlPrefix { get; set; }
        public List<tSolution> Solutions { get; set; }
        public IEnumerable<trBarrierType> BarrierTypes { get; set; }
        public IEnumerable<trBarrier> Barriers { get; set; }

        public string NewComments { get; set; }
        
    }
}