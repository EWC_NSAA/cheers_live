﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EWC_NSAA.Models;
using System.Web.Mvc;

namespace EWC_NSAA.ViewModels
{
    public class BarrierViewModel
    {
        public tBarrier tBarrier { get; set; }
        public int? SelectedBarrierType { get; set; }
        public int? SelectedBarrier { get; set; }
        public string SelectedSolution { get; set; }
        public string SelectedFollowupDt { get; set; }
        public string SelectedImplementationStatus { get; set; }
        public List<tSolution> Solutions { get; set; }
        public List<trBarrier> trBarrier { get; set; }
        public int NSRecipientID { get; set; }
        public string RecipLastName { get; set; }
        public string RecipFirstName { get; set; }
        

    }
}