﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EWC_NSAA.Models;

namespace EWC_NSAA.ViewModels
{
    public class NavigatorTransferViewModel
    {
        public int OldNavigatorID { get; set; }
        public int NewNavigatorID { get; set; }
        public int mode { get; set; }
        public DateTime NavigatorTransferDt { get; set; }
        public string NavigatorTransferReason { get; set; }
        public int NSRecipientID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Type { get; set; }
        public int Region { get; set; }
        public string RecipLastName { get; set; }
        public string RecipFirstName { get; set; }
        public string Language { get; set; }
        public string AgreeToSpeakEnglish { get; set; }
        public string Comments { get; set; }


    }
}