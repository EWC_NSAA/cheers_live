﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EWC_NSAA.Models;
using System.Web.Mvc;

namespace EWC_NSAA.ViewModels
{
    public class NavigationCycleListViewModel
    {
        public IEnumerable<trCancerSite> CancerSites { get; set; }
        public IEnumerable<tNavigationCycle> Cycles { get; set; }
        public IEnumerable<trNSProblem> NSProblems { get; set; }
        public IEnumerable<trHealthInsurance> HealthInsurance { get; set; }
        public IEnumerable<trHealthProgram> HealthPrograms { get; set; }
        public IEnumerable<tServiceRecipient> Recipients { get; set; }
        public IEnumerable<SelectListItem> ContactPrefTypeSelectItems
        {
            get
            {
                foreach (var item in EWC_NSAA.Controllers.NavigationCyclesController.GetContactPrefDropDown())
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = item.Text;
                    selectListItem.Value = item.Value;
                    selectListItem.Selected = item.Selected;
                    yield return selectListItem;
                }
            }
        }

        

    }
}