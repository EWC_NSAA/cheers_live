﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.ViewModels
{
    public class AssistanceViewModel
    {
        public int AssistanceID { get; set; }
        public int FK_NavigatorID { get; set; }
        public int IncidentNumber { get; set; }
        public DateTime IncidentDt { get; set; }
        public int? FK_CommunicationCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string RecipID { get; set; }
        public int FK_RequestorCode { get; set; }
        public string NPI { get; set; }
        public string NPIOwner { get; set; }
        public string NPISvcLoc { get; set; }
        public string BusinessName { get; set; }

    }
}