﻿using EWC_NSAA.Models;
using EWC_NSAA.RaceCBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.CaregiverApprovalCBL.Models;
using EWC_NSAA.ContactPreferenceCBL.Models;
using System.ComponentModel.DataAnnotations;
using EWC_NSAA.Metadata;

namespace EWC_NSAA.ViewModels
{
    [MetadataType(typeof(ReportMetaData))]
    public class RecipientReportViewModel
    {
        //tServiceRecipient
        public int NSRecipientID { get; set; }
        public string FK_RecipID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public System.DateTime? DOB { get; set; }
        public Nullable<bool> AddressNotAvailable { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone1 { get; set; }
        public string P1PhoneType { get; set; }
        public string P1PersonalMessage { get; set; }
        public string Phone2 { get; set; }
        public string P2PhoneType { get; set; }
        public string P2PersonalMessage { get; set; }
        public string MotherMaidenName { get; set; }
        public string Email { get; set; }
        public string SSN { get; set; }
        public string ImmigrantStatus { get; set; }
        public string CountryOfOrigin { get; set; }
        public string Gender { get; set; }
        public string Ethnicity { get; set; }
        public string PrimaryLanguage { get; set; }
        public string AgreeToSpeakEnglish { get; set; }
        public string CaregiverName { get; set; }
        public string Relationship { get; set; }
        public string RelationshipOther { get; set; }
        public string CaregiverPhone { get; set; }
        public string CaregiverPhoneType { get; set; }
        public string CaregiverPhonePersonalMessage { get; set; }
        public string CaregiverEmail { get; set; }
        public string CaregiverContactPreference { get; set; }
        public string Comments { get; set; }
        public int FK_NavigatorID { get; set; }
        public Nullable<System.DateTime> NavigatorTransferDt { get; set; }
        public Nullable<int> NavigatorTransferReason { get; set; }
        public Nullable<int> OldNavigatorID { get; set; }

        //NS Provider
        public int NSProviderID { get; set; }
        public string PCPName { get; set; }
        public string PCP_NPI { get; set; }
        public string PCPAddress1 { get; set; }
        public string PCPAddress2 { get; set; }
        public string PCPCity { get; set; }
        public string PCPState { get; set; }
        public string PCPZip { get; set; }
        public string PCPContactFName { get; set; }
        public string PCPContactLName { get; set; }
        public string PCPContactTitle { get; set; }
        public string PCPContactTelephone { get; set; }
        public string PCPContactEmail { get; set; }

        //tNavigationCycle
        public int NavigationCycleID { get; set; }
        public int FK_RecipientID { get; set; }
        public Nullable<int> FK_ReferralID { get; set; }
        public string CancerSite { get; set; }
        public string NSProblem { get; set; }
        public string HealthProgram { get; set; }
        public string HealthInsuranceStatus { get; set; }
        public string ContactPrefDays { get; set; }
        public string ContactPrefHours { get; set; }
        public string ContactPrefHoursOther { get; set; }
        public string ContactPrefType { get; set; }
        public Nullable<bool> SRIndentifierCodeGenerated { get; set; }
        public Nullable<int> FK_ProviderID { get; set; }

        //Race
        public tRace tRace { get; set; }
        public IEnumerable<Race> SelectedRace { get; set; }
        public PostedRace postedRace { get; set; }
        public int RaceAsianCode { get; set; }
        public int RacePacIslanderCode { get; set; }
        public string RaceOther { get; set; }

        //Caregiver Approvals
        public tCaregiverApprovals tCaregiverApprovals { get; set; }
        public IEnumerable<CaregiverApprovals> SelectedApprovals { get; set; }
        public PostedCaregiverApproval postedCaregiverApproval { get; set; }

        //Weekdays
        public IEnumerable<WeekdayContactPreference> AvailableWeekday { get; set; }
        public IEnumerable<WeekdayContactPreference> SelectedWeekday { get; set; }
        public PostedWeekday postedWeekday { get; set; }

        //Times
        public IEnumerable<TimeContactPreference> AvailableTime { get; set; }
        public IEnumerable<TimeContactPreference> SelectedTime { get; set; }
        public PostedTime postedTime { get; set; }

        //Barriers
        public IEnumerable<BarriersReport> BarriersReport { get; set; }

        //Encounters
        public int Encounters { get; set; }
        public int MissedAppointments { get; set; }
        public string LastEncounter { get; set; }
        public string NextEncounter { get; set; }
        public string SvcOutcomeStatus { get; set; }
        public string SvcOutcomeStatusDt { get; set; }
        public string ServicesTerminated { get; set; }
        public string ServicesTerminatedDt { get; set; }

    }
}