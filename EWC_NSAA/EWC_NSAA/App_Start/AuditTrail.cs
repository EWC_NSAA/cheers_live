﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.Models;
using EWC_NSAA.ViewModels;
using System.Security.Principal;
using EWC_NSAA.EWC_NSAA_Service_XML;
using System.Net;

namespace EWC_NSAA.App_Start
{
    
    public class AuditTrail
    {

        /*
       '  Web Services
       ' ----------------------------------------------------
       ' Minutes to wait for service to respond*/
        private const int MINUTES_TO_WAIT = 10;
        // convert each minute to millisecs, use this var to set the proxy timeout value
        private const int SERVICE_WAIT_TIME = 60000 * MINUTES_TO_WAIT;

        [NonAction]
        public void Init()
        {
            //Log User Access

            EWC_NSAA_Service_XML.EWC_NSAA_WebService_XML proxy = new EWC_NSAA_WebService_XML();

            proxy.Credentials = CredentialCache.DefaultCredentials;

            proxy.Timeout = SERVICE_WAIT_TIME;

            proxy.InsertUserAccess(System.Security.Principal.WindowsIdentity.GetCurrent().Name);

        }
            
    

    }
}