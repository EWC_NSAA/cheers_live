﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EWC_NSAA.Metadata
{
    public class BarrierMetaData
    {
        [Required(ErrorMessage = "Followup Date is required.")]
        public System.DateTime FollowUpDt { get; set; }
    }
}