﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EWC_NSAA.Metadata
{
    public class ReportMetaData
    {
       
        [DisplayFormat(DataFormatString = "{0:###-###-####}")]
        public string Phone1 { get; set; }

        [DisplayFormat(DataFormatString = "{0:###-###-####}")]
        public string Phone2 { get; set; }

        [DisplayFormat(DataFormatString = "{0:###-###-####}")]
        public string PCPContactTelephone { get; set; }

        [DisplayFormat(DataFormatString = "{0:###-###-####}")]
        public string CaregiverPhone { get; set; }

    }
}