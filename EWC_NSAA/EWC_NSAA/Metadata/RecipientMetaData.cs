﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EWC_NSAA.Metadata
{
    public class RecipientMetaData
    {
        [Required(ErrorMessage = "Last Name is required.")]
        [StringLength(30, ErrorMessage = "Last Name cannot be more than 30 characters long.")]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "Last Name may contain only letters.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "First Name is required.")]
        [StringLength(20, ErrorMessage = "First Name cannot be more than 30 characters long.")]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "First Name may contain only letters.")]
        public string FirstName { get; set; }

        [StringLength(1, ErrorMessage = "Middle Initial cannot be more than 1 character long.")]
        [RegularExpression("([a-zA-Z]+)", ErrorMessage = "Middle Initial may contain only letters.")]
        public string MiddleInitial { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Required(ErrorMessage = "Date of Birth is required.")]
        public System.DateTime DOB { get; set; }

        [StringLength(30, ErrorMessage = "Address Line 1 cannot be more than 30 characters long.")]
        public string Address1 { get; set; }

        [StringLength(30, ErrorMessage = "Address Line 2 cannot be more than 30 characters long.")]
        public string Address2 { get; set; }

        [StringLength(25, ErrorMessage = "City cannot be more than 25 characters long.")]
        public string City { get; set; }

        [StringLength(2, ErrorMessage = "State cannot be more than 2 characters long.")]
        public string State { get; set; }

        [StringLength(5, ErrorMessage = "Zip Code cannot be more than 5 characters long.")]
        [RegularExpression("^\\d{5}$", ErrorMessage = "Zip Code is invalid.")]
        public string Zip { get; set; }

        [Required(ErrorMessage ="Primary Phone number is required.")]
        [StringLength(15, ErrorMessage = "Primary Phone cannot be more than 15 characters long.")]
        //[RegularExpression("^\\d{10}$", ErrorMessage = "Primary Phone is invalid.")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        [DisplayFormat(DataFormatString = "{0:###-###-####}")]
        public string Phone1 { get; set; }

        [StringLength(15, ErrorMessage = "Secondary Phone cannot be more than 15 characters long.")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string Phone2 { get; set; }

        [StringLength(45, ErrorMessage = "Email cannot be more than 45 characters long.")]
        [RegularExpression("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Email is invalid.")]
        public string Email { get; set; }

        [StringLength(50, ErrorMessage = "Caregiver Name cannot be more than 50 characters long.")]
        public string CaregiverName { get; set; }

        [StringLength(30, ErrorMessage = "Relationship (Other) cannot be more than 30 characters long.")]
        public string RelationshipOther { get; set; }

        [StringLength(15, ErrorMessage = "Caregiver Phone cannot be more than 15 characters long.")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Caregiver Phone is invalid.")]
        public string CaregiverPhone { get; set; }

        [StringLength(45, ErrorMessage = "Caregiver Email cannot be more than 45 characters long.")]
        [RegularExpression("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", ErrorMessage = "Caregiver Email is invalid.")]
        public string CaregiverEmail { get; set; }


        [StringLength(500, ErrorMessage = "Comments cannot be more than 500 characters long.")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }
        
    }
}