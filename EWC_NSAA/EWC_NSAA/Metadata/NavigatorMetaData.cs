﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EWC_NSAA.Metadata
{
    public class NavigatorMetaData
    {
        [Required(ErrorMessage = "Last Name is required.")]
        [StringLength(30, ErrorMessage = "Last Name cannot be more than 30 characters long.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "First Name is required.")]
        [StringLength(20, ErrorMessage = "First Name cannot be more than 30 characters long.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Address is required.")]
        [StringLength(30, ErrorMessage = "Address Line 1 cannot be more than 30 characters long.")]
        public string Address1 { get; set; }

        [StringLength(30, ErrorMessage = "Address Line 2 cannot be more than 30 characters long.")]
        public string Address2 { get; set; }

        [Required(ErrorMessage = "City is required.")]
        [StringLength(25, ErrorMessage = "City cannot be more than 25 characters long.")]
        public string City { get; set; }

        [Required(ErrorMessage = "State is required.")]
        [StringLength(2, ErrorMessage = "State cannot be more than 2 characters long.")]
        public string State { get; set; }

        [Required(ErrorMessage = "Zip Code is required.")]
        [StringLength(5, ErrorMessage = "Zip Code cannot be more than 5 characters long.")]
        [RegularExpression("^\\d{5}$", ErrorMessage = "Zip Code is invalid.")]
        public string Zip { get; set; }
        public string Email { get; set; }

        [StringLength(20, ErrorMessage = "Domain Name cannot be more than 20 characters long.")]
        public string DomainName { get; set; }

        [Required(ErrorMessage = "Business Phone number is required.")]
        [StringLength(10, ErrorMessage = "Business Phone cannot be more than 10 characters long.")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "Business Phone is invalid.")]
        public string BusinessTelephone { get; set; }

        [StringLength(10, ErrorMessage = "Mobile Phone cannot be more than 10 characters long.")]
        [RegularExpression("^\\d{10}$", ErrorMessage = "Mobile Phone is invalid.")]
        public string MobileTelephone { get; set; }

    }
}