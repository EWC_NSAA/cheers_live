﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EWC_NSAA.Metadata
{
    public class NavigationCycleMetaData
    {
       
        [StringLength(15, ErrorMessage = "Health Insurance Plan Number cannot be more than 15 character long.")]
        public string HealthInsurancePlanNumber { get; set; }

        [StringLength(15, ErrorMessage = "Invalid selection.")]
        public string ContactPrefDays { get; set; }

        [StringLength(15, ErrorMessage = "Invalid Selection")]
        public string ContactPrefHours { get; set; }

        [StringLength(20, ErrorMessage = "Contact Preference Hours (Other) cannot be more than 20 character long.")]
        public string ContactPrefHoursOther { get; set; }
        
    }
}