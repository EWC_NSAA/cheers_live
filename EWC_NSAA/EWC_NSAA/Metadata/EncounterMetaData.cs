﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EWC_NSAA.Metadata
{
    public class EncounterMetaData
    {
        [Required(ErrorMessage = "Encounter Number is required.")]
        [RegularExpression("[0-9]", ErrorMessage = "Encounter Number may contain only numbers.")]
        public int EncounterNumber { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Required(ErrorMessage = "Encounter Date is required.")]
        public Nullable<System.DateTime> EncounterDt { get; set; }

        [StringLength(20, ErrorMessage = "Encounter Start Time cannot be more than 20 characters long.")]
        public string EncounterStartTime { get; set; }

        [StringLength(20, ErrorMessage = "Encounter End Time cannot be more than 20 characters long.")]
        public string EncounterEndTime { get; set; }

        [StringLength(20, ErrorMessage = "Next Appointment Time cannot be more than 20 characters long.")]
        public string NextAppointmentTime { get; set; }

        [StringLength(2000, ErrorMessage = "Notes cannot be more than 2000 character long.")]
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public Nullable<System.DateTime> NextAppointmentDt { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public Nullable<System.DateTime> ServicesTerminatedDt { get; set; }


    }
}