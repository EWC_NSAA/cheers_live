﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.Models;
using EWC_NSAA.RaceCBL.Models;
using EWC_NSAA.ViewModels;
using System.Collections;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using EWC_NSAA.Common;
using EWC_NSAA.CaregiverApprovalCBL.Models;
using EWC_NSAA.ContactPreferenceCBL.Models;


namespace EWC_NSAA.Controllers
{
    //[Authorize(Roles = @"EWC_NSAA_Contributors, EWC_NSAA_A")]
    public class ReportController : BaseController
    {

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Initialize
            RecipientReportViewModel recipreportVM = new RecipientReportViewModel();
            tRace tRace = new tRace();
            tCaregiverApprovals tCaregiverApprovals = new tCaregiverApprovals();
            tNavigationCycle tNavigationCycle = new tNavigationCycle();
            tNSProvider tNSProvider = new tNSProvider();
            List<Race> ls = new List<Race>();
            List<CaregiverApprovals> ls1 = new List<CaregiverApprovals>();
            List<WeekdayContactPreference> lsW = new List<WeekdayContactPreference>();

            //Get data
            DataSet dsServiceRecipient = getProxy().GetRecipientReport(1, (int)id, 0);
            recipreportVM = BuildRecipientData(recipreportVM, dsServiceRecipient);

            DataSet dsNavigationCycle = getProxy().GetRecipientReport(3, (int)id, 0);
            recipreportVM = BuildNavigationCycleData(recipreportVM, dsNavigationCycle);

            //int ProviderID = Convert.IsDBNull(recipreportVM.FK_ProviderID) ? 0 : (int)recipreportVM.FK_ProviderID;
            if (recipreportVM.FK_ProviderID != null)
            {
                DataSet dsNSProvider = getProxy().GetRecipientReport(2, 0, (int)recipreportVM.FK_ProviderID);
                recipreportVM = BuildNSProviderData(recipreportVM, dsNSProvider);
            }

            DataSet dsBarriers = getProxy().GetRecipientReport(4, (int)id, 0);
            recipreportVM = BuildBarriersData(recipreportVM, dsBarriers);

            DataSet dsEncounters = getProxy().GetRecipientReport(5, (int)id, 0);
            recipreportVM.Encounters = (int)dsEncounters.Tables[0].Rows[0]["encounters"];

            DataSet dsMissedAppts = getProxy().GetRecipientReport(6, (int)id, 0);
            recipreportVM.MissedAppointments = (int)dsMissedAppts.Tables[0].Rows[0]["missedappts"];

            DataSet dsLastEncounter = getProxy().GetRecipientReport(7, (int)id, 0);
            recipreportVM.LastEncounter = dsLastEncounter.Tables[0].Rows[0]["LastEncounter"].ToString();

            DataSet dsNextEncounter = getProxy().GetRecipientReport(8, (int)id, 0);
            recipreportVM.NextEncounter = dsNextEncounter.Tables[0].Rows[0]["NextEncounter"].ToString();

            DataSet dsSvcOutcomeStatus = getProxy().GetRecipientReport(9, (int)id, 0);
            if (dsSvcOutcomeStatus.Tables[0].Rows.Count > 0)
            {
                int SvcOutcomeStatus = Convert.IsDBNull(dsSvcOutcomeStatus.Tables[0].Rows[0]["SvcOutcomeStatus"]) ? 0 : (int)dsSvcOutcomeStatus.Tables[0].Rows[0]["SvcOutcomeStatus"];
                switch (SvcOutcomeStatus)
                {
                    case 0:
                        recipreportVM.SvcOutcomeStatus = "Not met";
                        break;
                    case 1:
                        recipreportVM.SvcOutcomeStatus = "Not met";
                        break;
                    case 2:
                        recipreportVM.SvcOutcomeStatus = "Met";
                        break;
                }
                recipreportVM.SvcOutcomeStatusDt = dsSvcOutcomeStatus.Tables[0].Rows[0]["LastUpdated"].ToString();

                bool ServicesTerminated = Convert.IsDBNull(dsSvcOutcomeStatus.Tables[0].Rows[0]["ServicesTerminated"]) ? false : (bool)dsSvcOutcomeStatus.Tables[0].Rows[0]["ServicesTerminated"];
                switch (ServicesTerminated)
                {
                    case false:
                        recipreportVM.ServicesTerminated = "No";
                        break;
                    case true:
                        recipreportVM.ServicesTerminated = "Yes";
                        break;
                }
                recipreportVM.ServicesTerminatedDt = dsSvcOutcomeStatus.Tables[0].Rows[0]["ServicesTerminatedDt"].ToString();
            }
            if (dsServiceRecipient == null)
            {
                return HttpNotFound();
            }


            ls = GetRaceFromDataSet((int)id);

            recipreportVM.RaceAsianCode = GetAsianRaceFromDataSet((int)id);

            recipreportVM.RacePacIslanderCode = GetPacIslanderRaceFromDataSet((int)id);

            tRace.RaceOther = GetRaceOtherFromDataSet((int)id);

            recipreportVM.SelectedRace = ls;

            ls1 = GetApprovalFromDataSet((int)id);

            tCaregiverApprovals.CaregiverApprovalOther = GetApprovalOtherFromDataSet((int)id);

            recipreportVM.SelectedApprovals = ls1;

            //recipVM.tServiceRecipient = tServiceRecipient;

            if (recipreportVM.ContactPrefDays != null)
            { 
                var postedWeekdayIds = new string[0];
                postedWeekdayIds = recipreportVM.ContactPrefDays.Split(',');

                // if there are any selected ids saved, create a list of weekday
                if (postedWeekdayIds.Any())
                {
                    recipreportVM.SelectedWeekday = GetAllWeekdays()
                     .Where(x => postedWeekdayIds.Any(s => x.WeekdayCode.ToString().Equals(s)))
                     .ToList();
                }
            }

            if (recipreportVM.ContactPrefHours != null)
            {
                var postedTimeIds = new string[0];
                postedTimeIds = recipreportVM.ContactPrefHours.Split(',');

                // if there are any selected ids saved, create a list of weekday
                if (postedTimeIds.Any())
                {
                    recipreportVM.SelectedTime = GetAllTimes()
                     .Where(x => postedTimeIds.Any(s => x.TimeCode.ToString().Equals(s)))
                     .ToList();
                }
            }

            recipreportVM.tRace = tRace;
            recipreportVM.tCaregiverApprovals = tCaregiverApprovals;

                       
            return View(recipreportVM);
        }

        #region Build Recipient Data

        private RecipientReportViewModel BuildRecipientData(RecipientReportViewModel recipVM, DataSet dsRecipientData)
        {
            RecipientReportViewModel recipreportVM = recipVM;

            recipreportVM.NSRecipientID = (int)dsRecipientData.Tables[0].Rows[0]["NSRecipientID"];
            recipreportVM.FK_NavigatorID = (int)dsRecipientData.Tables[0].Rows[0]["FK_NavigatorID"];
		    recipreportVM.FK_RecipID = dsRecipientData.Tables[0].Rows[0]["FK_RecipID"].ToString();
            recipreportVM.FirstName = dsRecipientData.Tables[0].Rows[0]["FirstName"].ToString();
            recipreportVM.LastName = dsRecipientData.Tables[0].Rows[0]["LastName"].ToString();
		    recipreportVM.Address1 = dsRecipientData.Tables[0].Rows[0]["Address1"].ToString();
            recipreportVM.Address2 = dsRecipientData.Tables[0].Rows[0]["Address2"].ToString();
		    recipreportVM.City = dsRecipientData.Tables[0].Rows[0]["City"].ToString();
		    recipreportVM.State = dsRecipientData.Tables[0].Rows[0]["State"].ToString();
		    recipreportVM.Zip = dsRecipientData.Tables[0].Rows[0]["Zip"].ToString();
            recipreportVM.Phone1 = dsRecipientData.Tables[0].Rows[0]["Phone1"].ToString();
            recipreportVM.P1PhoneType = dsRecipientData.Tables[0].Rows[0]["P1PhoneType"].ToString();
            recipreportVM.P1PersonalMessage = dsRecipientData.Tables[0].Rows[0]["P1PersonalMessage"].ToString();
            recipreportVM.Phone2 = dsRecipientData.Tables[0].Rows[0]["Phone2"].ToString();
            recipreportVM.P2PhoneType = dsRecipientData.Tables[0].Rows[0]["P2PhoneType"].ToString();
            recipreportVM.P2PersonalMessage = dsRecipientData.Tables[0].Rows[0]["P2PersonalMessage"].ToString();
            recipreportVM.Email = dsRecipientData.Tables[0].Rows[0]["Email"].ToString();
            recipreportVM.ImmigrantStatus = dsRecipientData.Tables[0].Rows[0]["ImmigrantStatus"].ToString();
            recipreportVM.CountryOfOrigin = dsRecipientData.Tables[0].Rows[0]["CountryOfOrigin"].ToString();
            recipreportVM.Ethnicity = dsRecipientData.Tables[0].Rows[0]["Ethnicity"].ToString();
            recipreportVM.PrimaryLanguage = dsRecipientData.Tables[0].Rows[0]["PrimaryLanguage"].ToString();
            recipreportVM.AgreeToSpeakEnglish = dsRecipientData.Tables[0].Rows[0]["AgreeToSpeakEnglish"].ToString();
            recipreportVM.CaregiverName = dsRecipientData.Tables[0].Rows[0]["CaregiverName"].ToString();
            recipreportVM.Relationship = dsRecipientData.Tables[0].Rows[0]["Relationship"].ToString();
            recipreportVM.RelationshipOther = dsRecipientData.Tables[0].Rows[0]["RelationshipOther"].ToString();
            recipreportVM.CaregiverPhone = dsRecipientData.Tables[0].Rows[0]["CaregiverPhone"].ToString();
            recipreportVM.CaregiverPhoneType = dsRecipientData.Tables[0].Rows[0]["CaregiverPhoneType"].ToString();
            recipreportVM.CaregiverPhonePersonalMessage = dsRecipientData.Tables[0].Rows[0]["CaregiverPhonePersonalMessage"].ToString();
            recipreportVM.CaregiverEmail = dsRecipientData.Tables[0].Rows[0]["CaregiverEmail"].ToString();
            recipreportVM.CaregiverContactPreference = dsRecipientData.Tables[0].Rows[0]["CaregiverContactPreference"].ToString();

            return recipreportVM;
        }

        #endregion

        #region Build NS Provider Data

        private RecipientReportViewModel BuildNSProviderData(RecipientReportViewModel recipVM, DataSet dsNSProvider)
        {
            RecipientReportViewModel recipreportVM = recipVM;

            recipreportVM.NSProviderID = (int)dsNSProvider.Tables[0].Rows[0]["NSProviderID"];
            recipreportVM.PCPName = dsNSProvider.Tables[0].Rows[0]["PCPName"].ToString();
            recipreportVM.PCP_NPI = dsNSProvider.Tables[0].Rows[0]["PCP_NPI"].ToString();
            recipreportVM.PCPAddress1 = dsNSProvider.Tables[0].Rows[0]["PCPAddress1"].ToString();
            recipreportVM.PCPAddress2 = dsNSProvider.Tables[0].Rows[0]["PCPAddress2"].ToString();
            recipreportVM.PCPCity = dsNSProvider.Tables[0].Rows[0]["PCPCity"].ToString();
            recipreportVM.PCPState = dsNSProvider.Tables[0].Rows[0]["PCPState"].ToString();
            recipreportVM.PCPZip = dsNSProvider.Tables[0].Rows[0]["PCPZip"].ToString();
            recipreportVM.PCPContactFName = dsNSProvider.Tables[0].Rows[0]["PCPContactFName"].ToString();
            recipreportVM.PCPContactLName = dsNSProvider.Tables[0].Rows[0]["PCPContactLName"].ToString();
            recipreportVM.PCPContactTitle = dsNSProvider.Tables[0].Rows[0]["PCPContactTitle"].ToString();
            recipreportVM.PCPContactTelephone = dsNSProvider.Tables[0].Rows[0]["PCPContactTelephone"].ToString();
            recipreportVM.PCPContactEmail = dsNSProvider.Tables[0].Rows[0]["PCPContactEmail"].ToString();
            
            return recipreportVM;
        }

        #endregion

        #region Build Navigation Cycle Data
        private RecipientReportViewModel BuildNavigationCycleData(RecipientReportViewModel recipVM, DataSet dsNavigationCycle)
        {
            RecipientReportViewModel recipreportVM = recipVM;

            if(dsNavigationCycle.Tables[0].Rows.Count > 0)
            {
                recipreportVM.NavigationCycleID = (int)dsNavigationCycle.Tables[0].Rows[0]["NavigationCycleID"];
                recipreportVM.FK_ProviderID = (int)dsNavigationCycle.Tables[0].Rows[0]["FK_ProviderID"];
                recipreportVM.CancerSite = dsNavigationCycle.Tables[0].Rows[0]["CancerSite"].ToString();
                recipreportVM.NSProblem = dsNavigationCycle.Tables[0].Rows[0]["NSProblem"].ToString();
                recipreportVM.HealthProgram = dsNavigationCycle.Tables[0].Rows[0]["HealthProgram"].ToString();
                recipreportVM.HealthInsuranceStatus = dsNavigationCycle.Tables[0].Rows[0]["HealthInsuranceStatus"].ToString();
                recipreportVM.ContactPrefDays = dsNavigationCycle.Tables[0].Rows[0]["ContactPrefDays"].ToString();
                recipreportVM.ContactPrefHours = dsNavigationCycle.Tables[0].Rows[0]["ContactPrefHours"].ToString();
                recipreportVM.ContactPrefHoursOther = dsNavigationCycle.Tables[0].Rows[0]["ContactPrefHoursOther"].ToString();
                recipreportVM.ContactPrefType = dsNavigationCycle.Tables[0].Rows[0]["ContactPrefType"].ToString();
            }

            return recipreportVM;

        }

        #endregion

        #region Build Barriers Data
        private RecipientReportViewModel BuildBarriersData(RecipientReportViewModel recipVM, DataSet dsBarriers)
        {
            RecipientReportViewModel recipreportVM = recipVM;

            List<BarriersReport> lsBarriers = new List<BarriersReport>();

            for (int i = 0; i < dsBarriers.Tables[0].Rows.Count; i++)
            {
                lsBarriers.Add(new BarriersReport
                {
                    BarrierID = (int)dsBarriers.Tables[0].Rows[i]["BarrierID"],
                    FK_EncounterID = (int)dsBarriers.Tables[0].Rows[i]["FK_EncounterID"],
                    EncounterDt = (DateTime)dsBarriers.Tables[0].Rows[i]["EncounterDt"],
                    BarrierType = dsBarriers.Tables[0].Rows[i]["BarrierType"].ToString(),
                    BarrierAssessed = dsBarriers.Tables[0].Rows[i]["BarrierAssessed"].ToString(),
                    SolutionID = (int)dsBarriers.Tables[0].Rows[i]["SolutionID"],
                    FK_BarrierID = (int)dsBarriers.Tables[0].Rows[i]["FK_BarrierID"],
                    SolutionOffered = dsBarriers.Tables[0].Rows[i]["SolutionOffered"].ToString(),
                    SolutionImplementationStatus = dsBarriers.Tables[0].Rows[i]["SolutionImplementationStatus"].ToString()
                });
            }

             recipreportVM.BarriersReport = lsBarriers;           
             return recipreportVM;

        }

        #endregion

        #region Get Race from DataSet
        private List<Race> GetRaceFromDataSet(int NSID)
        {
            List<Race> lsRace = new List<Race>();

            try
            {
                DataSet RaceDS = getProxy().GetRace(NSID);
                for (var index = 0; index < RaceDS.Tables[0].Rows.Count; index++)
                {
                    if ((int)RaceDS.Tables[0].Rows[index]["RaceCode"] < 10)
                    {
                        lsRace.Add(new Race() { RaceCode = (int)RaceDS.Tables[0].Rows[index]["RaceCode"], RaceName = RaceDS.Tables[0].Rows[index]["RaceName"].ToString(), IsSelected = true });
                    }
                }

            }
            catch
            {
                throw;
            }

            return lsRace;
        }

        #endregion

        #region Get Asian Race from DataSet
        private int GetAsianRaceFromDataSet(int NSID)
        {
            int raceAsian = 0;

            try
            {
                DataSet RaceAsianDS = getProxy().GetRace(NSID);
                for (var index = 0; index < RaceAsianDS.Tables[0].Rows.Count; index++)
                {
                    if ((int)RaceAsianDS.Tables[0].Rows[index]["RaceCode"] > 10 && (int)RaceAsianDS.Tables[0].Rows[index]["RaceCode"] < 21)
                    {
                        raceAsian = (int)RaceAsianDS.Tables[0].Rows[index]["RaceCode"];
                    }
                }

            }
            catch
            {
                throw;
            }

            return raceAsian;
        }

        #endregion

        #region Get Pac Islander Race from DataSet
        private int GetPacIslanderRaceFromDataSet(int NSID)
        {
            int racePacIslander = 0;

            try
            {
                DataSet RacePacIslanderDS = getProxy().GetRace(NSID);
                for (var index = 0; index < RacePacIslanderDS.Tables[0].Rows.Count; index++)
                {
                    if ((int)RacePacIslanderDS.Tables[0].Rows[index]["RaceCode"] > 10 && (int)RacePacIslanderDS.Tables[0].Rows[index]["RaceCode"] < 21)
                    {
                        racePacIslander = (int)RacePacIslanderDS.Tables[0].Rows[index]["RaceCode"];
                    }
                }

            }
            catch
            {
                throw;
            }

            return racePacIslander;
        }

        #endregion

        #region Get Race Other from DataSet
        private string GetRaceOtherFromDataSet(int NSID)
        {
            string raceOther = "";

            try
            {
                DataSet RaceAsianDS = getProxy().GetRace(NSID);

                if (RaceAsianDS.Tables[0].Rows.Count > 0)
                {
                    raceOther = RaceAsianDS.Tables[0].Rows[0]["RaceOther"].ToString();
                }



            }
            catch
            {
                throw;
            }

            return raceOther;
        }

        #endregion

        #region Get Approval from DataSet
        private List<CaregiverApprovals> GetApprovalFromDataSet(int NSID)
        {
            List<CaregiverApprovals> lsApprovals = new List<CaregiverApprovals>();

            try
            {
                DataSet ApprovalDS = getProxy().GetCaregiverApproval(NSID);
                for (var index = 0; index < ApprovalDS.Tables[0].Rows.Count; index++)
                {
                    if ((int)ApprovalDS.Tables[0].Rows[index]["CaregiverApproval"] < 100)
                    {
                        lsApprovals.Add(new CaregiverApprovals() { ApprovalCode = (int)ApprovalDS.Tables[0].Rows[index]["CaregiverApproval"], ApprovalDescription = ApprovalDS.Tables[0].Rows[index]["CaregiverApprovalDescription"].ToString(), IsSelected = true });
                    }
                }

            }
            catch
            {
                throw;
            }

            return lsApprovals;
        }

        #endregion

        #region Get Approval Other from DataSet
        private string GetApprovalOtherFromDataSet(int NSID)
        {
            string approvalOther = "";

            try
            {
                DataSet ApprovalOtherDS = getProxy().GetCaregiverApproval(NSID);

                if (ApprovalOtherDS.Tables[0].Rows.Count > 0)
                { approvalOther = ApprovalOtherDS.Tables[0].Rows[0]["CaregiverApprovalOther"].ToString(); }



            }
            catch
            {
                throw;
            }

            return approvalOther;
        }

        #endregion

        #region Get All Weekdays
        /// <summary>
        /// for get all Weekday
        /// </summary>
        public static IEnumerable<WeekdayContactPreference> GetAllWeekdays()
        {
            List<WeekdayContactPreference> ls = new List<WeekdayContactPreference>();

            ls.Add(new WeekdayContactPreference() { WeekdayName = "Monday", WeekdayCode = 1 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Tuesday", WeekdayCode = 2 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Wednesday", WeekdayCode = 3 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Thursday", WeekdayCode = 4 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Friday", WeekdayCode = 5 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Saturday", WeekdayCode = 6 });


            return ls;
        }
        #endregion

        #region Get All Times
        /// <summary>
        /// for get all Times
        /// </summary>
        public static IEnumerable<TimeContactPreference> GetAllTimes()
        {
            List<TimeContactPreference> ls = new List<TimeContactPreference>();

            ls.Add(new TimeContactPreference() { TimeName = "7am-9am", TimeCode = 1 });
            ls.Add(new TimeContactPreference() { TimeName = "10am-12pm", TimeCode = 2 });
            ls.Add(new TimeContactPreference() { TimeName = "1pm-3pm", TimeCode = 3 });
            ls.Add(new TimeContactPreference() { TimeName = "4pm-5pm", TimeCode = 4 });
            ls.Add(new TimeContactPreference() { TimeName = "6pm-9pm", TimeCode = 5 });
            ls.Add(new TimeContactPreference() { TimeName = "Other", TimeCode = 6 });


            return ls;
        }

        #endregion

    }
}