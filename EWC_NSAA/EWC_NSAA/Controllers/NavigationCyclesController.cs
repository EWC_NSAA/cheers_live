﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.Models;
using EWC_NSAA.ViewModels;
using EWC_NSAA.ContactPreferenceCBL.Models;
using EWC_NSAA.Common;

namespace EWC_NSAA.Controllers
{
    //[Authorize(Roles = @"EWC_NSAA_Contributors, EWC_NSAA_A")]
    public class NavigationCyclesController : BaseController
    {
       
        // GET: NavigationCycles
        public ActionResult Index(int NSID, int NavigatorID)
        {
            NavigationCycleListViewModel NavigationCyclesListVM = new NavigationCycleListViewModel();
            List<tNavigationCycle> navigationCyclesList = new List<tNavigationCycle>();
            List<trCancerSite> cancerSitesList = new List<trCancerSite>();
            List<trNSProblem> nsProblemsList = new List<trNSProblem>();
            List<trHealthProgram> healthProgramsList = new List<trHealthProgram>();
            List<trHealthInsurance> healthInsuranceList = new List<trHealthInsurance>();
            List<tServiceRecipient> lsRecipients = new List<tServiceRecipient>();

            try
            {
                navigationCyclesList = GetNavigationCyclesFromDataSet(NSID);
                NavigationCyclesListVM.Cycles = navigationCyclesList;

                cancerSitesList = GetCancerSitesFromDataSet();
                NavigationCyclesListVM.CancerSites = cancerSitesList;

                healthProgramsList = GetHealthProgramsFromDataSet();
                NavigationCyclesListVM.HealthPrograms = healthProgramsList;

                healthInsuranceList = GetHealthInsuranceFromDataSet();
                NavigationCyclesListVM.HealthInsurance = healthInsuranceList;

                nsProblemsList = GetNSProblemsFromDataSet();
                NavigationCyclesListVM.NSProblems = nsProblemsList;

                lsRecipients = GetRecipientsFromDataSet();
                NavigationCyclesListVM.Recipients = lsRecipients;

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }
            
            return View(NavigationCyclesListVM);
        }

        // GET: NavigationCycles/Create
        public ActionResult Create()
        {
            //Declare Navigation Cycle View Model
            var tnavigationcycleVM = new NavigationCycleViewModel();
            var tNSProvider = new tNSProvider();
            var selectedWeekday = new List<WeekdayContactPreference>();
            var selectedTime = new List<TimeContactPreference>();

            try
            {
                //Weekday setup
                tnavigationcycleVM.AvailableWeekday = GetAllWeekdays().ToList();
                tnavigationcycleVM.SelectedWeekday = selectedWeekday;

                //Time setup
                tnavigationcycleVM.AvailableTime = GetAllTimes().ToList();
                tnavigationcycleVM.SelectedTime = selectedTime;

                tNSProvider.NSProviderID = 0;
                tnavigationcycleVM.tNSProvider = tNSProvider;

            }
            catch
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return View(tnavigationcycleVM);
        }

        // POST: NavigationCycles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Exclude = "NSProviderID")] NavigationCycleViewModel CycleVM, int NSID, int NavigatorID)//[Bind(Include = "NavigationCycleID,FK_RecipientID,FK_ReferralID,CancerSite,NSProblem,HealthProgram,HealthInsuranceStatus,HealthInsurancePlan,HealthInsurancePlanNumber,ContactPrefDays,ContactPrefHours,ContactPrefHoursOther,ContactPrefType,SRIndentifierCodeGenerated,FK_NavigatorID,NavigatorTransfer,NavigatorTransferDt,NavigatorTransferReason,NewNavigatorID,xtag,DateCreated,CreatedBy,LastUpdated,UpdatedBy")] tNavigationCycle tNavigationCycle)
        {
            int NavigationCycleID = 0;
            int NSProviderID = 0;

            if (ModelState.IsValid)
            {
                //Weekdays 
                var WeekdaysList = GetWeekdayList(CycleVM);
                CycleVM.tNavigationCycle.ContactPrefDays = WeekdaysList;

                //Times 
                var TimesList = GetTimeList(CycleVM);
                CycleVM.tNavigationCycle.ContactPrefHours = TimesList;

                //CreatedBy
                CycleVM.tNavigationCycle.CreatedBy = "";

                //Get Navigator ID
                
                CycleVM.tNavigationCycle.FK_NavigatorID = NavigatorID;

                //Get Recipient ID
                CycleVM.tNavigationCycle.FK_RecipientID = NSID;
                
                //Provider CreatedBy
                CycleVM.tNSProvider.CreatedBy = "";


                try
                {
                    if (CycleVM.tNSProvider.PCPName != "" && CycleVM.tNSProvider.PCPName != null && CycleVM.tNSProvider.PCP_NPI != "" && CycleVM.tNSProvider.PCP_NPI != null)
                    {
                        //Update Provider Record
                        NSProviderID = getProxy().InsertNSProvider(CycleVM.tNSProvider.PCPName, 
                            CycleVM.tNSProvider.EWCPCP,
                            CycleVM.tNSProvider.PCP_NPI, CycleVM.tNSProvider.PCP_Owner, 
                            CycleVM.tNSProvider.PCP_Location, CycleVM.tNSProvider.PCPAddress1, 
                            CycleVM.tNSProvider.PCPAddress2, CycleVM.tNSProvider.PCPCity, 
                            CycleVM.tNSProvider.PCPState, CycleVM.tNSProvider.PCPZip, 
                            CycleVM.tNSProvider.PCPContactFName, CycleVM.tNSProvider.PCPContactLName, 
                            CycleVM.tNSProvider.PCPContactTitle, CycleVM.tNSProvider.PCPContactTelephone, 
                            CycleVM.tNSProvider.PCPContactEmail, CycleVM.tNSProvider.Medical, 
                            CycleVM.tNSProvider.MedicalSpecialty, CycleVM.tNSProvider.MedicalSpecialtyOther,
                            CycleVM.tNSProvider.ManagedCarePlan, CycleVM.tNSProvider.ManagedCarePlanOther);

                        //Get inserted Provider ID
                        CycleVM.tNavigationCycle.FK_ProviderID = NSProviderID;
                    }
                    //Insert Navigation Cycle

                    NavigationCycleID = getProxy().InsertNavigationCycle(CycleVM.tNavigationCycle.FK_RecipientID, CycleVM.tNavigationCycle.FK_ProviderID, CycleVM.tNavigationCycle.FK_ReferralID, CycleVM.tNavigationCycle.CancerSite,
                        CycleVM.tNavigationCycle.NSProblem, CycleVM.tNavigationCycle.HealthProgram, CycleVM.tNavigationCycle.HealthInsuranceStatus, CycleVM.tNavigationCycle.HealthInsurancePlan, CycleVM.tNavigationCycle.HealthInsurancePlanNumber,
                        CycleVM.tNavigationCycle.ContactPrefDays, CycleVM.tNavigationCycle.ContactPrefHours, CycleVM.tNavigationCycle.ContactPrefHoursOther, CycleVM.tNavigationCycle.ContactPrefType, CycleVM.tNavigationCycle.SRIndentifierCodeGenerated,
                        CycleVM.tNavigationCycle.FK_NavigatorID);

                        
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                TempData["FeedbackMessage"] = "Your data was saved successfully.";
                //return View(CycleVM);
                return RedirectToAction("Create", "Encounters", new { NSID = NSID, NavigatorID = NavigatorID, NavigationCycleID = NavigationCycleID });
            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                //Declare Navigation Cycle View Model
                var tnavigationcycleVM = new NavigationCycleViewModel();
                var selectedWeekday = new List<WeekdayContactPreference>();
                var selectedTime = new List<TimeContactPreference>();

                try
                {
                    //Weekday setup
                    tnavigationcycleVM.AvailableWeekday = GetAllWeekdays().ToList();
                    tnavigationcycleVM.SelectedWeekday = selectedWeekday;

                    //Time setup
                    tnavigationcycleVM.AvailableTime = GetAllTimes().ToList();
                    tnavigationcycleVM.SelectedTime = selectedTime;

                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                return View(tnavigationcycleVM);
            }

            
        }

        // GET: NavigationCycles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tNavigationCycle tNavigationCycle = GetNavigationCycle((int)id);
            if (tNavigationCycle == null)
            {
                return HttpNotFound();
            }

            NavigationCycleViewModel CycleVM = new NavigationCycleViewModel();
            CycleVM.tNavigationCycle = tNavigationCycle;

            //Weekdays
            PostedWeekday postedWeekday = new PostedWeekday();

            string weekdayString = CycleVM.tNavigationCycle.ContactPrefDays;
            string[] weekdays = weekdayString.Split(',');
            postedWeekday.WeekdayIds = weekdays;
            //Set up the weekdays in View Model
            CycleVM.SelectedWeekday = GetWeekdaySelected(postedWeekday);
            CycleVM.postedWeekday = postedWeekday;
            CycleVM.AvailableWeekday = GetAllWeekdays().ToList();

            //Times
            PostedTime postedTime = new PostedTime();

            string timeString = CycleVM.tNavigationCycle.ContactPrefHours;
            string[] times = timeString.Split(',');
            postedTime.TimeIds = times;
            //Set up the weekdays in View Model
            CycleVM.SelectedTime = GetTimeSelected(postedTime);
            CycleVM.postedTime = postedTime;
            CycleVM.AvailableTime = GetAllTimes().ToList();

            //Provider
            try
            {
                if (tNavigationCycle.FK_ProviderID != null && tNavigationCycle.FK_ProviderID != 0)
                {
                    CycleVM.tNSProvider = GetNSProvider((int)tNavigationCycle.FK_ProviderID);
                }
                else
                {
                    //Save 0 in NSPRoviderID
                    tNSProvider tnsProvider = new tNSProvider();
                    tnsProvider.NSProviderID = 0;
                    CycleVM.tNSProvider = tnsProvider;
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

           return View(CycleVM);
        }

        // POST: NavigationCycles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NavigationCycleViewModel CycleVM)//[Bind(Include = "NavigationCycleID,FK_RecipientID,FK_ReferralID,CancerSite,NSProblem,HealthProgram,HealthInsuranceStatus,HealthInsurancePlan,HealthInsurancePlanNumber,ContactPrefDays,ContactPrefHours,ContactPrefHoursOther,ContactPrefType,SRIndentifierCodeGenerated,FK_NavigatorID,NavigatorTransfer,NavigatorTransferDt,NavigatorTransferReason,NewNavigatorID,xtag,DateCreated,CreatedBy,LastUpdated,UpdatedBy")] tNavigationCycle tNavigationCycle)
        {
            if (ModelState.IsValid)
            {
                //Weekdays 
                var WeekdaysList = GetWeekdayList(CycleVM);
                CycleVM.tNavigationCycle.ContactPrefDays = WeekdaysList;

                //Times 
                var TimesList = GetTimeList(CycleVM);
                CycleVM.tNavigationCycle.ContactPrefHours = TimesList;

                //CreatedBy
                CycleVM.tNavigationCycle.CreatedBy = "";
                //
                CycleVM.tNavigationCycle.FK_ProviderID = CycleVM.tNSProvider.NSProviderID;

                try
                {
                    //Get Navigator ID
                    CycleVM.tNavigationCycle.FK_NavigatorID = GetNavigatorID();

                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }
                
                //Provider CreatedBy
                CycleVM.tNSProvider.CreatedBy = "";

                try
                {
                    //Update Provider Record
                    if (CycleVM.tNavigationCycle.FK_ProviderID != null && CycleVM.tNavigationCycle.FK_ProviderID != 0)
                    {
                        getProxy().UpdateNSProvider(CycleVM.tNSProvider.NSProviderID, CycleVM.tNSProvider.PCPName, CycleVM.tNSProvider.EWCPCP, 
                            CycleVM.tNSProvider.PCP_NPI, CycleVM.tNSProvider.PCP_Owner, CycleVM.tNSProvider.PCP_Location, CycleVM.tNSProvider.PCPAddress1, CycleVM.tNSProvider.PCPAddress2,
                        CycleVM.tNSProvider.PCPCity, CycleVM.tNSProvider.PCPState, CycleVM.tNSProvider.PCPZip, CycleVM.tNSProvider.PCPContactFName, CycleVM.tNSProvider.PCPContactLName, CycleVM.tNSProvider.PCPContactTitle,
                        CycleVM.tNSProvider.PCPContactTelephone, CycleVM.tNSProvider.PCPContactEmail, CycleVM.tNSProvider.Medical, CycleVM.tNSProvider.MedicalSpecialty, CycleVM.tNSProvider.MedicalSpecialtyOther,
                        CycleVM.tNSProvider.ManagedCarePlan, CycleVM.tNSProvider.ManagedCarePlanOther);


                    }
                    else if (CycleVM.tNSProvider.PCPName != "" && CycleVM.tNSProvider.PCPName != null && CycleVM.tNSProvider.PCP_NPI != "" && CycleVM.tNSProvider.PCP_NPI != null)
                    {
                        //Insert Provider Record
                        CycleVM.tNavigationCycle.FK_ProviderID = getProxy().InsertNSProvider(CycleVM.tNSProvider.PCPName, CycleVM.tNSProvider.EWCPCP,
                            CycleVM.tNSProvider.PCP_NPI, CycleVM.tNSProvider.PCP_Owner, 
                            CycleVM.tNSProvider.PCP_Location, CycleVM.tNSProvider.PCPAddress1, CycleVM.tNSProvider.PCPAddress2,
                            CycleVM.tNSProvider.PCPCity, CycleVM.tNSProvider.PCPState, CycleVM.tNSProvider.PCPZip, CycleVM.tNSProvider.PCPContactFName, CycleVM.tNSProvider.PCPContactLName, CycleVM.tNSProvider.PCPContactTitle,
                            CycleVM.tNSProvider.PCPContactTelephone, CycleVM.tNSProvider.PCPContactEmail, CycleVM.tNSProvider.Medical, CycleVM.tNSProvider.MedicalSpecialty, CycleVM.tNSProvider.MedicalSpecialtyOther,
                            CycleVM.tNSProvider.ManagedCarePlan, CycleVM.tNSProvider.ManagedCarePlanOther);

                    }
                    //Update Navigation Cycle
                    getProxy().UpdateNavigationCycle(CycleVM.tNavigationCycle.NavigationCycleID, CycleVM.tNavigationCycle.FK_RecipientID, CycleVM.tNavigationCycle.FK_ProviderID, CycleVM.tNavigationCycle.FK_ReferralID, CycleVM.tNavigationCycle.CancerSite,
                    CycleVM.tNavigationCycle.NSProblem, CycleVM.tNavigationCycle.HealthProgram, CycleVM.tNavigationCycle.HealthInsuranceStatus, CycleVM.tNavigationCycle.HealthInsurancePlan, CycleVM.tNavigationCycle.HealthInsurancePlanNumber,
                    CycleVM.tNavigationCycle.ContactPrefDays, CycleVM.tNavigationCycle.ContactPrefHours, CycleVM.tNavigationCycle.ContactPrefHoursOther, CycleVM.tNavigationCycle.ContactPrefType, CycleVM.tNavigationCycle.SRIndentifierCodeGenerated,
                    CycleVM.tNavigationCycle.FK_NavigatorID);

                    TempData["FeedbackMessage"] = "Your changes were saved successfully.";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                TempData["FeedbackMessage"] = "Your changes were saved successfully.";
                return View(CycleVM);
            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);

                tNavigationCycle tnavigationCycle = GetNavigationCycle(CycleVM.tNavigationCycle.NavigationCycleID);

                NavigationCycleViewModel cycleVM = new NavigationCycleViewModel();
                cycleVM.tNavigationCycle = tnavigationCycle;

                //Weekdays
                PostedWeekday postedWeekday = new PostedWeekday();

                string weekdayString = cycleVM.tNavigationCycle.ContactPrefDays;
                string[] weekdays = weekdayString.Split(',');
                postedWeekday.WeekdayIds = weekdays;
                //Set up the weekdays in View Model
                cycleVM.SelectedWeekday = GetWeekdaySelected(postedWeekday);
                cycleVM.postedWeekday = postedWeekday;
                cycleVM.AvailableWeekday = GetAllWeekdays().ToList();

                //Times
                PostedTime postedTime = new PostedTime();

                string timeString = cycleVM.tNavigationCycle.ContactPrefHours;
                string[] times = timeString.Split(',');
                postedTime.TimeIds = times;
                //Set up the weekdays in View Model
                cycleVM.SelectedTime = GetTimeSelected(postedTime);
                cycleVM.postedTime = postedTime;
                cycleVM.AvailableTime = GetAllTimes().ToList();

                //Provider
                try
                {
                    if (tnavigationCycle.FK_ProviderID != null && tnavigationCycle.FK_ProviderID != 0)
                    {
                        CycleVM.tNSProvider = GetNSProvider((int)tnavigationCycle.FK_ProviderID);
                    }
                    else
                    {
                        //Save 0 in NSPRoviderID
                        tNSProvider tnsProvider = new tNSProvider();
                        tnsProvider.NSProviderID = 0;
                        CycleVM.tNSProvider = tnsProvider;
                    }
                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                //TempData["FeedbackMessage"] = "Your changes were saved successfully.";
                return View(cycleVM);
            }

           
        }

        #region Get All Weekdays
        /// <summary>
        /// for get all Weekday
        /// </summary>
        public static IEnumerable<WeekdayContactPreference> GetAllWeekdays()
        {
            List<WeekdayContactPreference> ls = new List<WeekdayContactPreference>();

            ls.Add(new WeekdayContactPreference() { WeekdayName = "Monday", WeekdayCode = 1 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Tuesday", WeekdayCode = 2 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Wednesday", WeekdayCode = 3 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Thursday", WeekdayCode = 4 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Friday", WeekdayCode = 5 });
            ls.Add(new WeekdayContactPreference() { WeekdayName = "Saturday", WeekdayCode = 6 });


            return ls;
        }
        #endregion

        #region Get Weekday Selected
        /// <summary>
        /// for setup view model, after post the user selected weekday data
        /// </summary>
        private List<WeekdayContactPreference> GetWeekdaySelected(PostedWeekday postedWeekday)
        {
            // setup properties
            var model = new NavigationCycleViewModel();
            var selectedWeekday = new List<WeekdayContactPreference>();
            var postedWeekdayIds = new string[0];
            if (postedWeekday == null) postedWeekday = new PostedWeekday();

            // if a view model array of posted weekday ids exists
            // and is not empty,save selected ids
            if (postedWeekday.WeekdayIds != null && postedWeekday.WeekdayIds.Any())
            {
                postedWeekdayIds = postedWeekday.WeekdayIds;
            }

            // if there are any selected ids saved, create a list of weekday
            if (postedWeekdayIds.Any())
            {
                selectedWeekday = GetAllWeekdays()
                 .Where(x => postedWeekdayIds.Any(s => x.WeekdayCode.ToString().Equals(s)))
                 .ToList();
            }

            ////setup a view model
            //model.AvailableWeekday = GetAllWeekdays().ToList();
            //model.SelectedWeekday = selectedWeekday;
            //model.postedWeekday = postedWeekday;
            return selectedWeekday;
        }
        #endregion

        #region Get Weekday List
        private string GetWeekdayList(NavigationCycleViewModel CycleVM)
        {
            var selectedWeekday = new List<WeekdayContactPreference>();
            var postedWeekdayIds = new string[0];
            var postedWeekday = new PostedWeekday();
            postedWeekday = CycleVM.postedWeekday;
            // if a view model array of posted weekday ids exists
            // and is not empty,save selected ids
            if (postedWeekday != null && postedWeekday.WeekdayIds != null && postedWeekday.WeekdayIds.Any())
            {
                postedWeekdayIds = postedWeekday.WeekdayIds;
            }

            // if there are any selected ids saved, create a list of weekdays
            if (postedWeekdayIds.Any())
            {
                selectedWeekday = GetAllWeekdays()
                 .Where(x => postedWeekdayIds.Any(s => x.WeekdayCode.ToString().Equals(s)))
                 .ToList();
            }

            //setup a view model
            CycleVM.AvailableWeekday = GetAllWeekdays().ToList();
            CycleVM.SelectedWeekday = selectedWeekday;
            CycleVM.postedWeekday = postedWeekday;


            string WeekdayList = "";
            if (postedWeekday != null)
            {
                foreach (string WeekdayId in postedWeekday.WeekdayIds)
                {
                    if (WeekdayId != null)
                    {
                        WeekdayList = WeekdayList + WeekdayId + ",";
                    }

                }
                WeekdayList = WeekdayList.Substring(0, WeekdayList.Length - 1);
            }
            
            //End weekday
            return WeekdayList;

        }
        #endregion

        #region Get All Times
        /// <summary>
        /// for get all Times
        /// </summary>
        public static IEnumerable<TimeContactPreference> GetAllTimes()
        {
            List<TimeContactPreference> ls = new List<TimeContactPreference>();

            ls.Add(new TimeContactPreference() { TimeName = "7am-9am", TimeCode = 1 });
            ls.Add(new TimeContactPreference() { TimeName = "10am-12pm", TimeCode = 2 });
            ls.Add(new TimeContactPreference() { TimeName = "1pm-3pm", TimeCode = 3 });
            ls.Add(new TimeContactPreference() { TimeName = "4pm-5pm", TimeCode = 4 });
            ls.Add(new TimeContactPreference() { TimeName = "6pm-9pm", TimeCode = 5 });
            ls.Add(new TimeContactPreference() { TimeName = "Other", TimeCode = 6 });


            return ls;
        }

        #endregion

        #region Get Time Selected
        /// <summary>
        /// for setup view model, after post the user selected time data
        /// </summary>
        private List<TimeContactPreference> GetTimeSelected(PostedTime postedTime)
        {
            // setup properties
            var model = new NavigationCycleViewModel();
            var selectedTime = new List<TimeContactPreference>();
            var postedTimeIds = new string[0];
            if (postedTime == null) postedTime = new PostedTime();

            // if a view model array of posted time ids exists
            // and is not empty,save selected ids
            if (postedTime.TimeIds != null && postedTime.TimeIds.Any())
            {
                postedTimeIds = postedTime.TimeIds;
            }

            // if there are any selected ids saved, create a list of times
            if (postedTimeIds.Any())
            {
                selectedTime = GetAllTimes()
                 .Where(x => postedTimeIds.Any(s => x.TimeCode.ToString().Equals(s)))
                 .ToList();
            }

            ////setup a view model
            //model.AvailableTime = GetAllTimes().ToList();
            //model.SelectedTime = selectedTime;
            //model.postedTime = postedTime;
            return selectedTime;
        }
        #endregion

        #region Get Time List
        private string GetTimeList(NavigationCycleViewModel CycleVM)
        {
            var selectedTime = new List<TimeContactPreference>();
            var postedTimeIds = new string[0];
            var postedTime = new PostedTime();
            postedTime = CycleVM.postedTime;
            // if a view model array of posted time ids exists
            // and is not empty,save selected ids
            if (postedTime != null && postedTime.TimeIds != null && postedTime.TimeIds.Any())
            {
                postedTimeIds = postedTime.TimeIds;
            }

            // if there are any selected ids saved, create a list of race
            if (postedTimeIds.Any())
            {
                selectedTime = GetAllTimes()
                 .Where(x => postedTimeIds.Any(s => x.TimeCode.ToString().Equals(s)))
                 .ToList();
            }

            //setup a view model
            CycleVM.AvailableTime = GetAllTimes().ToList();
            CycleVM.SelectedTime = selectedTime;
            CycleVM.postedTime = postedTime;


            string TimeList = "";
            if (postedTime != null)
            {
                foreach (string TimeId in postedTime.TimeIds)
                {
                    if (TimeId != null)
                    {
                        TimeList = TimeList + TimeId + ",";
                    }

                }
                TimeList = TimeList.Substring(0, TimeList.Length - 1);
            }
           
            //End time
            return TimeList;

        }
        #endregion

        #region Get Cancer Site Dropdown
        public static List<SelectListItem> GetCancerSiteDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            //get values from web service
            DataSet cancersitesDS = getProxy().GetCancerSitesList();
            try
            {
                for (var index = 0; index < cancersitesDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new SelectListItem() { Text = cancersitesDS.Tables[0].Rows[index]["CancerSiteCode"].ToString(), Value = cancersitesDS.Tables[0].Rows[index]["CancerSiteCode"].ToString() });
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return ls;
        }
        #endregion

        #region Get NS Problem Dropdown

        public static List<SelectListItem> GetNSProblemDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();
            //get values from web service
            DataSet nsproblemsDS = getProxy().GetNSProblemsList();
            try
            {
                for (var index = 0; index < nsproblemsDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new SelectListItem() { Text = nsproblemsDS.Tables[0].Rows[index]["NSProblemDescription"].ToString(), Value = nsproblemsDS.Tables[0].Rows[index]["NSProblemCode"].ToString() });
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return ls;
        }
        #endregion
        
        #region GetContactPrefDropDown
        public static List<SelectListItem> GetContactPrefDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            ls.Add(new SelectListItem() { Text = "Phone Call", Value = "1" });
            ls.Add(new SelectListItem() { Text = "E-mail", Value = "2" });
            ls.Add(new SelectListItem() { Text = "Text", Value = "3" });
            ls.Add(new SelectListItem() { Text = "Face-to-Face", Value = "4" });

            
            return ls;
        }

        #endregion

        #region Get Cancer Sites from DataSet
        private List<trCancerSite> GetCancerSitesFromDataSet()
        {
            List<trCancerSite> lsCancerSites = new List<trCancerSite>();

            try
            {
                DataSet cancerSitesDS = getProxy().GetCancerSitesList();
                for (var index = 0; index < cancerSitesDS.Tables[0].Rows.Count; index++)
                {
                    lsCancerSites.Add(new trCancerSite() { CancerSiteName = cancerSitesDS.Tables[0].Rows[index]["CancerSiteName"].ToString(), CancerSiteCode = (int)cancerSitesDS.Tables[0].Rows[index]["CancerSiteCode"] });
                }

            }
            catch
            {
                throw;
            }

            return lsCancerSites;
        }

        #endregion

        #region Get NS Problems from DataSet
        private List<trNSProblem> GetNSProblemsFromDataSet()
        {
            List<trNSProblem> lsNSProblems = new List<trNSProblem>();

            try
            {
                DataSet NSProblemsDS = getProxy().GetNSProblemsList();
                for (var index = 0; index < NSProblemsDS.Tables[0].Rows.Count; index++)
                {
                    lsNSProblems.Add(new trNSProblem() { NSProblemDescription = NSProblemsDS.Tables[0].Rows[index]["NSProblemDescription"].ToString(), NSProblemCode = (int)NSProblemsDS.Tables[0].Rows[index]["NSProblemCode"] });
                }

            }
            catch
            {
                throw;
            }

            return lsNSProblems;
        }

        #endregion

        #region Get Navigation Cycles From DataSet
        private List<tNavigationCycle> GetNavigationCyclesFromDataSet(int NSID)
        {
            List<tNavigationCycle> lsCycles = new List<tNavigationCycle>();
            try
            {
                DataSet cyclesDS = getProxy().SelectNavigationCycle(NSID);
                for (var index = 0; index < cyclesDS.Tables[0].Rows.Count; index++)
                {

                    lsCycles.Add(new tNavigationCycle()
                    {
                        NavigationCycleID = (int)cyclesDS.Tables[0].Rows[index]["NavigationCycleID"],
                        FK_RecipientID = (int)cyclesDS.Tables[0].Rows[index]["FK_RecipientID"],
                        FK_ProviderID = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["FK_ProviderID"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["FK_ProviderID"],
                        FK_ReferralID = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["FK_ReferralID"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["FK_ReferralID"],
                        CancerSite = (int)cyclesDS.Tables[0].Rows[index]["CancerSite"],
                        NSProblem = (int)cyclesDS.Tables[0].Rows[index]["NSProblem"],
                        HealthProgram = (int)cyclesDS.Tables[0].Rows[index]["HealthProgram"],
                        HealthInsuranceStatus = (int)cyclesDS.Tables[0].Rows[index]["HealthInsuranceStatus"],
                        HealthInsurancePlan = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["HealthInsurancePlan"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["HealthInsurancePlan"],
                        HealthInsurancePlanNumber = cyclesDS.Tables[0].Rows[index]["HealthInsurancePlanNumber"].ToString(),
                        ContactPrefDays = cyclesDS.Tables[0].Rows[index]["ContactPrefDays"].ToString(),
                        ContactPrefHours = cyclesDS.Tables[0].Rows[index]["ContactPrefHours"].ToString(),
                        ContactPrefHoursOther = cyclesDS.Tables[0].Rows[index]["ContactPrefHoursOther"].ToString(),
                        ContactPrefType = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["ContactPrefType"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["ContactPrefType"],
                        SRIndentifierCodeGenerated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["SRIndentifierCodeGenerated"]) ? false : (bool?)cyclesDS.Tables[0].Rows[index]["SRIndentifierCodeGenerated"],
                        FK_NavigatorID = (int)cyclesDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        xtag = cyclesDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)cyclesDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = cyclesDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)cyclesDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = cyclesDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }

            }
            catch
            {
                throw;
            }

            return lsCycles;
        }

        #endregion

        #region Get Navigation Cycle
        private tNavigationCycle GetNavigationCycle(int NavigationCycleID)
        {
            tNavigationCycle tNavigationCycle = new tNavigationCycle();
            try
            {
                DataSet cyclesDS = getProxy().GetNavigationCycle(0, NavigationCycleID, 0);
                for (var index = 0; index < cyclesDS.Tables[0].Rows.Count; index++)
                {

                    tNavigationCycle = new tNavigationCycle()
                    {
                        NavigationCycleID = (int)cyclesDS.Tables[0].Rows[index]["NavigationCycleID"],
                        FK_RecipientID = (int)cyclesDS.Tables[0].Rows[index]["FK_RecipientID"],
                        FK_ProviderID = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["FK_ProviderID"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["FK_ProviderID"],
                        FK_ReferralID = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["FK_ReferralID"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["FK_ReferralID"],
                        CancerSite = (int)cyclesDS.Tables[0].Rows[index]["CancerSite"],
                        NSProblem = (int)cyclesDS.Tables[0].Rows[index]["NSProblem"],
                        HealthProgram = (int)cyclesDS.Tables[0].Rows[index]["HealthProgram"],
                        HealthInsuranceStatus = (int)cyclesDS.Tables[0].Rows[index]["HealthInsuranceStatus"],
                        HealthInsurancePlan = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["HealthInsurancePlan"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["HealthInsurancePlan"],
                        HealthInsurancePlanNumber = cyclesDS.Tables[0].Rows[index]["HealthInsurancePlanNumber"].ToString(),
                        ContactPrefDays = cyclesDS.Tables[0].Rows[index]["ContactPrefDays"].ToString(),
                        ContactPrefHours = cyclesDS.Tables[0].Rows[index]["ContactPrefHours"].ToString(),
                        ContactPrefHoursOther = cyclesDS.Tables[0].Rows[index]["ContactPrefHoursOther"].ToString(),
                        ContactPrefType = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["ContactPrefType"]) ? 0 : (int?)cyclesDS.Tables[0].Rows[index]["ContactPrefType"],
                        SRIndentifierCodeGenerated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["SRIndentifierCodeGenerated"]) ? false : (bool?)cyclesDS.Tables[0].Rows[index]["SRIndentifierCodeGenerated"],
                        FK_NavigatorID = (int)cyclesDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        xtag = cyclesDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)cyclesDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = cyclesDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(cyclesDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)cyclesDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = cyclesDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    };
                }

            }
            catch
            {
                throw;
            }

            return tNavigationCycle;
        }

        #endregion

        #region Get NS Provider
        private tNSProvider GetNSProvider(int NSProviderID)
        {
            tNSProvider tNSProvider = new tNSProvider();
            try
            {
                DataSet providersDS = getProxy().SelectNSProvider(NSProviderID);
                tNSProvider = new tNSProvider()
                    {
                        NSProviderID = (int)providersDS.Tables[0].Rows[0]["NSProviderID"],
                        PCPName = providersDS.Tables[0].Rows[0]["PCPName"].ToString(),
                        EWCPCP = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["EWCPCP"]) ? false : (bool)providersDS.Tables[0].Rows[0]["EWCPCP"],
                        PCP_NPI = providersDS.Tables[0].Rows[0]["PCP_NPI"].ToString(),
                        PCP_Owner = providersDS.Tables[0].Rows[0]["PCP_Owner"].ToString(),
                        PCP_Location = providersDS.Tables[0].Rows[0]["PCP_Location"].ToString(),
                        PCPAddress1 = providersDS.Tables[0].Rows[0]["PCPAddress1"].ToString(),
                        PCPAddress2 = providersDS.Tables[0].Rows[0]["PCPAddress2"].ToString(),
                        PCPCity = providersDS.Tables[0].Rows[0]["PCPCity"].ToString(),
                        PCPState = providersDS.Tables[0].Rows[0]["PCPState"].ToString(),
                        PCPZip = providersDS.Tables[0].Rows[0]["PCPZip"].ToString(),
                        PCPContactFName = providersDS.Tables[0].Rows[0]["PCPContactFName"].ToString(),
                        PCPContactLName = providersDS.Tables[0].Rows[0]["PCPContactLName"].ToString(),
                        PCPContactTitle = providersDS.Tables[0].Rows[0]["PCPContactTitle"].ToString(),
                        PCPContactTelephone = providersDS.Tables[0].Rows[0]["PCPContactTelephone"].ToString(),
                        PCPContactEmail = providersDS.Tables[0].Rows[0]["PCPContactEmail"].ToString(),
                        Medical = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["Medical"]) ? false : (bool)providersDS.Tables[0].Rows[0]["Medical"],
                        MedicalSpecialty = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["MedicalSpecialty"]) ? 0 : (int)providersDS.Tables[0].Rows[0]["MedicalSpecialty"],
                        MedicalSpecialtyOther = providersDS.Tables[0].Rows[0]["MedicalSpecialtyOther"].ToString(),
                        ManagedCarePlan = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["ManagedCarePlan"]) ? 0 : (int?)providersDS.Tables[0].Rows[0]["ManagedCarePlan"],
                        ManagedCarePlanOther = providersDS.Tables[0].Rows[0]["ManagedCarePlanOther"].ToString(),
                        xtag = providersDS.Tables[0].Rows[0]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["DateCreated"]) ? DateTime.MinValue : (DateTime)providersDS.Tables[0].Rows[0]["DateCreated"],
                        CreatedBy = providersDS.Tables[0].Rows[0]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(providersDS.Tables[0].Rows[0]["LastUpdated"]) ? DateTime.MinValue : (DateTime)providersDS.Tables[0].Rows[0]["LastUpdated"],
                        UpdatedBy = providersDS.Tables[0].Rows[0]["UpdatedBy"].ToString()};
                

            }
            catch
            {
                throw;
            }

            return tNSProvider;
        }

        #endregion

        #region Get Health Program Dropdown
        public static List<SelectListItem> GetHealthProgramDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            //get values from web service
            DataSet healthprogramDS = getProxy().GetHealthProgramsList();
            try
            {
                for (var index = 0; index < healthprogramDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new SelectListItem() { Text = healthprogramDS.Tables[0].Rows[index]["HealthProgramName"].ToString(), Value = healthprogramDS.Tables[0].Rows[index]["HealthProgramCode"].ToString() });
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return ls;
        }
        #endregion

        #region Get Health Insurance Dropdown
        public static List<SelectListItem> GetHealthInsuranceDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            //get values from web service
            DataSet healthinsuranceDS = getProxy().GetHealthInsuranceList();
            try
            {
                for (var index = 0; index < healthinsuranceDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new SelectListItem() { Text = healthinsuranceDS.Tables[0].Rows[index]["HealthInsuranceName"].ToString(), Value = healthinsuranceDS.Tables[0].Rows[index]["HealthInsuranceCode"].ToString() });
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return ls;
        }
        #endregion

        #region Get Health Programs from DataSet
        private List<trHealthProgram> GetHealthProgramsFromDataSet()
        {
            List<trHealthProgram> lsHealthPrograms = new List<trHealthProgram>();

            try
            {
                DataSet HealthProgramsDS = getProxy().GetHealthProgramsList();
                for (var index = 0; index < HealthProgramsDS.Tables[0].Rows.Count; index++)
                {
                    lsHealthPrograms.Add(new trHealthProgram() { HealthProgramName = HealthProgramsDS.Tables[0].Rows[index]["HealthProgramName"].ToString(), HealthProgramCode = (int)HealthProgramsDS.Tables[0].Rows[index]["HealthProgramCode"] });
                }

            }
            catch
            {
                throw;
            }

            return lsHealthPrograms;
        }

        #endregion

        #region Get Health Insurance from DataSet
        private List<trHealthInsurance> GetHealthInsuranceFromDataSet()
        {
            List<trHealthInsurance> lsHealthInsurance = new List<trHealthInsurance>();

            try
            {
                DataSet HealthInsuranceDS = getProxy().GetHealthInsuranceList();
                for (var index = 0; index < HealthInsuranceDS.Tables[0].Rows.Count; index++)
                {
                    lsHealthInsurance.Add(new trHealthInsurance() { HealthInsuranceName = HealthInsuranceDS.Tables[0].Rows[index]["HealthInsuranceName"].ToString(), HealthInsuranceCode = (int)HealthInsuranceDS.Tables[0].Rows[index]["HealthInsuranceCode"] });
                }

            }
            catch
            {
                throw;
            }

            return lsHealthInsurance;
        }

        #endregion

        #region Get Recipients From DataSet
        private List<tServiceRecipient> GetRecipientsFromDataSet()
        {
            List<tServiceRecipient> lsRecipients = new List<tServiceRecipient>();
            try
            {
                DataSet recipientsDS = getProxy().SelectServiceRecipient(GetNavigatorID());
                for (var index = 0; index < recipientsDS.Tables[0].Rows.Count; index++)
                {

                    lsRecipients.Add(new tServiceRecipient()
                    {
                        NSRecipientID = (int)recipientsDS.Tables[0].Rows[index]["NSRecipientID"],
                        FK_NavigatorID = (int)recipientsDS.Tables[0].Rows[index]["FK_NavigatorID"],
                        FK_RecipID = recipientsDS.Tables[0].Rows[index]["FK_RecipID"].ToString(),
                        LastName = recipientsDS.Tables[0].Rows[index]["LastName"].ToString(),
                        FirstName = recipientsDS.Tables[0].Rows[index]["FirstName"].ToString(),
                        MiddleInitial = recipientsDS.Tables[0].Rows[index]["MiddleInitial"].ToString(),
                        DOB = (DateTime)recipientsDS.Tables[0].Rows[index]["DOB"],
                        Address1 = recipientsDS.Tables[0].Rows[index]["Address1"].ToString(),
                        Address2 = recipientsDS.Tables[0].Rows[index]["Address2"].ToString(),
                        City = recipientsDS.Tables[0].Rows[index]["City"].ToString(),
                        State = recipientsDS.Tables[0].Rows[index]["State"].ToString(),
                        Zip = recipientsDS.Tables[0].Rows[index]["Zip"].ToString(),
                        Phone1 = recipientsDS.Tables[0].Rows[index]["Phone1"].ToString(),
                        P1PhoneType = (short)recipientsDS.Tables[0].Rows[index]["P1PhoneType"],
                        P1PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P1PersonalMessage"],
                        Phone2 = recipientsDS.Tables[0].Rows[index]["Phone2"].ToString(),
                        P2PhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["P2PhoneType"],
                        P2PersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["P2PersonalMessage"],
                        MotherMaidenName = recipientsDS.Tables[0].Rows[index]["MotherMaidenName"].ToString(),
                        Email = recipientsDS.Tables[0].Rows[index]["Email"].ToString(),
                        SSN = recipientsDS.Tables[0].Rows[index]["SSN"].ToString(),
                        ImmigrantStatus = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["ImmigrantStatus"],
                        CountryOfOrigin = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CountryOfOrigin"],
                        Gender = recipientsDS.Tables[0].Rows[index]["Gender"].ToString(),
                        Ethnicity = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Ethnicity"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["Ethnicity"],
                        PrimaryLanguage = (short)recipientsDS.Tables[0].Rows[index]["PrimaryLanguage"],
                        AgreeToSpeakEnglish = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["AgreeToSpeakEnglish"],
                        CaregiverName = recipientsDS.Tables[0].Rows[index]["CaregiverName"].ToString(),
                        Relationship = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["Relationship"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["Relationship"],
                        RelationshipOther = recipientsDS.Tables[0].Rows[index]["RelationshipOther"].ToString(),
                        CaregiverPhone = recipientsDS.Tables[0].Rows[index]["CaregiverPhone"].ToString(),
                        CaregiverPhoneType = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"]) ? 0 : (short?)recipientsDS.Tables[0].Rows[index]["CaregiverPhoneType"],
                        CaregiverPhonePersonalMessage = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"]) ? false : (bool)recipientsDS.Tables[0].Rows[index]["CaregiverPhonePersonalMessage"],
                        CaregiverEmail = recipientsDS.Tables[0].Rows[index]["CaregiverEmail"].ToString(),
                        CaregiverContactPreference = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"]) ? 0 : (int)recipientsDS.Tables[0].Rows[index]["CaregiverContactPreference"],
                        Comments = recipientsDS.Tables[0].Rows[index]["Comments"].ToString(),
                        xtag = recipientsDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = recipientsDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(recipientsDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)recipientsDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = recipientsDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }

            }
            catch
            {
                throw;
            }

            return lsRecipients;
        }

        #endregion

    }
}
