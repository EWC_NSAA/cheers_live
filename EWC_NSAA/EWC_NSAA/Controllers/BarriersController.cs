﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.Models;
using EWC_NSAA.ViewModels;
using EWC_NSAA.Common;
using System.Web.Routing;

namespace EWC_NSAA.Controllers
{
    //[Authorize(Roles = @"EWC_NSAA_Contributors, EWC_NSAA_A")]
    public class BarriersController : BaseController
    {

        
             public ActionResult clinicalservices()
        {
            return View();
        }
        public ActionResult Assesment()
        {
            return View();
        }
        // GET: Barriers/Create
        public ActionResult Create()
        {
            BarrierViewModel BarrierVM = new BarrierViewModel();
            return PartialView("Create", BarrierVM);
        }

        // POST: Barriers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BarrierViewModel BarrierVM, int EncounterID) //[Bind(Include = "BarrierID,FK_EncounterID,BarrierType,BarrierAssessed,FollowUpDt,xtag,DateCreated,CreatedBy,LastUpdated,UpdatedBy")] tBarrier tBarrier)
        {
            var BarrierID = 0;

            tBarrier tBarrier = new tBarrier();
            tSolution tSolution = new tSolution();

            if (ModelState.IsValid)
            {

                try
                {
                    if (BarrierVM.SelectedBarrierType > 0)
                    {
                        //Create the tBarrier object
                        tBarrier.BarrierType = BarrierVM.SelectedBarrierType;
                        tBarrier.BarrierAssessed = BarrierVM.SelectedBarrier;
                        tBarrier.CreatedBy = "";
                        tBarrier.FK_EncounterID = EncounterID;
                        tBarrier.FollowUpDt = DateTime.MinValue;

                        //Insert Barrier and get Barrier ID
                        BarrierID = getProxy().InsertBarrier(EncounterID, tBarrier.BarrierType, tBarrier.BarrierAssessed, tBarrier.FollowUpDt);

                        string Solutions = BarrierVM.SelectedSolution;
                        string[] solutions = Solutions.Split(',');

                        string FollowupDt = BarrierVM.SelectedFollowupDt;
                        string[] followupdt = FollowupDt.Split(',');

                        string ImplementationStatus = BarrierVM.SelectedImplementationStatus;
                        string[] implementationstatus = ImplementationStatus.Split(',');


                        for (int i = 0; i < solutions.Count(); i++)
                        {
                            string sol = solutions[i].Substring(solutions[i].IndexOf(":") + 1);
                            string fup = followupdt[i].Substring(followupdt[i].IndexOf(":") + 1);
                            string imp = implementationstatus[i].Substring(implementationstatus[i].IndexOf(":") + 1);

                            if (sol != "") //Solution is checked
                            {

                                tSolution.SolutionOffered = Convert.ToInt32(sol);
                                if (fup != "")
                                { tSolution.FollowUpDt = Convert.ToDateTime(fup); }
                                else
                                { tSolution.FollowUpDt = DateTime.MinValue; }

                                tSolution.SolutionImplementationStatus = Convert.ToInt32(imp);

                                getProxy().InsertSolution(BarrierID, tSolution.SolutionOffered, tSolution.SolutionImplementationStatus, tSolution.FollowUpDt);
                            }
                        }
                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);

            }


            //ViewBag.FK_EncounterID = new SelectList(db.tEncounter, "EncounterID", "EncounterStartTime", tBarrier.FK_EncounterID);
            return View(BarrierVM);
        }

        // GET: Barriers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tBarrier tBarrier = GetBarrier((int)id);
            if (tBarrier == null)
            {
                return HttpNotFound();
            }

            BarrierViewModel BarrierVM = new BarrierViewModel();
            BarrierVM.tBarrier = tBarrier;

            BarrierVM.SelectedBarrierType = BarrierVM.tBarrier.BarrierType;
            BarrierVM.SelectedBarrier = BarrierVM.tBarrier.BarrierAssessed;
            
            try
            {
                List<tSolution> solutionsList = GetSolutionsFromDataSet(BarrierVM.tBarrier.BarrierID);
                BarrierVM.Solutions = solutionsList.ToList();
                if(BarrierVM.Solutions.Count > 0)
                {
                    foreach(tSolution solution in BarrierVM.Solutions)
                    {
                        if(solution.FollowUpDt == DateTime.MinValue)
                        {
                            solution.FollowUpDt = null;
                        }
                    }
                }
                

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return View(BarrierVM);
        }

        // POST: Barriers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(BarrierViewModel BarrierVM)//[Bind(Include = "BarrierID,FK_EncounterID,BarrierType,BarrierAssessed,FollowUpDt,xtag,DateCreated,CreatedBy,LastUpdated,UpdatedBy")] tBarrier tBarrier)
        {
            var BarrierID = BarrierVM.tBarrier.BarrierID;

            tBarrier tBarrier = new tBarrier();
            tSolution tSolution = new tSolution();

            if (ModelState.IsValid)
            {
                
                try
                {
                    if (BarrierVM.SelectedBarrierType > 0)
                    {
                        //Create the tBarrier object
                        tBarrier.BarrierType = BarrierVM.SelectedBarrierType;
                        tBarrier.BarrierAssessed = BarrierVM.SelectedBarrier;
                        tBarrier.CreatedBy = "";
                        tBarrier.FK_EncounterID = BarrierVM.tBarrier.FK_EncounterID;
                        tBarrier.FollowUpDt = DateTime.MinValue;


                        //Update Barrier
                        getProxy().UpdateBarrier(BarrierID, tBarrier.FK_EncounterID, tBarrier.BarrierType, tBarrier.BarrierAssessed, tBarrier.FollowUpDt);

                        string Solutions = BarrierVM.SelectedSolution;
                        string[] solutions = Solutions.Split(',');

                        string FollowupDt = BarrierVM.SelectedFollowupDt;
                        string[] followupdt = FollowupDt.Split(',');

                        string ImplementationStatus = BarrierVM.SelectedImplementationStatus;
                        string[] implementationstatus = ImplementationStatus.Split(',');

                        //Remove all Solutions for this barrier before adding 
                        int retVal = getProxy().DeleteSolutions(BarrierID);

                        for (int i = 0; i < solutions.Count(); i++)
                        {
                            string sol = solutions[i].Substring(solutions[i].IndexOf(":") + 1);
                            string fup = followupdt[i].Substring(followupdt[i].IndexOf(":") + 1);
                            string imp = implementationstatus[i].Substring(implementationstatus[i].IndexOf(":") + 1);

                            if (sol != "") //Solution is checked
                            {

                                tSolution.SolutionOffered = Convert.ToInt32(sol);
                                if (fup != "")
                                { tSolution.FollowUpDt = Convert.ToDateTime(fup); }
                                else
                                { tSolution.FollowUpDt = DateTime.MinValue; }

                                tSolution.SolutionImplementationStatus = Convert.ToInt32(imp);

                                getProxy().InsertSolution(BarrierID, tSolution.SolutionOffered, tSolution.SolutionImplementationStatus, tSolution.FollowUpDt);
                            }
                        }

                    }

                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }
                //return View(EncounterVM);

                TempData["FeedbackMessage"] = "Your changes were saved successfully.";

                return RedirectToAction("Edit", "Encounters", new { id = BarrierVM.tBarrier.FK_EncounterID });
            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);

                tBarrier = GetBarrier(BarrierID);
                if (tBarrier == null)
                {
                    return HttpNotFound();
                }

                BarrierVM.tBarrier = tBarrier;

                BarrierVM.SelectedBarrierType = BarrierVM.tBarrier.BarrierType;
                BarrierVM.SelectedBarrier = BarrierVM.tBarrier.BarrierAssessed;

                try
                {
                    List<tSolution> solutionsList = GetSolutionsFromDataSet(BarrierVM.tBarrier.BarrierID);
                    BarrierVM.Solutions = solutionsList.ToList();

                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                return View(BarrierVM);
            }

        }

        [HttpPost]
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult BarrierTypePost(int paramBarrierType)
        {
            //ViewBag.BarrierType = paramBarrierType;

            List<trBarrier> objbarrier = new List<trBarrier>();
            objbarrier = GetBarrierLookup().Where(m => m.BarrierTypeCode == paramBarrierType).ToList();
            SelectList obgbarrier = new SelectList(objbarrier, "BarrierCode", "BarrierDescription", 0);
            return Json(obgbarrier);

        }

        [HttpPost]
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.None)]
        public ActionResult BarrierPost(int paramBarrierType)
        {
            //ViewBag.Barrier = paramBarrier;

            List<trBarrierSolution> objbarriersolution = new List<trBarrierSolution>();
            objbarriersolution = GetBarrierSolutionLookup().Where(m => m.BarrierTypeCode == paramBarrierType).ToList();
            SelectList obgbarriersolution = new SelectList(objbarriersolution, "BarrierSolutionCode", "BarrierSolutionDescription", 0);
            return Json(obgbarriersolution);

        }


        #region Get Solutions From DataSet
        private List<tSolution> GetSolutionsFromDataSet(int BarrierID)
        {
            List<tSolution> lsSolutions = new List<tSolution>();
            try
            {
                DataSet solutionsDS = getProxy().GetSolutionsList(BarrierID);
                for (var index = 0; index < solutionsDS.Tables[0].Rows.Count; index++)
                {

                    lsSolutions.Add(new tSolution()
                    {
                        SolutionID = (int)solutionsDS.Tables[0].Rows[index]["SolutionID"],
                        FK_BarrierID = (int)solutionsDS.Tables[0].Rows[index]["FK_BarrierID"],
                        SolutionOffered = (int)solutionsDS.Tables[0].Rows[index]["SolutionOffered"],
                        SolutionImplementationStatus = Convert.IsDBNull(solutionsDS.Tables[0].Rows[index]["SolutionImplementationStatus"]) ? 0 : (int)solutionsDS.Tables[0].Rows[index]["SolutionImplementationStatus"],
                        FollowUpDt = Convert.IsDBNull(solutionsDS.Tables[0].Rows[index]["FollowUpDt"]) ? DateTime.MinValue : (DateTime)solutionsDS.Tables[0].Rows[index]["FollowUpDt"]
                    });
                        
                }

            }
            catch
            {
                throw;
            }

            return lsSolutions;
        }

        #endregion

        #region Get Barrier
        private tBarrier GetBarrier(int BarrierID)
        {
            tBarrier tbarrier = new tBarrier();
            try
            {
                DataSet barriersDS = getProxy().GetBarrier(BarrierID);
                for (var index = 0; index < barriersDS.Tables[0].Rows.Count; index++)
                {

                    tbarrier = new tBarrier()
                    {
                        BarrierID = (int)barriersDS.Tables[0].Rows[index]["BarrierID"],
                        FK_EncounterID = (int)barriersDS.Tables[0].Rows[index]["FK_EncounterID"],
                        BarrierType = (int)barriersDS.Tables[0].Rows[index]["BarrierType"],
                        BarrierAssessed = (int)barriersDS.Tables[0].Rows[index]["BarrierAssessed"],
                        FollowUpDt = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["FollowUpDt"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["FollowUpDt"],
                        xtag = barriersDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = barriersDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = barriersDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    };
                }

            }
            catch
            {
                throw;
            }

            return tbarrier;
        }

        #endregion

        #region Get Barrier Type Lookup
        public static List<SelectListItem> GetBarrierTypeLookup()
        {
            List<SelectListItem> ls = new List<SelectListItem>();
            //get values from web service

            DataSet barrierTypeDS = getProxy().SelectBarrierType();

            try
            {
                for (var index = 0; index < barrierTypeDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new SelectListItem() { Text = barrierTypeDS.Tables[0].Rows[index]["BarrierTypeDescription"].ToString(), Value = barrierTypeDS.Tables[0].Rows[index]["BarrierTypeCode"].ToString() });
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }


            return ls;
        }
        #endregion

        #region Get Barrier Lookup
        public static List<trBarrier> GetBarrierLookup()
        {

            List<trBarrier> ls = new List<trBarrier>();

            DataSet barrierDS = getProxy().SelectBarriers();

            try
            {
                for (var index = 0; index < barrierDS.Tables[0].Rows.Count; index++)
                {
                    
                    ls.Add(new trBarrier()
                    {
                        BarrierDescription = barrierDS.Tables[0].Rows[index]["BarrierDescription"].ToString(),
                        BarrierCode = (int)barrierDS.Tables[0].Rows[index]["BarrierCode"],
                        BarrierTypeCode = (int)barrierDS.Tables[0].Rows[index]["BarrierTypeCode"]
                    });
                        
                   
                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return ls;
        }

        #endregion

        #region Get Barrier Solution Lookup
        public static List<trBarrierSolution> GetBarrierSolutionLookup()
        {
            List<trBarrierSolution> ls = new List<trBarrierSolution>();

            DataSet barrierSolutionDS = getProxy().SelectBarrierSolutions();

            try
            {
                for (var index = 0; index < barrierSolutionDS.Tables[0].Rows.Count; index++)
                {
                    ls.Add(new trBarrierSolution()
                    {
                        BarrierSolutionDescription = barrierSolutionDS.Tables[0].Rows[index]["BarrierSolutionDescription"].ToString(),
                        BarrierSolutionCode = (int)barrierSolutionDS.Tables[0].Rows[index]["BarrierSolutionCode"],
                        BarrierTypeCode = (int)barrierSolutionDS.Tables[0].Rows[index]["BarrierTypeCode"]
                    });

                }

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return ls;
        }

        #endregion

        #region Get Barriers From DataSet
        private List<tBarrier> GetBarriersFromDataSet(int EncounterID)
        {
            List<tBarrier> lsBarriers = new List<tBarrier>();
            try
            {
                DataSet barriersDS = getProxy().GetBarriersList(EncounterID);
                for (var index = 0; index < barriersDS.Tables[0].Rows.Count; index++)
                {

                    lsBarriers.Add(new tBarrier()
                    {
                        BarrierID = (int)barriersDS.Tables[0].Rows[index]["BarrierID"],
                        FK_EncounterID = (int)barriersDS.Tables[0].Rows[index]["FK_EncounterID"],
                        BarrierType = (int)barriersDS.Tables[0].Rows[index]["BarrierType"],
                        BarrierAssessed = (int)barriersDS.Tables[0].Rows[index]["BarrierAssessed"],
                        FollowUpDt = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["FollowUpDt"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["FollowUpDt"],
                        xtag = barriersDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = barriersDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(barriersDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)barriersDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = barriersDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }

            }
            catch
            {
                throw;
            }

            return lsBarriers;
        }

        #endregion
    }
}
