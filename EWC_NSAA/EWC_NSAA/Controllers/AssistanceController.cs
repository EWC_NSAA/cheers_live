﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EWC_NSAA.Models;
using EWC_NSAA.ViewModels;
using EWC_NSAA.Common;
using System.Web.Mvc;

namespace EWC_NSAA.Controllers
{
    //[Authorize(Roles = @"EWC_NSAA_Contributors, EWC_NSAA_A")]
    public class AssistanceController: BaseController
    {
        public ActionResult Index()

        {
            return View();
        }
        public ActionResult Create()
        {
            AssistanceViewModel assistanceVM = new AssistanceViewModel();
            return View(assistanceVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AssistanceViewModel AssistanceVM)
        {
            int AssistanceID = 0;
            int NavigatorID = 0;

            if (ModelState.IsValid)
            {
                

                try
                {
                    AssistanceVM.FK_NavigatorID = GetNavigatorID(); //NavigatorVM.tNavigator.NavigatorID;
                    NavigatorID = GetNavigatorID();
                }
                catch (Exception e)
                {
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }


                try
                {
                    int retValue = getProxy().InsertAssistance(AssistanceVM.FK_NavigatorID,
                    AssistanceVM.IncidentNumber,
                    AssistanceVM.IncidentDt,
                    AssistanceVM.FK_CommunicationCode,
                    AssistanceVM.FirstName,
                    AssistanceVM.LastName, 
                    AssistanceVM.Telephone,
                    AssistanceVM.Email,
                    AssistanceVM.RecipID,
                    AssistanceVM.FK_RequestorCode,
                    AssistanceVM.NPI,
                    AssistanceVM.NPIOwner,
                    AssistanceVM.NPISvcLoc,
                    AssistanceVM.BusinessName
                    );

                    AssistanceID = retValue;

                    TempData["FeedbackMessage"] = "Your changes were saved successfully.";
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
                }

                return RedirectToAction("ViewEnrolledRecipient", "Enrollment");
                //return View(RecipVM);

            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                
                return View(AssistanceVM);
            }

        }
    }
}