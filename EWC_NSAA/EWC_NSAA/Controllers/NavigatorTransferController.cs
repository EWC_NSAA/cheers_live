﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EWC_NSAA.ViewModels;
using System.Data;


namespace EWC_NSAA.Controllers
{
    //[Authorize(Roles = @"EWC_NSAA_Contributors, EWC_NSAA_A")]
    public class NavigatorTransferController : BaseController
    {
        // GET: NavigatorTransfer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexInitiated(int CurrentNavigatorID)
        {
            NavigatorTransferInterimListViewModel NavigatorTransferVM = new NavigatorTransferInterimListViewModel();
            
            List<NavigatorTransferInterimViewModel> lsNavigatorTransfers = new List<NavigatorTransferInterimViewModel>();

            try
            {
                
                lsNavigatorTransfers = GetInitiatedTransfersFromDataSet(CurrentNavigatorID);

                NavigatorTransferVM.NavigatorTransfers = lsNavigatorTransfers;
                

            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return View(NavigatorTransferVM);
        }

        public ActionResult IndexRequested(int CurrentNavigatorID)
        {
            NavigatorTransferInterimListViewModel NavigatorTransferVM = new NavigatorTransferInterimListViewModel();

            List<NavigatorTransferInterimViewModel> lsNavigatorTransfers = new List<NavigatorTransferInterimViewModel>();

            try
            {

                lsNavigatorTransfers = GetRequestedTransfersFromDataSet(CurrentNavigatorID);

                NavigatorTransferVM.NavigatorTransfers = lsNavigatorTransfers;


            }
            catch (Exception e)
            {
                throw; //Errors.PublishException(e, GetAppName(false), GetVersion());
            }

            return View(NavigatorTransferVM);
        }

        public ActionResult NavigatorTransferStatus(int TransferID, int NSID, int NavigatorID)
        {
            NavigatorTransferInterimViewModel navigatortransferinterimVM = new NavigatorTransferInterimViewModel();

            DataSet dsNavigatorTransferInterim = getProxy().GetNavigatorInfo(1, NSID, NavigatorID, 0, DateTime.MinValue, 0);

            DataSet dsRecipient = getProxy().GetNavigatorInfo(4, NSID, NavigatorID, 0, DateTime.MinValue, 0);

            navigatortransferinterimVM.TransferID = TransferID;
            navigatortransferinterimVM.FK_RecipientID = NSID;
            navigatortransferinterimVM.CurrentNavigatorID = NavigatorID;
            navigatortransferinterimVM.FirstName = dsNavigatorTransferInterim.Tables[0].Rows[0]["FirstName"].ToString();
            navigatortransferinterimVM.LastName = dsNavigatorTransferInterim.Tables[0].Rows[0]["LastName"].ToString();
            navigatortransferinterimVM.Region = (int)dsNavigatorTransferInterim.Tables[0].Rows[0]["Region"];

            navigatortransferinterimVM.RecipFirstName = dsRecipient.Tables[0].Rows[0]["FirstName"].ToString();
            navigatortransferinterimVM.RecipLastName = dsRecipient.Tables[0].Rows[0]["LastName"].ToString();
            navigatortransferinterimVM.Language = dsRecipient.Tables[0].Rows[0]["PrimaryLanguage"].ToString();
            navigatortransferinterimVM.AgreeToSpeakEnglish = dsRecipient.Tables[0].Rows[0]["AgreeToSpeakEnglish"].ToString();

            navigatortransferinterimVM.TransferDt = DateTime.Today;

            switch ((int)dsNavigatorTransferInterim.Tables[0].Rows[0]["Type"])
            {
                case 1:
                    navigatortransferinterimVM.Type = "Health Educator";
                    break;
                case 2:
                    navigatortransferinterimVM.Type = "Clinical Coordinator";
                    break;
                case 3:
                    navigatortransferinterimVM.Type = "CHW";
                    break;
                case 4:
                    navigatortransferinterimVM.Type = "Other";
                    break;
            }

            return View(navigatortransferinterimVM);
        }

        [HttpPost]  
        public ActionResult NavigatorTransferStatus(NavigatorTransferInterimViewModel navigatortransferinterimVM)
        {

            int TransferID = navigatortransferinterimVM.TransferID;
            int FK_RecipientID = navigatortransferinterimVM.FK_RecipientID;
            int CurrentNavigatorID = navigatortransferinterimVM.CurrentNavigatorID;
            int NewNavigatorID = navigatortransferinterimVM.NewNavigatorID;
            DateTime NavigatorTransferDt = navigatortransferinterimVM.TransferDt;
            string NavigatorTransferReason = navigatortransferinterimVM.NavigatorTransferReason;
            int NavigatorTransferStatus = navigatortransferinterimVM.TransferStatus;
            DateTime TransferRefusedDt = navigatortransferinterimVM.TransferRefusedDt;
            string TransferRefusedReason = navigatortransferinterimVM.TransferRefusedReason;

            if (navigatortransferinterimVM.TransferStatus == 1)
            {

                //Transfer Accepted
                //Update Transfer Status here
                getProxy().UpdateTransferStatus(TransferID, NavigatorTransferStatus, TransferRefusedReason, TransferRefusedDt);

                getProxy().TransferNavigator(2, FK_RecipientID, CurrentNavigatorID, NewNavigatorID, NavigatorTransferDt, NavigatorTransferReason);
                TempData["FeedbackMessage"] = "Recipient transfer has been completed.";
            }

            if (navigatortransferinterimVM.TransferStatus == 2)
            {
                //Transfer Refused
                //Update Transfer Status here
                getProxy().UpdateTransferStatus(TransferID, NavigatorTransferStatus, TransferRefusedReason, TransferRefusedDt);

                TempData["FeedbackMessage"] = "Recipient transfer has been rejected.";
            }   

            return RedirectToAction("NavigatorTransfer", "Enrollment", new { NSID = FK_RecipientID, NavigatorID = NewNavigatorID });

        }

        #region Get Initiated Navigator Transfers From DataSet
        private List<NavigatorTransferInterimViewModel> GetInitiatedTransfersFromDataSet(int CurrentNavigatorID)
        {
            List<NavigatorTransferInterimViewModel> lsNavigatorTransfers = new List<NavigatorTransferInterimViewModel>();
            try
            {
                DataSet navigatortransfersDS = getProxy().GetNavigatorInfo(6, 0, CurrentNavigatorID, 0, DateTime.MinValue, 0);
                for (var index = 0; index < navigatortransfersDS.Tables[0].Rows.Count; index++)
                {

                    lsNavigatorTransfers.Add(new NavigatorTransferInterimViewModel()
                    {
                        FK_RecipientID = (int)navigatortransfersDS.Tables[0].Rows[index]["FK_RecipientID"],
                        CurrentNavigatorID = (int)navigatortransfersDS.Tables[0].Rows[index]["CurrentNavigatorID"],
                        NewNavigatorID = (int)navigatortransfersDS.Tables[0].Rows[index]["NewNavigatorID"],
                        NavigatorTransferReason = navigatortransfersDS.Tables[0].Rows[index]["NavigatorTransferReason"].ToString(),
                        TransferDt = (DateTime)navigatortransfersDS.Tables[0].Rows[index]["NavigatorTransferDt"],
                        TransferStatus = Convert.IsDBNull(navigatortransfersDS.Tables[0].Rows[index]["TransferStatus"]) ? 0 : (int)(navigatortransfersDS.Tables[0].Rows[index]["TransferStatus"]),
                        TransferRefusedReason = navigatortransfersDS.Tables[0].Rows[index]["TransferRefusedReason"].ToString(),
                        xtag = navigatortransfersDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(navigatortransfersDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)navigatortransfersDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = navigatortransfersDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(navigatortransfersDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)navigatortransfersDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = navigatortransfersDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }

            }
            catch
            {
                throw;
            }

            return lsNavigatorTransfers;
        }

        #endregion

        #region Get Requested Navigator Transfers From DataSet
        private List<NavigatorTransferInterimViewModel> GetRequestedTransfersFromDataSet(int CurrentNavigatorID)
        {
            List<NavigatorTransferInterimViewModel> lsNavigatorTransfers = new List<NavigatorTransferInterimViewModel>();
            try
            {
                DataSet navigatortransfersDS = getProxy().GetNavigatorInfo(7, 0, CurrentNavigatorID, 0, DateTime.MinValue, 0);
                for (var index = 0; index < navigatortransfersDS.Tables[0].Rows.Count; index++)
                {

                    lsNavigatorTransfers.Add(new NavigatorTransferInterimViewModel()
                    {
                        TransferID = (int)navigatortransfersDS.Tables[0].Rows[index]["TransferID"],
                        FK_RecipientID = (int)navigatortransfersDS.Tables[0].Rows[index]["FK_RecipientID"],
                        CurrentNavigatorID = (int)navigatortransfersDS.Tables[0].Rows[index]["CurrentNavigatorID"],
                        NewNavigatorID = (int)navigatortransfersDS.Tables[0].Rows[index]["NewNavigatorID"],
                        NavigatorTransferReason = navigatortransfersDS.Tables[0].Rows[index]["NavigatorTransferReason"].ToString(),
                        TransferDt = (DateTime)navigatortransfersDS.Tables[0].Rows[index]["NavigatorTransferDt"],
                        TransferStatus = Convert.IsDBNull(navigatortransfersDS.Tables[0].Rows[index]["TransferStatus"]) ? 0 : (int)(navigatortransfersDS.Tables[0].Rows[index]["TransferStatus"]),
                        TransferRefusedReason = navigatortransfersDS.Tables[0].Rows[index]["TransferRefusedReason"].ToString(),
                        xtag = navigatortransfersDS.Tables[0].Rows[index]["xtag"].ToString(),
                        DateCreated = Convert.IsDBNull(navigatortransfersDS.Tables[0].Rows[index]["DateCreated"]) ? DateTime.MinValue : (DateTime)navigatortransfersDS.Tables[0].Rows[index]["DateCreated"],
                        CreatedBy = navigatortransfersDS.Tables[0].Rows[index]["CreatedBy"].ToString(),
                        LastUpdated = Convert.IsDBNull(navigatortransfersDS.Tables[0].Rows[index]["LastUpdated"]) ? DateTime.MinValue : (DateTime)navigatortransfersDS.Tables[0].Rows[index]["LastUpdated"],
                        UpdatedBy = navigatortransfersDS.Tables[0].Rows[index]["UpdatedBy"].ToString()
                    });
                }

            }
            catch
            {
                throw;
            }

            return lsNavigatorTransfers;
        }

        #endregion

        #region GetTransferStatusDropDown
        public static List<SelectListItem> GetTransferStatusDropDown()
        {
            List<SelectListItem> ls = new List<SelectListItem>();

            ls.Add(new SelectListItem() { Text = "Accept", Value = "1" });
            ls.Add(new SelectListItem() { Text = "Refuse", Value = "2" });
           
            return ls;
        }

        #endregion

        #region Get Navigator Transfer Reason drop down
        public static List<SelectListItem> GetTransferReasonsDropDown()
        {
            List<SelectListItem> lsTransferReasons = new List<SelectListItem>();

            try
            {
                DataSet transferreasonsDS = getProxy().GetTransferReasonsList(0,0);
                for (var index = 0; index < transferreasonsDS.Tables[0].Rows.Count; index++)
                {
                    lsTransferReasons.Add(new SelectListItem() { Text = transferreasonsDS.Tables[0].Rows[index]["NavigatorTransferReason"].ToString(), Value = transferreasonsDS.Tables[0].Rows[index]["NavigatorTransferReasonCode"].ToString() });
                }

            }
            catch
            {
                throw;
            }

            return lsTransferReasons;
        }

        #endregion

        public JsonResult GetTransferReason(int NavigatorID)

        {
            DataSet NavigatorDS1 = getProxy().GetNavigator(User.Identity.Name);

            int NavigatorID1 = (int)NavigatorDS1.Tables[0].Rows[0]["NavigatorID"];
            int NavigatorID2 = NavigatorID;

            List<SelectListItem> reasons = new List<SelectListItem>();
            try
            {
                DataSet transferreasonsDS = getProxy().GetTransferReasonsList(NavigatorID1, NavigatorID2);
                for (var index = 0; index < transferreasonsDS.Tables[0].Rows.Count; index++)
                {
                    reasons.Add(new SelectListItem() { Text = transferreasonsDS.Tables[0].Rows[index]["NavigatorTransferReason"].ToString(), Value = transferreasonsDS.Tables[0].Rows[index]["NavigatorTransferReasonCode"].ToString() });
                }

            }
            catch
            {
                throw;
            }

            return Json(new SelectList(reasons, "Value", "Text"));

        }


    }
}