//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EWC_NSAA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class trHealthInsuranceStatus
    {
        public int HealthInsuranceStatusCode { get; set; }
        public string HealthInsuranceStatusName { get; set; }
    }
}
