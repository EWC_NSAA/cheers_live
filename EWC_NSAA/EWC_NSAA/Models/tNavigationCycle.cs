//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EWC_NSAA.Models
{
    using System;
    using System.Collections.Generic;

    using System.ComponentModel.DataAnnotations;
    using EWC_NSAA.Metadata;
    [MetadataType(typeof(NavigationCycleMetaData))]
    public partial class tNavigationCycle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tNavigationCycle()
        {
            this.tEncounter = new HashSet<tEncounter>();
            this.tCaregiverApprovals = new HashSet<tCaregiverApprovals>();
        }
    
        public int NavigationCycleID { get; set; }
        public int FK_RecipientID { get; set; }
        public Nullable<int> FK_ReferralID { get; set; }
        public int CancerSite { get; set; }
        public int NSProblem { get; set; }
        public int HealthProgram { get; set; }
        public int HealthInsuranceStatus { get; set; }
        public Nullable<int> HealthInsurancePlan { get; set; }
        public string HealthInsurancePlanNumber { get; set; }
        public string ContactPrefDays { get; set; }
        public string ContactPrefHours { get; set; }
        public string ContactPrefHoursOther { get; set; }
        public Nullable<int> ContactPrefType { get; set; }
        public Nullable<bool> SRIndentifierCodeGenerated { get; set; }
        public int FK_NavigatorID { get; set; }
        public string xtag { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<int> FK_ProviderID { get; set; }
    
        public virtual tNavigator tNavigator { get; set; }
        public virtual tServiceRecipient tServiceRecipient { get; set; }
        public virtual tNavigationCycle tNavigationCycle1 { get; set; }
        public virtual tNavigationCycle tNavigationCycle2 { get; set; }
        public virtual tNSProvider tNSProvider { get; set; }
        public virtual tNSProvider tNSProvider1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tEncounter> tEncounter { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tCaregiverApprovals> tCaregiverApprovals { get; set; }
    }
}
