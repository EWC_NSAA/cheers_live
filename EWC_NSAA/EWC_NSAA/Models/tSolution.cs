//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EWC_NSAA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tSolution
    {
        public int SolutionID { get; set; }
        public int FK_BarrierID { get; set; }
        public int SolutionOffered { get; set; }
        public int SolutionImplementationStatus { get; set; }
        public DateTime? FollowUpDt { get; set; }
        public string xtag { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string UpdatedBy { get; set; }
    
        public virtual tBarrier tBarrier { get; set; }
    }
}
