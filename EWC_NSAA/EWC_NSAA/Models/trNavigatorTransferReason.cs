﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.Models
{
    public class trNavigatorTransferReason
    {
        public int NavigatorTransferReasonCode { get; set; }
        public string NavigatorTransferReason { get; set; }
    }
}