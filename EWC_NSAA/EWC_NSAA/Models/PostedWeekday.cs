﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.ContactPreferenceCBL.Models
{
    public class PostedWeekday
    {
        //this array will be used to POST values from the form to the controller
        public string[] WeekdayIds { get; set; }
    }
}