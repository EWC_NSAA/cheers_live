﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.ContactPreferenceCBL.Models
{
    public class WeekdayContactPreference
    {
        //Integer value of a checkbox
        public int? WeekdayCode { get; set; }

        //String name of a checkbox
        public string WeekdayName { get; set; }

        //Boolean value to select a checkbox
        //on the list
        public bool? WeekdayIsSelected { get; set; }

        //Object of html tags to be applied
        //to checkbox, e.g.:'new{tagName = "tagValue"}'
        public object WeekdayTags { get; set; }
   
    }
}