﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.Models
{
    public class NavigatorLanguages
    {
        public int NavigatorLanguageID { get; set; }
        public int FK_NavigatorID { get; set; }
        public int LanguageCode { get; set; }
        public string LanguageName { get; set; }
        public int SpeakingScore { get; set; }
        public int ListeningScore { get; set; }
        public int ReadingScore { get; set; }
        public int WritingScore { get; set; }
        public string xtag { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string UpdatedBy { get; set; }
    }
}