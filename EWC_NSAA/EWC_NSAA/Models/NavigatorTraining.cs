﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.Models
{
    public class NavigatorTraining
    {
        public int TrainingID { get; set; }
        public int FK_NavigatorID { get; set; }
        public string CourseName { get; set; }
        public string Organization { get; set; }
        public DateTime BeginDt { get; set; }
        public DateTime CompletionDt { get; set; }
        public bool Certificate { get; set; }
        public bool CMEAwarded { get; set; }
        public string xtag { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string UpdatedBy { get; set; }
    }
}