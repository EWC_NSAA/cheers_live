﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.CaregiverApprovalCBL.Models
{
    public class CaregiverApprovals
    {

        //Integer value of a checkbox
        public int ApprovalCode { get; set; }

        //String name of a checkbox
        public string ApprovalDescription { get; set; }

        //Boolean value to select a checkbox
        //on the list
        public bool IsSelected { get; set; }

        //Object of html tags to be applied
        //to checkbox, e.g.:'new{tagName = "tagValue"}'
        public object Tags { get; set; }
    }
}