﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using EWC_NSAA.Metadata;

namespace EWC_NSAA.Models
{
    [MetadataType(typeof(BarriersReportMetaData))]
    public class BarriersReport
    {
        public int BarrierID { get; set; }
        public int FK_EncounterID { get; set; }
        public DateTime? EncounterDt { get; set; }
        public string BarrierType { get; set; }
        public string BarrierAssessed { get; set; }
        public int SolutionID { get; set; }
        public int FK_BarrierID { get; set; }
        public string SolutionOffered { get; set; }
        public string SolutionImplementationStatus { get; set; }
        public DateTime FollowUpDt { get; set; }
    }
}