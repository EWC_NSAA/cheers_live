//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EWC_NSAA.Models
{
    using System;
    
    public partial class usp_GetRace_Result
    {
        public int RaceID { get; set; }
        public int FK_NSRecipientID { get; set; }
        public int RaceCode { get; set; }
        public string RaceName { get; set; }
        public string RaceOther { get; set; }
        public string xtag { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string UpdatedBy { get; set; }
    }
}
