﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EWC_NSAA.Common
{
    public static class StringExtensions
    {
        public static string GetUnformattedTelPhone(this string data)
        {
            string strtel = string.Empty;
            if (!string.IsNullOrWhiteSpace(data))
            {
                strtel = data.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ","").Trim();
            }
            return strtel;
        }
    }
}