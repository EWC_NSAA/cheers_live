﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Web;

namespace EWC_NSAA.Common
{
    /// <summary>
	/// Error class - 
	/// 
	/// </summary>

    /* ========================================================================

      1. IN YOUR Project 
      ==================
      Add a Reference to: System.Configuration
     

      2. IN FILE:  Globals.asax.cs
      ============================
      
      Add below to trap all unhandled exceptions
      ------------------------------------------
     
        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex != null) {
                Server.ClearError();
                ITSD.Errors.ErrLog.PublishException(ex);
            }
        }

      
     
      
     
     3. IN FILE:  web.config
     =======================
     
     Add below in to the <appSettings> section and CUSTOMIZE the values as needed 
     
<appSettings>
     
        <!-- comma delimited string of email addresses to send error details to  -->
		<add key="ERROR_EMailErrorsTo" value="rcalmann@dhs.ca.gov"/>

        <!-- single From address to use -->
		<add key="ERROR_EmailErrorsFrom" value="noReply@dhcs.ca.gov"/>

        <!-- mail server to use -->
		<add key="ERROR_MailServer" value="smtpoutbound.dhs.ca.gov"/>
		
        <!-- REQUIRED ENTRY - Error Page name relative to the Root of the site (used in Redirect)  -->
		<add key="ERROR_ErrorPage" value="~/ErrorPage.aspx"/>
    
		<!-- 
            Error Log file name ONLY, name it so user cannot BROWSE it ie. "ErrorLog.config"  
            if any error sending the email then writes the exception details to this log
            Remove completely or set value to "" to NOT log to file (but is not reccomended)
            Note: Log file will be truncated if it grows > 400K bytes
        -->
		<add key="ERROR_LogFile" value="ErrorLog.config"/>
   
		<!-- send mail asyc, will send sync if error on async: true,yes,on,1 OR false,no,off,0 -->
		<add key="ERROR_SendEmailAsync" value="yes"/>

        <!-- When the actual error page is missing we response.write a standby page -->
        <!-- The first message line in the standby error page -->
        <add key="ERROR_MainAbortMsg" value="An Adminstrator has been notified" />
        <!-- The next message line in the standby error page -->
        <add key="ERROR_ExitAbortMsg" value="Sorry for any inconvenience, please try again later" />

        <!-- 
          Report on maximum of "ERROR_ErrorThreshold" Errors in "ERROR_ThresholdMinutes" 
          Adjust as per expected number of users and concurrent users in your application
          to avoid a flood of emails but also not to mask any exceptions
        -->
        <add key="ERROR_ErrorThreshold" value="10"/>
        <add key="ERROR_ThresholdMinutes" value="30"/>

</appSettings> 
      
    =========================================================================== */
    public class Errors
    {
        // ***************************************************************************************
        // NOTE on Sending Mail ASYNC
        // To Send Email Asynch REQUIRES setting this attribute on each page:  <%@ Page Async="true"
        // If sending async Fails then the sendmail code will retry non-async
        // ***************************************************************************************


        // ------------------------------------------------------------------------------------------------
        // defaults to use just in case missing any web.config entries above
        // (BUT this should never happen)
        // ------------------------------------------------------------------------------------------------
        private static string DEFAULT_MAILTO_ADDRESS = "bilwa.buchake@dhcs.ca.gov";
        private static string DEFAULT_MAILFROM_ADDRESS = "noreply@dhcs.ca.gov";
        private static string DEFAULT_MAILSERVER = "smtpoutbound.dhs.ca.gov";

        // Threshold default is max of 10 errors every 60 minutes
        private static int DEFAULT_ERR_THRESHOLD_MINS = 60;
        private static int DEFAULT_ERR_THRESHOLD = 10;



        // ------------------------------------------------------------------------------------------------
        // web.config keys
        // ------------------------------------------------------------------------------------------------
        private static string KEY_MAILTO = "ERROR_EMailErrorsTo";             // use original key
        private static string KEY_MAILFROM = "ERROR_EmailErrorsFrom";           // use original key
        private static string KEY_MAILSERVER = "ERROR_MailServer";         // use original key

        private static string KEY_ERR_THRESHOLD_MINS = "ERROR_ThresholdMinutes";      // time to hold last error in cache for comapring of duplicates
        private static string KEY_ERROR_PAGE = "ERROR_ErrorPage";
        private static string KEY_LOGFILE = "ERROR_LogFile";
        private static string KEY_SEND_ASYNC = "ERROR_SendEmailAsync";
        private static string KEY_ABORT_MSGMAIN = "ERROR_MainAbortMsg";
        private static string KEY_ABORT_MSGEXIT = "ERROR_ExitAbortMsg";
        private static string KEY_ERR_THRESHOLD = "ERROR_ErrorThreshold";






        // ------------------------------------------------------------------------------------------------
        // Misc
        // ------------------------------------------------------------------------------------------------
        private static string DATE_FMT_STR = "MM/dd/yyyy";
        private static string CACHE_KEY_COUNT = "AppErrorsCountKey";
        private static string CACHE_KEY_TIME = "AppErrorsTimeKey";

        private static long MAXLOG_SZ = 400000L;  // truncate log if any larger



        #region Publish Errors

        /// <summary>Publish the exception passed and do extra work as per params passed (use this for finer control)</summary>				
        /// <param name="ex">The Exception to publish</param>
        /// <returns>void</returns>
        public static void PublishException(Exception ex, string appName, string version)
        {


            HttpContext ctx = HttpContext.Current;

            ctx.Server.ClearError();

            ctx.Session["ERRORPAGE_MSG"] = "";

            if (ex == null) return;

            // ----------------------------------------------
            // Fix any ThreadAbort exceptions and continue on
            // ----------------------------------------------
            if (ex.GetType() == typeof(System.Threading.ThreadAbortException))
            {
                Thread.ResetAbort();
                return;
            }


            // ---------------------------------------------
            // Valid Exception if here
            // ---------------------------------------------
            string errMsg = "";
            string tm = DateTime.Now.ToString(DATE_FMT_STR) + " " + DateTime.Now.ToShortTimeString();
            string NL = Environment.NewLine;

            // may be an anonymous app
            string userName = "Unknown User";
            if (System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
                userName = System.Threading.Thread.CurrentPrincipal.Identity.Name;


            string mailSubject = "Application Error in " + appName + " v" + version;


            // ----------------------------------------------
            // Mail Addresses
            // ----------------------------------------------
            string mailTo = ConfigHelper.GetConfigData(KEY_MAILTO, ""); // allow no entry to signal - not to send mail
            string mailFrom = ConfigHelper.GetConfigData(KEY_MAILFROM, DEFAULT_MAILFROM_ADDRESS).Trim().Replace(" ", "");
            // -----------------------------------------------
            // The Error Page - Must Exist, no defaults possible
            // -----------------------------------------------
            string errPage = ConfigHelper.GetConfigData(KEY_ERROR_PAGE, "").Trim();

            // do not send if missing TO address in config
            bool noMail = (mailTo.Length == 0);


            // --------------------------------------------------------------------
            // HOST:  If user accessed via proxy "Forwarded" will not be null
            //        else use normal remote_addr
            // --------------------------------------------------------------------
            string host = ctx.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] + "";
            if (host.Length == 0)
                host = ctx.Request.ServerVariables["REMOTE_ADDR"] + "";

            string svr = ctx.Server.MachineName + "";
            string agent = ctx.Request.UserAgent.ToString() + "";
            string rawUrl = ctx.Request.Url.AbsoluteUri;

            // ---------------------------------------------------------------------
            // get ALL the inner exception messages
            // ---------------------------------------------------------------------
            //StringBuilder sb    = new StringBuilder();
            //getInnerExceptions(sb,ex);
            //string messages     = sb.ToString();

            string baseMsg = ex.GetBaseException().Message;
            string stkTrace = ex.GetBaseException().StackTrace;


            // ---------------------------------------------------------------------
            // Put together the full email body
            // ---------------------------------------------------------------------
            errMsg += "Date: " + tm + NL + "User: " + userName + NL;
            errMsg += "Server: " + svr + "   Host: " + host + NL;
            errMsg += "Exception Page: " + rawUrl + NL;
            errMsg += "Exception Source: " + ex.Source + NL;
            errMsg += "UserAgent: " + agent + NL + NL;

            errMsg += "Base Exception: " + baseMsg + NL + NL;
            errMsg += "Base Stack Trace:" + NL + stkTrace + NL + NL;

            errMsg += "Exception: " + ex.Message + NL + NL;
            errMsg += "Stack Trace:" + NL + ex.StackTrace;

            // --------------------------------------------------------
            // avoid a flood of emails in recursive errors or otherwise
            // --------------------------------------------------------
            bool exceeded = hasExceededMaxErrors(ConfigHelper.GetConfigDataInt(KEY_ERR_THRESHOLD, DEFAULT_ERR_THRESHOLD), Convert.ToDouble(ConfigHelper.GetConfigDataInt(KEY_ERR_THRESHOLD_MINS, DEFAULT_ERR_THRESHOLD_MINS)));


            // ----------------------------------------------
            // Send the Mail 
            // ----------------------------------------------

            // use MailHelper and pass server
            string emailServer = ConfigHelper.GetConfigData(KEY_MAILSERVER, DEFAULT_MAILSERVER).Trim();

            string retMsg = "";
            if (!exceeded && !noMail)
            {
                retMsg = MailHelper.SendEMail(mailTo, mailFrom, mailSubject, errMsg, false, sendMailAsync(), emailServer);
            }

            // if email failed allow for the error page to display a message to the user to notify the admin
            if (retMsg.Length > 0)
                ctx.Session["ERRORPAGE_MSG"] = "Please Notify the web administrator";

            // ----------------------------------------------
            // If no Error Page OR the Error was in the errPage itself then just end here 
            // ----------------------------------------------
            if (errPage.Length == 0 || isPageInPath(rawUrl, errPage))
            {

                string mainMsg = ConfigHelper.GetConfigData(KEY_ABORT_MSGMAIN, "The admin has been notified");
                string exitMsg = ConfigHelper.GetConfigData(KEY_ABORT_MSGEXIT, "Please try again later");

                // if email not sent due to some error
                if (retMsg.Length > 0) mainMsg = "Please notify the web administrator";

                EndThisResponse(ctx, mailSubject + "<br />" + mainMsg, exitMsg);
                return;
            }


            // ----------------------------------------------
            // goto the Error Page
            // ----------------------------------------------
            try
            {
                ctx.Response.Redirect(errPage, false);
            }
            catch (System.Threading.ThreadAbortException)
            {
                Thread.ResetAbort();
            }
            catch (Exception eee)
            {
                // ignore or write to a log file on disk
                string msg = eee.Message;
            }
        } // end function


        // -----------------------------------------------------------------
        // !!! Recursively !!! obtains all messages of exceptions and innerexceptions
        // -----------------------------------------------------------------
        private static void getInnerExceptions(StringBuilder sb, Exception ex)
        {
            if (ex != null)
            {
                sb.Append("\r\n" + ex.Message);
                getInnerExceptions(sb, ex.InnerException);
            }
        }

        private static bool isPageInPath(string fullUrl, string thePage)
        {
            if ((thePage == null || thePage.Length == 0) || (fullUrl == null || fullUrl.Length == 0))
                return false;

            // get the page part of the url passed
            string[] parts = fullUrl.Split('/');

            string page = (parts.Length > 0) ? parts[parts.Length - 1] : parts[0];

            // get thePage (may be as "~/errorPage.aspx")
            string[] thePageParts = thePage.Split('/');

            thePage = (thePageParts.Length > 0) ? thePageParts[thePageParts.Length - 1] : thePageParts[0];

            // check if a match to thePage passed
            return (String.Compare(page, thePage, true) == 0);
        }


        private static bool sendMailAsync()
        {

            string s = ConfigHelper.GetConfigData(KEY_SEND_ASYNC, "false").Trim().ToLower();
            return (s == "yes" || s == "on" || s == "true" || s == "1");

        }


        /// <summary>Check if this is exception message is a duplicate of the previous exception message</summary>				
        /// <param name="maxErrs">The error threshold not to exceed in the thresHold Minutes</param>
        /// <param name="thresholdMinutes">Time in minutes to track error count</param>
        /// <returns>void</returns>
        private static bool hasExceededMaxErrors(int maxErrs, double thresholdMinutes)
        {

            // ----------------------------------------------------------------------------------------
            //  Check if the #errors does not exceed the maximum specified in the time period specified
            // ----------------------------------------------------------------------------------------

            HttpContext ctx = HttpContext.Current;

            DateTime dtNow = DateTime.Now;

            try
            {


                if (ctx.Cache[CACHE_KEY_COUNT] == null || ctx.Cache[CACHE_KEY_TIME] == null)
                {
                    ctx.Cache[CACHE_KEY_TIME] = dtNow.AddMinutes(thresholdMinutes);
                    ctx.Cache[CACHE_KEY_COUNT] = 1;
                    return false;
                }

                // get error and time count from the cache
                int counter = (int)ctx.Cache[CACHE_KEY_COUNT];
                DateTime dt = (DateTime)ctx.Cache[CACHE_KEY_TIME];

                ++counter;

                if (DateTime.Compare(dtNow, dt) <= 0)
                {
                    if (counter > maxErrs)
                        return true;                            // within time span and exceeded error count
                    else
                        ctx.Cache[CACHE_KEY_COUNT] = counter;   // update the count

                }
                else {

                    // timed out, so reset
                    ctx.Cache[CACHE_KEY_TIME] = DateTime.Now.AddMinutes(thresholdMinutes);
                    ctx.Cache[CACHE_KEY_COUNT] = 1;

                }

                return false;


                // Not Yet exceeded so update 
                // ctx.Cache.Insert(CACHE_KEY, counter, null, DateTime.Now.AddMinutes(thresholdMinutes), TimeSpan.Zero);

            }
            catch
            {
                return false;       // a safe return value
            }

        }





        #endregion


        #region Page_Utilities

        public static void SetFriendlyErrorMsg(string msg)
        {
            HttpContext ctx = HttpContext.Current;
            ctx.Session["FriendlyErrMsg"] = msg;
        }

        public static string GetFriendlyErrMsg()
        {
            // allow get 1 time only
            HttpContext ctx = HttpContext.Current;

            string msg = (ctx.Session["FriendlyErrMsg"] == null) ? "" : ctx.Session["FriendlyErrMsg"].ToString();

            ctx.Session["FriendlyErrMsg"] = null;   // clear after get

            return msg;

        }



        public static void EndThisResponse(HttpContext ctx, string msgMain, string msgExit)
        {
            try
            {
                ctx.Response.Clear();
                ctx.Response.Write("<center>");
                ctx.Response.Write("<h1>An Error has occurred in this application</h1>");
                ctx.Response.Write("<br /><h2> " + msgMain + " </h2>");
                ctx.Response.Write("<br /><h3>" + msgExit + "</h3>");
                ctx.Response.Write("</center>");
                ctx.Response.Flush();

                if (ctx.Session != null)
                    ctx.Session.Abandon();

                ctx.Request.Cookies.Clear();

            }
            catch
            {
                // ignore

            }
            finally
            {
                ctx.Response.End();  // no matter what
            }

        }


        private static string GetCurrentPageName()
        {
            return HttpContext.Current.GetType().Name;
        }


        private static string GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }


        private static string GetAppName(bool fullName)
        {
            string name = "";

            if (fullName)
                name = Assembly.GetExecutingAssembly().GetName().FullName;
            else
                name = Assembly.GetExecutingAssembly().GetName().Name;


            return name;
        }

        #endregion



        #region LOG

        public static void WriteErrorLog(string msg)
        {


            HttpContext ctx = HttpContext.Current;

            string logFile = ConfigHelper.GetConfigData(KEY_LOGFILE, "").Trim();

            if (logFile.Length == 0) return;  // user selected no log file

            string fPath = ctx.Request.MapPath(".", "", false) + "\\" + logFile;

            string dt = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

            bool appendData = true;

            try
            {

                new FileIOPermission(FileIOPermissionAccess.AllAccess, fPath).Demand();

                // avoid huge file on disk
                if (File.Exists(fPath))
                {
                    FileInfo f = new FileInfo(fPath);
                    if (f.Length > MAXLOG_SZ)
                        appendData = false;
                }

            }
            catch
            {
                return;
            }

            using (StreamWriter sw = new StreamWriter(fPath, appendData))
            {
                sw.WriteLine(" ");
                sw.WriteLine(dt);
                sw.WriteLine(msg);
                sw.WriteLine(" ");
            }


        }

        #endregion



    }
}