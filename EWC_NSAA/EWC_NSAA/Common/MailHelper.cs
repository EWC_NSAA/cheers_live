﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Permissions;
using System.Web;

namespace EWC_NSAA.Common
{
    public class MailHelper
    {

        static string _asyncErrMsg = "";

        public static void SendCompletedCallback(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {

            _asyncErrMsg = "";

            try
            {

                // Get the unique identifier for this asynchronous operation.
                String token = (string)e.UserState;

                if (e.Cancelled)
                    _asyncErrMsg = String.Format("[{0}] Send canceled.", token);
                else if (e.Error != null)
                    _asyncErrMsg = String.Format("[{0}] {1}", token, e.Error.ToString());

            }
            catch
            {
                // ignore
            }

        }



        public static string SendEMail(string toAddress, string fromAddress, string subject, string msg, bool htmlFormat, bool async, string emailServer)
        {
            // returns empty string if no problems, else an error message (Do Not Throw exceptions here)
            // to allow multiple address (Use comma seperated list): 

            // check valid address
            if (toAddress == null || toAddress.Trim().Length == 0 || fromAddress == null || fromAddress.Trim().Length == 0)
                return "Missing To Address and/or From Address";   //throw new ApplicationException("SendMail: Missing To and/or From Address");


            toAddress = toAddress.Replace(";", ",");  // in case is semi-colon delimited by mistake

            // 1.0.0.13 commented out for Bug Fix (was Not Used and caused exception if multiple addresses)
            //MailAddress toAdd   = new MailAddress(toAddress);
            //MailAddress fromAdd = new MailAddress(fromAddress);


            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(fromAddress, toAddress, subject, msg);

            mail.IsBodyHtml = htmlFormat;


            SmtpClient client = new SmtpClient(emailServer);

            try
            {

                if (async)
                {

                    // The userState can be any object 
                    string userState = "ITSD: " + DateTime.Now.Ticks.ToString();
                    // Set the method that is called back when the send operation ends. (Optional as nothing to do anyway)
                    client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);


                    client.SendAsync(mail, userState);
                    return "";

                }
                else {

                    //Send synch
                    client.Send(mail);
                    return "";
                }


            }
            catch (Exception ex)
            {

                string eMsg = ex.Message;

                // may be due to NOT having @Page Async=true set in the page, in any case retry using sync
                if (async)
                {
                    async = false;
                    try
                    {
                        client.Send(mail);
                        return "";
                    }
                    catch (Exception ee)
                    {
                        eMsg = ee.Message;
                    }
                }

                // if here then try to log it to a file
                WriteErrorLog("Error Sending Email: " + eMsg + "\n\n" + subject + "\n" + msg);
                return eMsg;

            }
            finally
            {
                if (!async)
                    mail.Dispose();
            }



        }



        public static string SendEMailWithAttachments(string toAddress, string fromAddress, string subject, string msg, bool htmlFormat, bool async, string emailServer, string[] fPaths)
        {
            // returns empty string if no problems, else an error message (Do Not Throw exceptions here)
            // to allow multiple address (Use comma seperated list): 

            // check valid address
            if (toAddress == null || toAddress.Trim().Length == 0 || fromAddress == null || fromAddress.Trim().Length == 0)
                return "Missing To Address and/or From Address";   //throw new ApplicationException("SendMail: Missing To and/or From Address");


            toAddress = toAddress.Replace(";", ",");  // in case is semi-colon delimited by mistake

            // 1.0.0.13 commented out for Bug Fix (was Not Used and caused exception if multiple addresses)
            //MailAddress toAdd   = new MailAddress(toAddress);
            //MailAddress fromAdd = new MailAddress(fromAddress);


            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(fromAddress, toAddress, subject, msg);

            mail.IsBodyHtml = htmlFormat;


            //ADD THE ATTACHMENT TO THE EMAIL
            if (fPaths != null && fPaths.Length > 0)
            {

                for (int ii = 0; ii < fPaths.Length; ++ii)
                {

                    Attachment mailAttachment = new Attachment(fPaths[ii]);
                    mail.Attachments.Add(mailAttachment);
                }

            }



            SmtpClient client = new SmtpClient(emailServer);

            try
            {

                if (async)
                {

                    // The userState can be any object 
                    string userState = "ITSD: " + DateTime.Now.Ticks.ToString();
                    // Set the method that is called back when the send operation ends. (Optional as nothing to do anyway)
                    client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);


                    client.SendAsync(mail, userState);
                    return "";

                }
                else {

                    //Send synch
                    client.Send(mail);
                    return "";
                }


            }
            catch (Exception ex)
            {

                string eMsg = ex.Message;

                // may be due to NOT having @Page Async=true set in the page, in any case retry using sync
                if (async)
                {
                    async = false;
                    try
                    {
                        client.Send(mail);
                        return "";
                    }
                    catch (Exception ee)
                    {
                        eMsg = ee.Message;
                    }
                }

                // if here then try to log it to a file
                WriteErrorLog("Error Sending Email: " + eMsg + "\n\n" + subject + "\n" + msg);
                return eMsg;

            }
            finally
            {
                if (!async)
                    mail.Dispose();
            }



        }




        #region LOG

        public static void WriteErrorLog(string msg)
        {


            HttpContext ctx = HttpContext.Current;
            string fPath = ctx.Request.MapPath(".", "", false) + "\\ErrorLog.Config";

            if (fPath.Length == 0) return;  // user selected no log file

            string dt = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

            bool appendData = true;

            try
            {

                new FileIOPermission(FileIOPermissionAccess.AllAccess, fPath).Demand();

                // avoid huge file on disk
                if (File.Exists(fPath))
                {
                    FileInfo f = new FileInfo(fPath);
                    if (f.Length > 1024000L)
                        appendData = false;
                }


                using (StreamWriter sw = new StreamWriter(fPath, appendData))
                {
                    sw.WriteLine(" ");
                    sw.WriteLine(dt);
                    sw.WriteLine(msg);
                    sw.WriteLine(" ");
                }



            }
            catch
            {
                return;
            }



        }

        #endregion

    }//end class
}//end namespace